############################################################################
# I/O STANDARDS and Location Constraints                                   #
############################################################################

# helper function to set all pin properties and to simplify pin definition syntax
proc define_pin { portf loc iostd { pindir "" } { p1 "" } { p2 "" } { p3 "" } } {
    set port [get_ports $portf]
    set p1a ""; set p1b ""; 
    set p2a ""; set p2b ""; 
    set p3a ""; set p3b ""; 
    
    if { $port == "" } {
        puts stderr "MYERROR: Pin constraint could not be applied for $portf - it does not exist in the design"
    } elseif {
        [catch {
            set_property PACKAGE_PIN $loc $port
            set_property iostandard $iostd $port
            if { $pindir != "" && $pindir != "DEFAULT" } {
                set_property PIO_DIRECTION $pindir $port
            }
            if { $p1 != "" } {
                set x [split $p1 " "]; set p1a [lindex $x 0]; set p1b [lindex $x 1];  
                set_property $p1a $p1b $port
                append p1a "="
            } 
            if { $p2 != "" } { 
                set x [split $p2 " "]; set p2a [lindex $x 0]; set p2b [lindex $x 1];  
                set_property $p2a $p2b $port
                append p2a "="
            } 
            if { $p3 != "" } { 
                set x [split $p3 " "]; set p3a [lindex $x 0]; set p3b [lindex $x 1];  
                set_property $p3a $p3b $port
                append p3a "="
            } 
        } result]} {
            puts stderr "MYERROR: Pin definition failed for $port - see next line"
            puts stderr "->$result"
    } else {
        puts stderr "MYINFO: Pin defined $port: LOC=$loc IOSTD=$iostd DIR=$pindir $p1a$p1b $p2a$p2b $p3a$p3b"
    }
}

####################################################################################
####################################################################################
#*****************************************************
#* Version SQM4-ZY7-LAN: 2xETH - USB + RTC - JTAG
#*****************************************************
# Do not populate:
# - ETH0_3V3: R84, R85
# - ETH1_3V3: R69, R79
# - ETH1_ALT: R96, R97, R98, R99, R100, R101, R102, R103
# - USB: U8, Y1, [R26, R11, C2, C6], (C56, C55)
# - LDO: R16
# - JTAG: R77, R73, R39, R14, R92
#
# Populate:
# - JTAG: J1, R80=0R0, R81=0R0, R82=0R0, R83=0R0

 
####################################################################################
# FIXED_IO pins: It seems that these pins (and also DDR_WEB) should not be defined 
# in XDC otherwise it creates a conflict with some internal properties and causes 
# bitstream generation to fail.

#define_pin PS_PORB_pad   B5 LVCMOS18 {slew fast} {drive 8} ; 
#define_pin PS_SRSTB_pad  C9 LVCMOS18 {slew fast} {drive 8} ; 
#define_pin PS_CLK_pad    F7 LVCMOS18 {slew fast} {drive 8} ; 

####################################################################################
# INPUT CLOCK PINS
# 25 MHz

define_pin SYSCLK_pad B19 LVCMOS33 INPUT  ;# IO_L13P_T2_MRCC_35 - Main clock for PL

############################################################################
# MIO PINS and their assigned SQM4-ZY7-V20-L functions 
 
define_pin MIO_pads[53] C12 LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# Enet 0/1 - mdio
define_pin MIO_pads[52] D10 LVCMOS33  OUTPUT {slew slow} {drive 8} {pullup TRUE}  ;# Enet 0/1 - mdc

define_pin MIO_pads[39] C13 LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rx_ctl
define_pin MIO_pads[38] F13 LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rxd_pads[3]
define_pin MIO_pads[37] B14 LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rxd_pads[2]
define_pin MIO_pads[36] A9  LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rxd_pads[1]
define_pin MIO_pads[35] F14 LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rxd_pads[0]
define_pin MIO_pads[34] B12 LVCMOS33  INPUT  {slew fast} {pullup TRUE}           ;# Enet 1 - rx_clk
define_pin MIO_pads[33] G13 LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - tx_ctl
define_pin MIO_pads[32] C7  LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - txd_pads[3]
define_pin MIO_pads[31] F9  LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - txd_pads[2]
define_pin MIO_pads[30] A11 LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - txd_pads[1]
define_pin MIO_pads[29] E8  LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - txd_pads[0]
define_pin MIO_pads[28] A12 LVCMOS33  OUTPUT {slew fast} {pullup TRUE}           ;# Enet 1 - tx_clk

define_pin MIO_pads[27] D7  LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rx_ctl
define_pin MIO_pads[26] A13 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[3]
define_pin MIO_pads[25] F12 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[2]
define_pin MIO_pads[24] B7  LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[1]
define_pin MIO_pads[23] E11 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[0]
define_pin MIO_pads[22] A14 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rx_clk
define_pin MIO_pads[21] F11 LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / tx_ctl
define_pin MIO_pads[20] A8  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[3]
define_pin MIO_pads[19] E10 LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[2]
define_pin MIO_pads[18] A7  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[1]
define_pin MIO_pads[17] E9  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[0]
define_pin MIO_pads[16] D6  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / tx_clk

define_pin MIO_pads[8]  E5  LVCMOS33  INPUT  {slew slow}  ;# NC (external pull-down)
define_pin MIO_pads[7]  D5  LVCMOS33  INPUT  {slew slow}  ;# NC (external pull-down)
                                                                                 
define_pin MIO_pads[6]  A4  LVCMOS33  OUTPUT {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_sclk
define_pin MIO_pads[5]  A3  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[3]
define_pin MIO_pads[4]  E4  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[2]
define_pin MIO_pads[3]  F6  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[1]
define_pin MIO_pads[2]  A2  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[0]
define_pin MIO_pads[1]  A1  LVCMOS33  OUTPUT {slew fast} {drive 8} {pullup TRUE}  ;# Quad SPI Flash - qspi0_ss_b

define_pin MIO_pads[0]  G6  LVCMOS33  INPUT   {slew slow} {pullup TRUE}  ;# NC 

############################################################################
# MIO PINS and their assigned SQM4_ZY7_V20_L-RIM/EASYBOARD V22 functions 

# LED
define_pin MIO_pads[51] C10 LVCMOS33  OUTPUT  {slew slow} {drive 8}                ;# RIM10 - LED_CTRL   
 
# CAN
define_pin MIO_pads[50] D13 LVCMOS33  OUTPUT  {slew slow} {drive 8} {pullup TRUE}  ;# RIM19 - GPIO/CAN_STB
define_pin MIO_pads[45] B9  LVCMOS33  INPUT   {slew slow} {drive 8}                ;# RIM17 - CAN_RX 
define_pin MIO_pads[44] E13 LVCMOS33  OUTPUT  {slew slow} {drive 8}                ;# RIM18 - CAN_TX  
                                                                                 
# EEPROM SPI
define_pin MIO_pads[49] C14 LVCMOS33  OUTPUT  {slew slow} {drive 8}                ;# RIM35 - SPI2_PCS
define_pin MIO_pads[48] D11 LVCMOS33  OUTPUT  {slew slow} {drive 8}                ;# RIM32 - SPI2_CCK
define_pin MIO_pads[47] B10 LVCMOS33  INPUT   {slew slow} {drive 8} {pullup TRUE}  ;# RIM34 - SPI2_SIN
define_pin MIO_pads[46] D12 LVCMOS33  OUTPUT  {slew slow} {drive 8} {pullup TRUE}  ;# RIM33 - SPI2_SOUT

# I2C0 + (RTC)
define_pin MIO_pads[43] B11 LVCMOS33  BIDIR   {slew slow} {drive 8} {pullup TRUE}  ;# RIM16 - I2C0_SDA + RTC:I2C-SDA
define_pin MIO_pads[42] D8  LVCMOS33  BIDIR   {slew slow} {drive 8} {pullup TRUE}  ;# RIM15 - I2C0_CSL + RTC:I2C-SCL

# UART1                                                                                 
define_pin MIO_pads[41] C8  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM6 - UART1_RX  
define_pin MIO_pads[40] E14 LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM5 - UART1_TX  
                                                                                 
# SDHC
define_pin MIO_pads[15] E6  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM129 - SDHC_D3   
define_pin MIO_pads[14] B6  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM128 - SDHC_D2
define_pin MIO_pads[13] A6  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM127 - SDHC_D1
define_pin MIO_pads[12] C5  LVCMOS33  OUTPUT  {slew fast} {drive 8} {pullup TRUE}  ;# RIM124 - SDHC_CLK
define_pin MIO_pads[11] B4  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM125 - SDHC_CMD            
define_pin MIO_pads[10] G7  LVCMOS33  BIDIR   {slew fast} {drive 8} {pullup TRUE}  ;# RIM126 - SDHC_D0
define_pin MIO_pads[9]  C4  LVCMOS33  INPUT   {slew fast} {drive 8} {pullup TRUE}  ;# RIM130 - SDHC_CD


############################################################################
# DDR PINS

define_pin DDR_WEB_pad    R4 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_RAS_n_pad  R5 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_ODT_pad    P5 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_DRSTB_pad  F3 SSTL15 BIDIR  {slew fast}  ;
define_pin DDR_CS_n_pad   P6 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_CKE_pad    V3 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_CAS_n_pad  P3 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_Clk_p_pad  N4 DIFF_SSTL15 INPUT {slew fast}   ;
define_pin DDR_Clk_n_pad  N5 DIFF_SSTL15 INPUT {slew fast}   ;
 
define_pin DDR_VRP_pad    N7 SSTL15_T_DCI DEFAULT {slew fast}  ;
define_pin DDR_VRN_pad    M7 SSTL15_T_DCI DEFAULT {slew fast}  ;

define_pin DDR_DQS_p_pads[3] V2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[2] N2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[1] H2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[0] C2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[3] W2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[2] P2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[1] J2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[0] D2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;

define_pin DDR_DQ_pads[31] Y1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[30] W3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[29] Y3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[28] W1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[27] U2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[26] AA1 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[25] U1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[24] AA3 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[23] R1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[22] M2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[21] T2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[20] R3  SSTL15_T_DCI BIDIR {slew fast} ;
define_pin DDR_DQ_pads[19] T1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[18] N3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[17] T3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[16] M1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[15] K3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[14] J1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[13] K1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[12] L3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[11] L2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[10] L1  SSTL15_T_DCI BIDIR {slew fast} ;
define_pin DDR_DQ_pads[9]  G1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[8]  G2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[7]  F1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[6]  F2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[5]  E1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[4]  E3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[3]  D3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[2]  B2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[1]  C3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[0]  D1  SSTL15_T_DCI BIDIR {slew fast} ; 

define_pin DDR_DM_pads[3]  AA2 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[2]  P1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[1]  H3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[0]  B1  SSTL15_T_DCI BIDIR {slew fast} ; 
 
define_pin DDR_Addr_pads[14] G4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[13] F4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[12] H4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[11] G5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[10] J3  SSTL15 OUTPUT {slew slow} ;
define_pin DDR_Addr_pads[9]  H5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[8]  J5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[7]  J6  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[6]  J7  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[5]  K5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[4]  K6  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[3]  L4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[2]  K4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[1]  M5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[0]  M4  SSTL15 OUTPUT {slew slow} ; 

define_pin DDR_BankAddr_pads[2] M6  SSTL15 OUTPUT {slew slow}  ; 
define_pin DDR_BankAddr_pads[1] L6  SSTL15 OUTPUT {slew slow}  ; 
define_pin DDR_BankAddr_pads[0] L7  SSTL15 OUTPUT {slew slow}  ; 

############################################################################
# IO PINS connected to ethernet phys and their assigned SQM4-ZY7-V20-L

define_pin ETH0_PHYLED0_pad   D16 LVCMOS33 INPUT  ;# IO_L2P_T0_AD8P_35: ETH_PHY.LED0 to Zynq
define_pin ETH0_PHYLED1_pad   J18 LVCMOS33 INPUT  ;# IO_L7P_T1_34: ETH_PHY.LED1 to Zynq
define_pin ETH0_PHYLED2_pad   J17 LVCMOS33 INPUT  ;# IO_L2N_T0_34: ETH_PHY.LED2/INT to Zynq
define_pin ETH0_PHYCLK125_pad L18 LVCMOS33 INPUT  ;# IO_L12P_T1_MRCC_34: ETH_PHY.CLK125 to Zynq
define_pin ETH0_PHYCONFIG_pad K21 LVCMOS33 INPUT  ;# IO_L9N_T1_DQS_34: ETH_PHY.CONFIG to Zynq
define_pin ETH0_PHYRESET_B_pad  H19 LVCMOS33 OUTPUT ;# IO_L19P_T3_35: ETH_PHY.RESET (To enable phy must be hi)

define_pin ETH1_PHYLED0_pad   E15 LVCMOS33 INPUT  ;# IO_L3P_T0_DQS_AD1P_35: ETH_PHY.LED0 to Zynq
define_pin ETH1_PHYLED1_pad   D15 LVCMOS33 INPUT  ;# IO_L3N_T0_DQS_AD1N_35: ETH_PHY.LED1 to Zynq
define_pin ETH1_PHYLED2_pad   H17 LVCMOS33 INPUT  ;# IO_0_35: ETH_PHY.LED2/INT to Zynq
define_pin ETH1_PHYCLK125_pad B20 LVCMOS33 INPUT  ;# IO_L13N_T2_MRCC_35: ETH_PHY.CLK125 to Zynq
define_pin ETH1_PHYCONFIG_pad H20 LVCMOS33 INPUT  ;# IO_L19N_T3_VREF_35: ETH_PHY.CONFIG to Zynq
define_pin ETH1_PHYRESET_B_pad  B15 LVCMOS33 OUTPUT ;# IO_L7N_T1_AD2N_35: ETH_PHY.RESET (To enable phy must be hi)

####################################################################################
# RIM/EASYBOARD V22

# RIM special-purpose pins
define_pin BUTTON_pad  P21 LVCMOS33 INPUT {pullup TRUE} ;# RIM_2 IO_L18N_T2_34: (user button)

define_pin RLED_pad  J21 LVCMOS33 OUTPUT {drive 8}  ;# RIM75 IO_L8P_T1_34 (Ethernet connector LED R)
define_pin LLED_pad  C15 LVCMOS33 OUTPUT {drive 8}  ;# RIM74 IO_L7P_T1_AD2P_35 (Ethernet connector LED L)

define_pin TAMPER0_pad M15 LVCMOS33 BIDIR {pullup TRUE} ;# RIM112 IO_L6P_T0_34:  (J10.2)
define_pin TAMPER1_pad M16 LVCMOS33 BIDIR {pullup TRUE} ;# RIM113 IO_L6N_T0_VREF_34:  (J10.1)

define_pin TOUCH_IRQ_pad N22 LVCMOS33 INPUT {pullup TRUE} ;# RIM_4   IO_L16P_T2_34: (CRTOUCH interrupt)

define_pin TAP_pad       J22 LVCMOS33 OUTPUT {drive 8}    ;# NC - right for Marvell PHY {if(R85=0R&R84=DNP) -> RIM69; if(R85=DNP&R84=0R) -> 3V3 for MAGJACK}
														   # if 3V3 not connect to RIM69 -> RLED not light

# RIM GPIO signals 
define_pin GPIO_pads[1]   L21 LVCMOS33 BIDIR {pullup TRUE} ;# RIM14 IO_L10P_T1_34: GPIO_1  (J10.13)
define_pin GPIO_pads[2]   C19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM24 IO_L12N_T1_MRCC_35: GPIO_2  (J10.47)
define_pin GPIO_pads[3]   D18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM25 IO_L12P_T1_MRCC_35: GPIO_3  (J10.46)
define_pin GPIO_pads[4]   C20 LVCMOS33 BIDIR {pullup TRUE} ;# RIM26 IO_L14N_T2_AD4N_SRCC_35: GPIO_4  (J10.45)
define_pin GPIO_pads[5]   E20 LVCMOS33 BIDIR {pullup TRUE} ;# RIM27 IO_L21N_T3_DQS_AD14N_35: GPIO_5  (J10.44)
define_pin GPIO_pads[6]   E21 LVCMOS33 BIDIR {pullup TRUE} ;# RIM28 IO_L17P_T2_AD5P_35: GPIO_6  (J10.48)
define_pin GPIO_pads[7]   D21 LVCMOS33 BIDIR {pullup TRUE} ;# RIM29 IO_L17N_T2_AD5N_35: GPIO_7  (J10.49)
define_pin GPIO_pads[8]   V10 LVCMOS33 BIDIR {pullup TRUE} ;# RIM117 IO_L1P_T0_13: GPIO_8  (J10.26)
define_pin GPIO_pads[9]   E18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM76 IO_L5N_T0_AD9N_35: GPIO_9  (J10.25)
define_pin GPIO_pads[10]  E19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM77 IO_L21P_T3_DQS_AD14P_35: GPIO_10  (J10.24)
define_pin GPIO_pads[11]  D17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM78 IO_L2N_T0_AD8N_35: GPIO_11  (J10.23)
define_pin GPIO_pads[12]  G16 LVCMOS33 BIDIR {pullup TRUE} ;# RIM79 IO_L4N_T0_35: GPIO_12  (J10.22)
define_pin GPIO_pads[13]  F17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM80 IO_L6N_T0_VREF_35: GPIO_13  (J10.21)
define_pin GPIO_pads[14]  F18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM81 IO_L5P_T0_AD9P_35: GPIO_14  (J10.17)
define_pin GPIO_pads[15]  G20 LVCMOS33 BIDIR {pullup TRUE} ;# RIM82 IO_L22P_T3_AD7P_35: GPIO_15  (J10.16)
define_pin GPIO_pads[16]  G19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM83 IO_L20P_T3_AD6P_35: GPIO_16  (J10.15)
define_pin GPIO_pads[17]  F19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM84 IO_L20N_T3_AD6N_35: GPIO_17  (J10.14)
define_pin GPIO_pads[18]  G21 LVCMOS33 BIDIR {pullup TRUE} ;# NC 
define_pin GPIO_pads[19]  W10 LVCMOS33 BIDIR {pullup TRUE} ;# RIM102 IO_L3N_T0_DQS_13: GPIO_19  (J10.12)
define_pin GPIO_pads[20]  W11 LVCMOS33 BIDIR {pullup TRUE} ;# RIM103 IO_L3P_T0_DQS_13: GPIO_20  (J10.11)
define_pin GPIO_pads[21]  W8  LVCMOS33 BIDIR {pullup TRUE} ;# RIM104 IO_L2N_T0_13: GPIO_21  (J10.10)
define_pin GPIO_pads[22]  V8  LVCMOS33 BIDIR {pullup TRUE} ;# RIM105 IO_L2P_T0_13: GPIO_22  (J10.9)
define_pin GPIO_pads[23]  Y8  LVCMOS33 BIDIR {pullup TRUE} ;# RIM106 IO_L12N_T1_MRCC_13: GPIO_23  (J10.8)
define_pin GPIO_pads[24]  Y9  LVCMOS33 BIDIR {pullup TRUE} ;# RIM107 IO_L12P_T1_MRCC_13: GPIO_24  (J10.7)
define_pin GPIO_pads[25]  W7  LVCMOS33 BIDIR {pullup TRUE} ;# RIM108 IO_L23N_T3_13: GPIO_25  (J10.6)
define_pin GPIO_pads[26]  V7  LVCMOS33 BIDIR {pullup TRUE} ;# RIM109 IO_L23P_T3_13: GPIO_26  (J10.5)
define_pin GPIO_pads[27]  Y10 LVCMOS33 BIDIR {pullup TRUE} ;# RIM110 IO_L10N_T1_13: GPIO_27  (J10.4)
define_pin GPIO_pads[28]  Y11 LVCMOS33 BIDIR {pullup TRUE} ;# RIM111 IO_L10P_T1_13: GPIO_28  (J10.3)

####################################################################################
# Analog pins of EasyBoard - used as digital IO pins on J10 connector   

define_pin ADIO_pads[0]  AA4 LVCMOS33 BIDIR {pullup TRUE} ;# RIM90 IO_L18N_T2_13: ADIO_0  (J10.30)
define_pin ADIO_pads[1]  Y4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM91 IO_L18P_T2_13: ADIO_1  (J10.29)
define_pin ADIO_pads[2]  V4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM92 IO_L21N_T3_DQS_13: ADIO_2  (J10.28)
define_pin ADIO_pads[3]  V5  LVCMOS33 BIDIR {pullup TRUE} ;# RIM93 IO_L21P_T3_DQS_13: ADIO_3  (J10.27)
define_pin ADIO_pads[4]  W5  LVCMOS33 BIDIR {pullup TRUE} ;# RIM94 IO_L24N_T3_13: ADIO_4
define_pin ADIO_pads[5]  W6  LVCMOS33 BIDIR {pullup TRUE} ;# RIM95 IO_L24P_T3_13: ADIO_5
define_pin ADIO_pads[6]  U4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM96 IO_L20N_T3_13: ADIO_6
define_pin ADIO_pads[7]  T4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM97 IO_L20P_T3_13: ADIO_7

####################################################################################
# Zynq analog input pins, EasyBoard defines these as DAC
define_pin ANALOG_VN_pad M12 LVCMOS33 BIDIR ;# Zynq.VN_0, RIM88 - grounded on AE10
define_pin ANALOG_VP_pad L11 LVCMOS33 BIDIR ;# Zynq.VP_0, RIM89 - grounded on AE10

####################################################################################
# GLCD pins on J2
define_pin GLCD_HSYNC_pad AB1  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM132 IO_L15N_T2_DQS_13: GLCD_HSYNC  (J2.37)
define_pin GLCD_VSYNC_pad AB2  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM133 IO_L15P_T2_DQS_13: GLCD_VSYNC  (J2.36)
define_pin GLCD_PCLK_pad  Y5   LVCMOS33 OUTPUT {pullup TRUE} ;# RIM134 IO_L13N_T2_MRCC_13: GLCD_PCLK  (J2.39)
define_pin GLCD_DE_pad    T6   LVCMOS33 OUTPUT {pullup TRUE} ;# RIM136 IO_L19N_T3_VREF_13: GLCD_DE  (J2.38)
define_pin GLCD_R_pads[0]  R6   LVCMOS33 OUTPUT {pullup TRUE} ;# RIM137 IO_L19P_T3_13: GLCD_D0_R0  (J2.35)
define_pin GLCD_R_pads[1]  AB4  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM138 IO_L16N_T2_13: GLCD_D1_R1  (J2.34)
define_pin GLCD_R_pads[2]  AB5  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM139 IO_L16P_T2_13: GLCD_D2_R2  (J2.33)
define_pin GLCD_R_pads[3]  AB6  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM140 IO_L17N_T2_13: GLCD_D3_R3  (J2.32)
define_pin GLCD_R_pads[4]  AB7  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM141 IO_L17P_T2_13: GLCD_D4_R4  (J2.31)
define_pin GLCD_R_pads[5]  AA8  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM142 IO_L11N_T1_SRCC_13: GLCD_D5_R5  (J2.30)
define_pin GLCD_R_pads[6]  AA9  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM143 IO_L11P_T1_SRCC_13: GLCD_D6_R6  (J2.29)
define_pin GLCD_R_pads[7]  AB9  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM144 IO_L9N_T1_DQS_13: GLCD_D7_R7  (J2.28)
define_pin GLCD_G_pads[0]  AB10 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM145 IO_L9P_T1_DQS_13: GLCD_D8_G0  (J2.27)
define_pin GLCD_G_pads[1]  U9   LVCMOS33 OUTPUT {pullup TRUE} ;# RIM146 IO_L6N_T0_VREF_13: GLCD_D9_G1  (J2.26)
define_pin GLCD_G_pads[2]  U10  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM147 IO_L6P_T0_13: GLCD_D10_G2  (J2.25)
define_pin GLCD_G_pads[3]  U11  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM148 IO_L5N_T0_13: GLCD_D11_G3  (J2.24)
define_pin GLCD_G_pads[4]  U12  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM149 IO_L5P_T0_13: GLCD_D12_G4  (J2.23)
define_pin GLCD_G_pads[5]  AB12 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM150 IO_L7N_T1_13: GLCD_D13_G5  (J2.22)
define_pin GLCD_G_pads[6]  AA12 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM151 IO_L7P_T1_13: GLCD_D14_G6  (J2.21)
define_pin GLCD_G_pads[7]  P17  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM152 IO_L20P_T3_34: GLCD_D15_G7  (J2.20)
define_pin GLCD_B_pads[0]  R16  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM153 IO_L24N_T3_34: GLCD_D16_B0  (J2.19)
define_pin GLCD_B_pads[1]  P16  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM154 IO_L24P_T3_34: GLCD_D17_B1  (J2.18)
define_pin GLCD_B_pads[2]  T19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM155 IO_L22N_T3_34: GLCD_D18_B2  (J2.16)
define_pin GLCD_B_pads[3]  R19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM156 IO_L22P_T3_34: GLCD_D19_B3  (J2.15)
define_pin GLCD_B_pads[4]  N20  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM157 IO_L14N_T2_SRCC_34: GLCD_D20_B4  (J2.14)
define_pin GLCD_B_pads[5]  N19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM158 IO_L14P_T2_SRCC_34: GLCD_D21_B5  (J2.13)
define_pin GLCD_B_pads[6]  K20  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM159 IO_L11N_T1_SRCC_34: GLCD_D22_B6  (J2.12)
define_pin GLCD_B_pads[7]  K19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM160 IO_L11P_T1_SRCC_34: GLCD_D23_B7  (J2.11)
define_pin LCD_BACKLIGHT_pad M17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM9 IO_L4N_T0_34:  ()

####################################################################################
# I2S SGTL5000 pins also available on J2 
define_pin I2S_TX_BCLK_pad V9  LVCMOS33 BIDIR {pullup TRUE} ;# RIM116 IO_L1N_T0_13:  (J2.50)
define_pin I2S_TXD0_pad    W12 LVCMOS33 BIDIR {pullup TRUE} ;# RIM118 IO_L4N_T0_13:  (J2.48)
define_pin I2S_TX_FS_pad   V12 LVCMOS33 BIDIR {pullup TRUE} ;# RIM119 IO_L4P_T0_13:  (J2.49)
define_pin I2S_MCLK_pad    M19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM120 IO_L13P_T2_MRCC_34:  (J2.44)
define_pin I2S_RX_BCLK_pad M20 LVCMOS33 BIDIR {pullup TRUE} ;# RIM121 IO_L13N_T2_MRCC_34:  (J2.47)
define_pin I2S_RXD0_pad    T18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM122 IO_L23N_T3_34:  (J2.45)
define_pin I2S_RX_FS_pad   R18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM123 IO_L23P_T3_34:  (J2.46)

####################################################################################
# SPI0 EMIO pins - on display connector J2 
define_pin SPI0_PCS0_pad H22 LVCMOS33 BIDIR               ;# RIM20 IO_L24P_T3_AD15P_35:  (J2.43)
define_pin SPI0_SIN_pad  G22 LVCMOS33 BIDIR {pullup TRUE} ;# RIM21 IO_L24N_T3_AD15N_35:  (J2.42)
define_pin SPI0_SOUT_pad F21 LVCMOS33 BIDIR {pullup TRUE} ;# RIM22 IO_L23P_T3_35:  (J2.41)
define_pin SPI0_SCK_pad  F22 LVCMOS33 BIDIR               ;# RIM23 IO_L23N_T3_35:  (J2.40)

####################################################################################
# UART0 EMIO pins - RS485, EasyBoard J4
define_pin UART0_RTS_pad L22 LVCMOS33 OUTPUT               ;# RIM13 IO_L10N_T1_34:  ()
define_pin UART0_RX_pad  M21 LVCMOS33 INPUT  {pullup TRUE} ;# RIM12 IO_L15P_T2_DQS_34:  ()
define_pin UART0_TX_pad  M22 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM11 IO_L15N_T2_DQS_34:  ()

####################################################################################
# UART1 is on MIO, modem signals on EMIO, EasyBoard J3  
define_pin UART1_CTS_pad N15 LVCMOS33 INPUT {pullup TRUE} ;# RIM8 IO_L19P_T3_34:  ()
define_pin UART1_RTS_pad P15 LVCMOS33 OUTPUT              ;# RIM7 IO_L19N_T3_VREF_34:  ()

####################################################################################
# EasyBoard pins marked as UART2 are general IO pins (no UART2 on Zynq)   
define_pin UART2_CTS_pad U6 LVCMOS33 BIDIR {pullup TRUE} ;# RIM101 IO_L22P_T3_13:  (J10.36)
define_pin UART2_RTS_pad U5 LVCMOS33 BIDIR {pullup TRUE} ;# RIM100 IO_L22N_T3_13:  (J10.39)
define_pin UART2_RX_pad AA7 LVCMOS33 BIDIR {pullup TRUE} ;# RIM99 IO_L14P_T2_SRCC_13:  (J10.37)
define_pin UART2_TX_pad AA6 LVCMOS33 BIDIR {pullup TRUE} ;# RIM98 IO_L14N_T2_SRCC_13:  (J10.38)

####################################################################################
# USB Enable signals, EasyBoard U8,J14,J6   
# (USB not available on this module version)
define_pin USB0_EN_pad D22 LVCMOS33 OUTPUT               ;# RIM30 IO_L16P_T2_35:  ()
define_pin USB0_OC_pad C22 LVCMOS33 INPUT  {pullup TRUE} ;# RIM31 IO_L16N_T2_35:  ()
define_pin USB1_EN_pad P22 LVCMOS33 OUTPUT               ;# RIM3 IO_L16N_T2_34:  ()
define_pin USB1_OC_pad D20 LVCMOS33 INPUT  {pullup TRUE} ;# RIM36 IO_L14P_T2_AD4P_SRCC_35:  ()

####################################################################################
# the following pins are named, but not connected on EasyBoard    
define_pin WIFI_DEBUG_UART_RXD_pad B21 LVCMOS33 BIDIR  {pullup TRUE} ;# RIM38 IO_L18P_T2_AD13p_35:
define_pin WIFI_DEBUG_UART_TXD_pad A22 LVCMOS33 BIDIR  {pullup TRUE} ;# RIM39 IO_L15N_T2_DQS_AD12N_35:

####################################################################################
# the following pins are not named on RIM, of course not connected on EasyBoard    
define_pin OTHIO_pads[0]  P20  LVCMOS33 BIDIR {pullup TRUE} ;# RIM1 IO_L18P_T2_34:  ()
define_pin OTHIO_pads[1]  B22  LVCMOS33 BIDIR {pullup TRUE} ;# RIM37 IO_L18N_T2_AD13N_35:  ()
define_pin OTHIO_pads[2]  A21  LVCMOS33 BIDIR {pullup TRUE} ;# RIM40 IO_L15P_T2_DQS_AD12P_35:  ()
define_pin OTHIO_pads[3]  A19  LVCMOS33 BIDIR {pullup TRUE} ;# RIM41 IO_L10N_T1_AD11N_35:  ()
define_pin OTHIO_pads[4]  A18  LVCMOS33 BIDIR {pullup TRUE} ;# RIM42 IO_L10P_T1_AD11P_35:  ()
define_pin OTHIO_pads[5]  B17  LVCMOS33 BIDIR {pullup TRUE} ;# RIM43 IO_L8N_T1_AD10N_35:  ()
define_pin OTHIO_pads[6]  B16  LVCMOS33 BIDIR {pullup TRUE} ;# RIM44 IO_L8P_T1_AD10P_35:  ()
define_pin OTHIO_pads[7]  A17  LVCMOS33 BIDIR {pullup TRUE} ;# RIM45 IO_L9N_T1_DQS_AD3N_35:  ()
define_pin OTHIO_pads[8]  A16  LVCMOS33 BIDIR {pullup TRUE} ;# RIM46 IO_L9P_T1_DQS_AD3P_35:  ()
define_pin OTHIO_pads[9]  C18  LVCMOS33 BIDIR {pullup TRUE} ;# RIM47 IO_L11N_T1_SRCC_35:  ()
define_pin OTHIO_pads[10] C17  LVCMOS33 BIDIR {pullup TRUE} ;# RIM48 IO_L11P_T1_SRCC_35:  ()
define_pin OTHIO_pads[11] AB11 LVCMOS33 BIDIR {pullup TRUE} ;# RIM114 IO_L8N_T1_13:  ()
define_pin OTHIO_pads[12] AA11 LVCMOS33 BIDIR {pullup TRUE} ;# RIM115 IO_L8P_T1_13:  ()
define_pin OTHIO_pads[13] Y6   LVCMOS33 BIDIR {pullup TRUE} ;# RIM135 IO_L13P_T2_MRCC_13:  ()

