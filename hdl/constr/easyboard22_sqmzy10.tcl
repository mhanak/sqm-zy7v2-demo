############################################################################
# I/O STANDARDS and Location Constraints                                   #
############################################################################

# helper function to set all pin properties and to simplify pin definition syntax
proc define_pin { portf loc iostd { pindir "" } { p1 "" } { p2 "" } { p3 "" } } {
    set port [get_ports $portf]
    set p1a ""; set p1b ""; 
    set p2a ""; set p2b ""; 
    set p3a ""; set p3b ""; 
    
    if { $port == "" } {
        puts stderr "MYPROBLEM: Pin constraint could not be applied for $portf - it does not exist in the design"
    } elseif {
        [catch {
            set_property PACKAGE_PIN $loc $port
            set_property iostandard $iostd $port
            if { $pindir != "" && $pindir != "DEFAULT" } {
                set_property PIO_DIRECTION $pindir $port
            }
            if { $p1 != "" } {
                set x [split $p1 " "]; set p1a [lindex $x 0]; set p1b [lindex $x 1];  
                set_property $p1a $p1b $port
                append p1a "="
            } 
            if { $p2 != "" } { 
                set x [split $p2 " "]; set p2a [lindex $x 0]; set p2b [lindex $x 1];  
                set_property $p2a $p2b $port
                append p2a "="
            } 
            if { $p3 != "" } { 
                set x [split $p3 " "]; set p3a [lindex $x 0]; set p3b [lindex $x 1];  
                set_property $p3a $p3b $port
                append p3a "="
            } 
        } result]} {
            puts stderr "MYERROR: Pin definition failed for $port - see next line"
            puts stderr "->$result"
    } else {
        puts stderr "MYINFO: Pin defined $port: LOC=$loc IOSTD=$iostd DIR=$pindir $p1a$p1b $p2a$p2b $p3a$p3b"
    }
}

####################################################################################
####################################################################################
# PIN CONFIGURATION

set USE_ENET1_EXT  0   ;# Microchip LAN 100M U17
set USE_ENET1_LAN  0   ;# External Marvel on J2

####################################################################################
# FIXED_IO pins: It seems that these pins (and also DDR_WEB) should not be defined 
# in XDC otherwise it creates a conflict with some internal properties and causes 
# bitstream generation to fail.

#define_pin PS_PORB_pad   B5 LVCMOS18 {slew fast} {drive 8} ; 
#define_pin PS_SRSTB_pad  C9 LVCMOS18 {slew fast} {drive 8} ; 
#define_pin PS_CLK_pad    F7 LVCMOS18 {slew fast} {drive 8} ; 

####################################################################################
# INPUT CLOCK PINS

define_pin SYSCLK_pad B19 LVCMOS33 INPUT  

#### define_pin SYSCLK_P_pad B19 LVDS_25 
#### define_pin SYSCLK_N_pad B20 LVDS_25  



############################################################################
# MIO PINS and their assigned EasyBoard functions 
 
define_pin MIO_pads[53] C12 LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# Enet 0 - mdio
define_pin MIO_pads[52] D10 LVCMOS33  OUTPUT {slew slow} {drive 8} {pullup TRUE}  ;# Enet 0 - mdc

define_pin MIO_pads[51] C10 LVCMOS33  INPUT  {slew slow} {drive 8} {pullup TRUE}  ;# NC  

define_pin MIO_pads[50] D13 LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# RIM19 (GPIO/CAN_STB)

define_pin MIO_pads[49] C14 LVCMOS33  BIDIR  {slew slow} {drive 8}                ;# RIM35 (SPI 1 - ss[0])
define_pin MIO_pads[48] D11 LVCMOS33  BIDIR  {slew slow} {drive 8}                ;# RIM32 (SPI 1 - sclk)
define_pin MIO_pads[47] B10 LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# RIM34 (SPI 1 - miso)
define_pin MIO_pads[46] D12 LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# RIM33 (SPI 1 - mosi)

define_pin MIO_pads[45] B9  LVCMOS33  INPUT  {slew slow} {drive 8}                ;# RIM17 (CAN 1 - rx)  
define_pin MIO_pads[44] E13 LVCMOS33  OUTPUT {slew slow} {drive 8}                ;# RIM18 (CAN 1 - tx)  
                                                                                 
define_pin MIO_pads[43] B11 LVCMOS33  BIDIR  {slew slow} {drive 8}                ;# RIM16 (I2C 0 - sda) + RTC:I2C-SDA
define_pin MIO_pads[42] D8  LVCMOS33  BIDIR  {slew slow} {drive 8}                ;# RIM15 (I2C 0 - scl) + RTC:I2C-SCL
                                                                                 
define_pin MIO_pads[41] C8  LVCMOS33  INPUT  {slew fast} {drive 8}                ;# RIM6 (UART 1 - rx)  
define_pin MIO_pads[40] E14 LVCMOS33  OUTPUT {slew fast} {drive 8}                ;# RIM5 (UART 1 - tx)  
                                                                                 
define_pin MIO_pads[39] C13 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[7]
define_pin MIO_pads[38] F13 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[6]
define_pin MIO_pads[37] B14 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[5]
define_pin MIO_pads[36] A9  LVCMOS33  INPUT  {slew fast} {drive 8}                ;# USB 0 / clk
define_pin MIO_pads[35] F14 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[3]
define_pin MIO_pads[34] B12 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[2]
define_pin MIO_pads[33] G13 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[1]
define_pin MIO_pads[32] C7  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[0]
define_pin MIO_pads[31] F9  LVCMOS33  INPUT  {slew fast} {drive 8}                ;# USB 0 / nxt
define_pin MIO_pads[30] A11 LVCMOS33  OUTPUT {slew fast} {drive 8}                ;# USB 0 / stp
define_pin MIO_pads[29] E8  LVCMOS33  INPUT  {slew fast} {drive 8}                ;# USB 0 / dir
define_pin MIO_pads[28] A12 LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# USB 0 / data_pads[4]

define_pin MIO_pads[27] D7  LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rx_ctl
define_pin MIO_pads[26] A13 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[3]
define_pin MIO_pads[25] F12 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[2]
define_pin MIO_pads[24] B7  LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[1]
define_pin MIO_pads[23] E11 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rxd_pads[0]
define_pin MIO_pads[22] A14 LVCMOS33 INPUT  {slew fast} {pullup TRUE}            ;# Enet 0 / rx_clk
define_pin MIO_pads[21] F11 LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / tx_ctl
define_pin MIO_pads[20] A8  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[3]
define_pin MIO_pads[19] E10 LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[2]
define_pin MIO_pads[18] A7  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[1]
define_pin MIO_pads[17] E9  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / txd_pads[0]
define_pin MIO_pads[16] D6  LVCMOS33 OUTPUT {slew fast} {pullup TRUE}            ;# Enet 0 / tx_clk

define_pin MIO_pads[15] E6  LVCMOS33  BIDIR  {slew fast} {drive 8} {pullup TRUE}  ;# RIM129 (SD 0 - data_pads[3])   
define_pin MIO_pads[14] B6  LVCMOS33  BIDIR  {slew fast} {drive 8} {pullup TRUE}  ;# RIM128 (SD 0 - data_pads[2])
define_pin MIO_pads[13] A6  LVCMOS33  BIDIR  {slew fast} {drive 8} {pullup TRUE}  ;# RIM127 (SD 0 - data_pads[1])
define_pin MIO_pads[12] C5  LVCMOS33  OUTPUT {slew fast} {drive 8} {pullup TRUE}  ;# RIM124 (SD 0 - clk)
define_pin MIO_pads[11] B4  LVCMOS33  BIDIR  {slew fast} {drive 8} {pullup TRUE}  ;# RIM125 (SD 0 - cmd)            
define_pin MIO_pads[10] G7  LVCMOS33  BIDIR  {slew fast} {drive 8} {pullup TRUE}  ;# RIM126 (SD 0 - data_pads[0])
define_pin MIO_pads[9]  C4  LVCMOS33  INPUT  {slew fast} {drive 8} {pullup TRUE}  ;# RIM130 (SD 0 - cd)

#define_pin MIO_pads[8]  E5  LVCMOS33  OUTPUT {slew slow} {drive 8}                ;# Quad SPI Flash / qspi_fbclk
define_pin MIO_pads[8]  E5  LVCMOS33  INPUT  {slew slow} {drive 8}                ;# NC
#define_pin MIO_pads[7]  D5  LVCMOS33  BIDIR  {slew slow} {drive 8}                ;# GPIO
define_pin MIO_pads[7]  D5  LVCMOS33  INPUT  {slew slow} {drive 8}                ;# NC
                                                                                 
define_pin MIO_pads[6]  A4  LVCMOS33  OUTPUT {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_sclk
define_pin MIO_pads[5]  A3  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[3]
define_pin MIO_pads[4]  E4  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[2]
define_pin MIO_pads[3]  F6  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[1]
define_pin MIO_pads[2]  A2  LVCMOS33  BIDIR  {slew fast} {drive 8}                ;# Quad SPI Flash - qspi0_io_pads[0]
define_pin MIO_pads[1]  A1  LVCMOS33  OUTPUT {slew fast} {drive 8} {pullup TRUE}  ;# Quad SPI Flash - qspi0_ss_b

define_pin MIO_pads[0]  G6  LVCMOS33  BIDIR  {slew slow} {drive 8} {pullup TRUE}  ;# RIM10 (LED) 

############################################################################
# DDR PINS

define_pin DDR_WEB_pad    R4 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_RAS_n_pad  R5 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_ODT_pad    P5 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_DRSTB_pad  F3 SSTL15 BIDIR  {slew fast}  ;
define_pin DDR_CS_n_pad   P6 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_CKE_pad    V3 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_CAS_n_pad  P3 SSTL15 OUTPUT {slew slow}  ;
define_pin DDR_Clk_p_pad  N4 DIFF_SSTL15 INPUT {slew fast}   ;
define_pin DDR_Clk_n_pad  N5 DIFF_SSTL15 INPUT {slew fast}   ;
 
define_pin DDR_VRP_pad    N7 SSTL15_T_DCI DEFAULT {slew fast}  ;
define_pin DDR_VRN_pad    M7 SSTL15_T_DCI DEFAULT {slew fast}  ;

define_pin DDR_DQS_p_pads[3] V2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[2] N2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[1] H2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_p_pads[0] C2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[3] W2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[2] P2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[1] J2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;
define_pin DDR_DQS_n_pads[0] D2 DIFF_SSTL15_T_DCI BIDIR {slew fast}  ;

define_pin DDR_DQ_pads[31] Y1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[30] W3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[29] Y3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[28] W1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[27] U2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[26] AA1 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[25] U1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[24] AA3 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[23] R1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[22] M2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[21] T2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[20] R3  SSTL15_T_DCI BIDIR {slew fast} ;
define_pin DDR_DQ_pads[19] T1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[18] N3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[17] T3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[16] M1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[15] K3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[14] J1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[13] K1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[12] L3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[11] L2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[10] L1  SSTL15_T_DCI BIDIR {slew fast} ;
define_pin DDR_DQ_pads[9]  G1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[8]  G2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[7]  F1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[6]  F2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[5]  E1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[4]  E3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[3]  D3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[2]  B2  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[1]  C3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DQ_pads[0]  D1  SSTL15_T_DCI BIDIR {slew fast} ; 

define_pin DDR_DM_pads[3]  AA2 SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[2]  P1  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[1]  H3  SSTL15_T_DCI BIDIR {slew fast} ; 
define_pin DDR_DM_pads[0]  B1  SSTL15_T_DCI BIDIR {slew fast} ; 
 
define_pin DDR_Addr_pads[14] G4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[13] F4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[12] H4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[11] G5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[10] J3  SSTL15 OUTPUT {slew slow} ;
define_pin DDR_Addr_pads[9]  H5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[8]  J5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[7]  J6  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[6]  J7  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[5]  K5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[4]  K6  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[3]  L4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[2]  K4  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[1]  M5  SSTL15 OUTPUT {slew slow} ; 
define_pin DDR_Addr_pads[0]  M4  SSTL15 OUTPUT {slew slow} ; 

define_pin DDR_BankAddr_pads[2] M6  SSTL15 OUTPUT {slew slow}  ; 
define_pin DDR_BankAddr_pads[1] L6  SSTL15 OUTPUT {slew slow}  ; 
define_pin DDR_BankAddr_pads[0] L7  SSTL15 OUTPUT {slew slow}  ; 

####################################################################################
# RIM special-purpose pins used on EasyBoard

define_pin BUTTON_pad    P21 LVCMOS33 INPUT {pullup TRUE} ;# RIM2 (user button)
define_pin RLED_pad      J21 LVCMOS33 OUTPUT {drive 8}    ;# RIM75 (Ethernet connector LED L)

define_pin TAMPER0_pad   M15 LVCMOS33 BIDIR {pullup TRUE} ;# RIM113 (J10.2)
define_pin TAMPER1_pad   M16 LVCMOS33 BIDIR {pullup TRUE} ;# RIM114 (J10.1)

define_pin TOUCH_IRQ_pad N22 LVCMOS33 INPUT {pullup TRUE} ;# RIM4 (CRTOUCH interrupt)

define_pin TAP_pad       J22 LVCMOS33 OUTPUT {drive 8}    ;# RIM69 if R69=0 -> 3V3 for MAGJACK; if R69=DNP ??? 

####################################################################################
# RIM GPIO signals 

define_pin GPIO_pads[1]  L21 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM14 (J10.13)
define_pin GPIO_pads[2]  C19 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R80) RIM24 (J10.47) 
define_pin GPIO_pads[3]  D18 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R81) RIM25 (J10.46)
define_pin GPIO_pads[4]  C20 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R82) RIM26 (J10.45)
define_pin GPIO_pads[5]  E20 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R83) RIM27 (J10.44)
define_pin GPIO_pads[6]  E21 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM28  (J10.48)
define_pin GPIO_pads[7]  D21 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM29  (J10.49)
define_pin GPIO_pads[8]  U12 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM117 (J10.26)
if { $USE_ENET1_LAN == 0 } {
    define_pin GPIO_pads[9]  E18 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R101) RIM76 (J10.25); also U17.MDIO
    define_pin GPIO_pads[10] E19 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R102) RIM77 (J10.24); also U17.MDC
    define_pin GPIO_pads[11] D17 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R100) RIM78 (J10.23); also U17.CRS_DV/MODE2, RIM78=U17.VDDCR
    define_pin GPIO_pads[12] G16 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R99)  RIM79 (J10.22); also U17.RXD1/MODE1, RIM79=U17.TXP
    define_pin GPIO_pads[13] F17 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R98)  RIM80 (J10.21); also U17.RXD0/MODE0, RIM80=U17.TXN
    define_pin GPIO_pads[14] F18 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R97)  RIM81 (J10.17); also U17.RXER/PHYAD0, RIM81=U17.RXP
    define_pin GPIO_pads[15] G20 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R96)  RIM82 (J10.16); also U17.TXD1, RIM82=U17.RXN
    define_pin GPIO_pads[16] G19 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R95)  RIM83 (J10.15); also U17.TXD0, RIM83=U17.LED1/REGOFF
    define_pin GPIO_pads[17] F19 LVCMOS33 OUTPUT {pullup TRUE} ;# if(R94)  RIM84 (J10.14); also U17.TXEN, RIM84=LED2/NINTSEL
}    
define_pin GPIO_pads[18] V9 LVCMOS33 OUTPUT {pullup TRUE}  ;# unused
define_pin GPIO_pads[19] W11 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM102 (J10.12)
define_pin GPIO_pads[20] V10 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM103 (J10.11)
define_pin GPIO_pads[21] U9  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM104 (J10.10)
define_pin GPIO_pads[22] T4  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM105 (J10.9)
define_pin GPIO_pads[23] U5  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM106 (J10.8) 
define_pin GPIO_pads[24] T6  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM107 (J10.7)
define_pin GPIO_pads[25] R7  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM108 (J10.6)
define_pin GPIO_pads[26] V7  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM109 (J10.5)
define_pin GPIO_pads[27] J15 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM110 (J10.4); also short to W8
define_pin GPIO_pads[28] K15 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM111 (J10.3); also short to V8


# minor short-circuit bug, will be fixed in next SQM version
define_pin dummy_W8_pad W8 LVCMOS33 INPUT ;# short to J15 (RIM110)
define_pin dummy_V8_pad V8 LVCMOS33 INPUT ;# short to K15 (RIM111)

####################################################################################
# 2nd ENET module on EMIO - pins overlapping GPIO[17:9]   

if { $USE_ENET1_LAN > 0 } {
    define_pin ENET1_MDIO_pad    E18 LVCMOS33 BIDIR  {slew slow} {drive 8} {pullup TRUE} ;# U17.MDIO        
    define_pin ENET1_MDC_pad     E19 LVCMOS33 OUTPUT {slew slow} {drive 8} {pullup TRUE} ;# U17.MDC
             
    define_pin ENET1_CRS_DV_pad  D17 LVCMOS33 INPUT  {slew fast} {pullup TRUE}           ;# U17.CRS_DV/MODE2
    define_pin ENET1_RXD_pads[1] G16 LVCMOS33 INPUT  {slew fast} {pullup TRUE}           ;# U17.RXD1/MODE1 
    define_pin ENET1_RXD_pads[0] F17 LVCMOS33 INPUT  {slew fast} {pullup TRUE}           ;# U17.RXD0/MODE0 
    define_pin ENET1_RX_ER_pad   F18 LVCMOS33 INPUT  {slew fast} {pullup TRUE}           ;# U17.RXER/PHYAD0,

    define_pin ENET1_TXD_pads[1] G20 LVCMOS33 OUTPUT {slew fast} {drive 8} {pullup TRUE}     ;# U17.TXD1
    define_pin ENET1_TXD_pads[0] G19 LVCMOS33 OUTPUT {slew fast} {drive 8} {pullup TRUE}     ;# U17.TXD0
    define_pin ENET1_TX_EN_pad   F19 LVCMOS33 OUTPUT {slew fast} {drive 8} {pullup TRUE}     ;# U17.TXEN

#    define_pin ENET1_TXD_pads[1] G20 LVCMOS33 OUTPUT {slew fast} {drive 16} {OUT_TERM TUNED}     ;# U17.TXD1
#    define_pin ENET1_TXD_pads[0] G19 LVCMOS33 OUTPUT {slew fast} {drive 16} {OUT_TERM TUNED}     ;# U17.TXD0
#    define_pin ENET1_TX_EN_pad   F19 LVCMOS33 OUTPUT {slew fast} {drive 16} {OUT_TERM TUNED}     ;# U17.TXEN

    define_pin ENET1_CLK_pad     L19 LVCMOS33 OUTPUT {slew fast} {drive 8}                   ;# U17.XTAL1/CLKIN
}

####################################################################################
# Analog pins of EasyBoard - used as digital IO pins on J10 connector   

define_pin ADIO_pads[0] V4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM90 (J10.30)
define_pin ADIO_pads[1] Y4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM91 (J10.29)
define_pin ADIO_pads[2] AA4 LVCMOS33 BIDIR {pullup TRUE} ;# RIM92 (J10.28)
define_pin ADIO_pads[3] AB4 LVCMOS33 BIDIR {pullup TRUE} ;# RIM93 (J10.27)
define_pin ADIO_pads[4] W5  LVCMOS33 BIDIR {pullup TRUE} ;# RIM94
define_pin ADIO_pads[5] W6  LVCMOS33 BIDIR {pullup TRUE} ;# RIM95
define_pin ADIO_pads[6] W7  LVCMOS33 BIDIR {pullup TRUE} ;# RIM96
define_pin ADIO_pads[7] U4  LVCMOS33 BIDIR {pullup TRUE} ;# RIM97

# Zynq analog input pins, EasyBoard defines these as DAC
define_pin ANALOG_VN_pad M12 LVCMOS33 BIDIR ;# Zynq.VN_0, RIM88 (J10.35)
define_pin ANALOG_VP_pad L11 LVCMOS33 BIDIR ;# Zynq.VP_0, RIM89 (J10.34)

####################################################################################
# GLCD pins on J2

if { $USE_ENET1_EXT == 0 } {
    define_pin GLCD_HSYNC_pad AB1  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM132 (J2.37)
    define_pin GLCD_VSYNC_pad AB2  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM133 (J2.36)
    define_pin GLCD_PCLK_pad  Y5   LVCMOS33 OUTPUT {pullup TRUE} ;# RIM134 (J2.39)
    define_pin GLCD_DE_pad    AB5  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM136 (J2.38)
    define_pin GLCD_R_pads[0] AA6  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM137 (J2.35)
    define_pin GLCD_R_pads[1] AA7  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM138 (J2.34)
    define_pin GLCD_R_pads[2] AB6  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM139 (J2.33)
    define_pin GLCD_R_pads[3] AB7  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM140 (J2.32)
    define_pin GLCD_R_pads[4] AA8  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM141 (J2.31)
    define_pin GLCD_R_pads[5] AA9  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM142 (J2.30)
    define_pin GLCD_R_pads[6] AB9  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM143 (J2.29)
    define_pin GLCD_R_pads[7] AB10 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM144 (J2.28)
    define_pin GLCD_G_pads[0] AB11 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM145 (J2.27)
    define_pin GLCD_G_pads[1] AA11 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM146 (J2.26)
    define_pin GLCD_G_pads[2] Y10  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM147 (J2.25)
    define_pin GLCD_G_pads[3] Y11  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM148 (J2.24)
    define_pin GLCD_G_pads[4] AB12 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM149 (J2.23)
    define_pin GLCD_G_pads[5] AA12 LVCMOS33 OUTPUT {pullup TRUE} ;# RIM150 (J2.22)
    define_pin GLCD_G_pads[6] P18  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM151 (J2.21)
    define_pin GLCD_G_pads[7] P17  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM152 (J2.20)
    define_pin GLCD_B_pads[0] R16  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM153 (J2.19)
    define_pin GLCD_B_pads[1] P16  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM154 (J2.18)
    define_pin GLCD_B_pads[2] T19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM155 (J2.16)
    define_pin GLCD_B_pads[3] R19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM156 (J2.15)
    define_pin GLCD_B_pads[4] N20  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM157 (J2.14)
    define_pin GLCD_B_pads[5] N19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM158 (J2.13)
    define_pin GLCD_B_pads[6] K20  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM159 (J2.12)
    define_pin GLCD_B_pads[7] K19  LVCMOS33 OUTPUT {pullup TRUE} ;# RIM160 (J2.11)
    define_pin LCD_BACKLIGHT_pad M17 LVCMOS33 OUTPUT
}

####################################################################################
# I2S SGTL5000 pins also available on J2 

if { $USE_ENET1_EXT == 0 } {
    define_pin I2S_MCLK_pad    M19  LVCMOS33 BIDIR {pullup TRUE} ;# RIM120 (J2.44)
    define_pin I2S_RX_BCLK_pad M20  LVCMOS33 BIDIR {pullup TRUE} ;# RIM121 (J2.47)
    define_pin I2S_RXD0_pad    T18  LVCMOS33 BIDIR {pullup TRUE} ;# RIM122 (J2.45)
    define_pin I2S_RX_FS_pad   R18  LVCMOS33 BIDIR {pullup TRUE} ;# RIM123 (J2.46)
    define_pin I2S_TX_BCLK_pad U11  LVCMOS33 BIDIR {pullup TRUE} ;# RIM116 (J2.50)
    define_pin I2S_TX_FS_pad   W12  LVCMOS33 BIDIR {pullup TRUE} ;# RIM119 (J2.49)
    define_pin I2S_TXD0_pad    V12  LVCMOS33 BIDIR {pullup TRUE} ;# RIM118 (J2.48)
}

####################################################################################
# SPI0 EMIO pins - on display connector J2 

if { $USE_ENET1_EXT == 0 } {
    define_pin SPI0_PCS_pad    H22  LVCMOS33 BIDIR                ;# RIM20 (J2.43)
    define_pin SPI0_SIN_pad    G22  LVCMOS33 BIDIR  {pullup TRUE} ;# RIM21 (J2.42)
    define_pin SPI0_SOUT_pad   F21  LVCMOS33 BIDIR  {pullup TRUE} ;# RIM22 (J2.41)
    define_pin SPI0_SCK_pad    F22  LVCMOS33 BIDIR                ;# RIM23 (J2.40)
}

####################################################################################
# SQM4_Exp1G ENET module on EMIO - pins overlapping J2 pins

if { $USE_ENET1_EXT > 0 } {
    define_pin ENET1_TXD_pads[3] K19 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM160 (J2.11) {OUT_TERM TUNED}
    define_pin ENET1_TXD_pads[2] K20 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM159 (J2.12) {OUT_TERM TUNED}
    define_pin ENET1_TXD_pads[1] N19 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM158 (J2.13) {OUT_TERM TUNED}
    define_pin ENET1_TXD_pads[0] N20 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM157 (J2.14) {OUT_TERM TUNED}
    
    define_pin ENET1_TX_CLK_pad  R19 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM156 (J2.15)
    define_pin ENET1_TX_CTRL_pad T19 LVCMOS33 OUTPUT {slew fast} {drive 16}     ;# RIM155 (J2.16)
    
    define_pin ENET1_RXD_pads[3] P16 LVCMOS33 INPUT  {slew fast} {pullup TRUE}  ;# RIM154 (J2.18)
    define_pin ENET1_RXD_pads[2] R16 LVCMOS33 INPUT  {slew fast} {pullup TRUE}  ;# RIM153 (J2.19)
    define_pin ENET1_RXD_pads[1] P17 LVCMOS33 INPUT  {slew fast} {pullup TRUE}  ;# RIM152 (J2.20)
    define_pin ENET1_RXD_pads[0] P18 LVCMOS33 INPUT  {slew fast} {pullup TRUE}  ;# RIM151 (J2.21)

    define_pin ENET1_RESET_B_pad H22 LVCMOS33 OUTPUT {slew slow}   {drive 16}  ;# RIM20 (J2.43)
    #define_pin ENET1_CLK_25MHZ_pad M19 LVCMOS33 OUTPUT {slew fast} {drive 8}   ;# RIM120 (J2.44)
    define_pin ENET1_CLK_25MHZ_pad M19 LVCMOS33 INPUT {slew fast}               ;# RIM120 (J2.44)

    define_pin ENET1_RX_CLK_pad  T18 LVCMOS33 INPUT  {slew fast}                ;# RIM122 (J2.45)
    define_pin ENET1_RX_CTRL_pad R18 LVCMOS33 INPUT  {slew fast} {pullup TRUE}  ;# RIM123 (J2.46)
    
    define_pin ENET1_125MHZ_pad  M20 LVCMOS33 INPUT  {slew fast}                ;# RIM121 (J2.47)

    define_pin ENET1_CONFIG_pad  V12 LVCMOS33 INPUT  {slew slow} {pullup TRUE} ;# RIM118 (J2.48)

    define_pin ENET1_MDIO_pad    W12 LVCMOS33 BIDIR  {slew slow} {drive 8} {pullup TRUE} ;# RIM119 (J2.49)        
    define_pin ENET1_MDC_pad     U11 LVCMOS33 OUTPUT {slew slow} {drive 8} {pullup TRUE} ;# RIM116 (J2.50)
}

####################################################################################
# UART0 EMIO pins - RS485, EasyBoard J4
 
define_pin UART0_TX_pad    M22  LVCMOS33 OUTPUT               ;# RIM11
define_pin UART0_RX_pad    M21  LVCMOS33 INPUT  {pullup TRUE} ;# RIM12 
define_pin UART0_RTS_pad   L22  LVCMOS33 OUTPUT               ;# RIM13

####################################################################################
# UART1 is on MIO, modem signals on EMIO, EasyBoard J3  
 
define_pin UART1_CTS_pad   N15  LVCMOS33 INPUT  {pullup TRUE} ;# RIM8
define_pin UART1_RTS_pad   P15  LVCMOS33 OUTPUT               ;# RIM7

####################################################################################
# EasyBoard pins marked as UART2 are general IO pins (no UART2 on Zynq)   

define_pin UART2_TX_pad    V5   LVCMOS33 BIDIR                ;# RIM98
define_pin UART2_RX_pad    U6   LVCMOS33 BIDIR                ;# RIM99
define_pin UART2_RTS_pad   U7   LVCMOS33 BIDIR                ;# RIM100
define_pin UART2_CTS_pad   W10  LVCMOS33 BIDIR                ;# RIM101

####################################################################################
# USB Enable signals, EasyBoard U8,J14,J6   

define_pin USB0_EN_pad     D22  LVCMOS33 BIDIR  {pullup TRUE} ;# RIM30 
define_pin USB0_OC_pad     C22  LVCMOS33 INPUT  {pullup TRUE} ;# RIM31
define_pin USB1_EN_pad     P22  LVCMOS33 BIDIR  {pullup TRUE} ;# RIM3
define_pin USB1_OC_pad     D20  LVCMOS33 INPUT  {pullup TRUE} ;# RIM36

####################################################################################
# the following pins are named, but not connected on EasyBoard    

define_pin WIFI_DEBUG_UART_RXD_pad B21 LVCMOS33 BIDIR  {pullup TRUE} ;# RIM38
define_pin WIFI_DEBUG_UART_TXD_pad A22 LVCMOS33 BIDIR  {pullup TRUE} ;# RIM39

####################################################################################
# the following pins are not named on RIM, of course not connected on EasyBoard    
 
define_pin OTHIO_pads[0]  P20 LVCMOS33 BIDIR {pullup TRUE} ;# RIM1
define_pin OTHIO_pads[1]  B22 LVCMOS33 BIDIR {pullup TRUE} ;# RIM37
define_pin OTHIO_pads[2]  A21 LVCMOS33 BIDIR {pullup TRUE} ;# RIM40
define_pin OTHIO_pads[3]  A19 LVCMOS33 BIDIR {pullup TRUE} ;# RIM41
define_pin OTHIO_pads[4]  A18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM42
define_pin OTHIO_pads[5]  B17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM43
define_pin OTHIO_pads[6]  B16 LVCMOS33 BIDIR {pullup TRUE} ;# RIM44
define_pin OTHIO_pads[7]  A17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM45
define_pin OTHIO_pads[8]  A16 LVCMOS33 BIDIR {pullup TRUE} ;# RIM46
define_pin OTHIO_pads[9]  C18 LVCMOS33 BIDIR {pullup TRUE} ;# RIM47
define_pin OTHIO_pads[10] C17 LVCMOS33 BIDIR {pullup TRUE} ;# RIM48
define_pin OTHIO_pads[11] Y8  LVCMOS33 BIDIR {pullup TRUE} ;# RIM114
define_pin OTHIO_pads[12] Y9  LVCMOS33 BIDIR {pullup TRUE} ;# RIM115
define_pin OTHIO_pads[13] Y6  LVCMOS33 BIDIR {pullup TRUE} ;# RIM135
