`timescale 1ns / 1ps

// GPIO outputs from CPU
`define GPIO_EMIO_ETH0_PHYRESET_B    0
`define GPIO_EMIO_USB0_OTG_EN_B      1  // power enable to USB_OTG connector
`define GPIO_EMIO_USB0_HOST_EN_B     2  // power enable to USB_HOST connector
`define GPIO_EMIO_USB0_VBUS_PWRFLT_FORCE_B 3  // manual force of power fault to USB IP block

// GPIO inputs to CPU
`define GPIO_EMIO_ETH0_CONFIG       32
`define GPIO_EMIO_USB0_OTG_OC_B     33  // over-current on USB_OTG connector
`define GPIO_EMIO_USB0_HOST_OC_B    34  // over-current on USB_HOST connector
`define GPIO_EMIO_USB0_PORT_INDCTL0 35  // port indicator from USB IP block
`define GPIO_EMIO_USB0_PORT_INDCTL1 36  // port indicator from USB IP block
`define GPIO_EMIO_USB0_VBUS_PWRSEL  37  // power selection from USB IP block
`define GPIO_EMIO_USB0_VBUS_PWRFLT  38  // power fault as it is presented to USB IP block
  
module main(
        // 25MHz oscillator input
        input SYSCLK_pad,
       
        // peripheral pads    
        inout [53:0] MIO_pads,
    
        // DDR Bus   
        inout  DDR_Clk_p_pad,
        inout  DDR_Clk_n_pad,
        inout  DDR_CKE_pad,
        inout  DDR_CS_n_pad,
        inout  DDR_RAS_n_pad,
        inout  DDR_CAS_n_pad,    
        inout  DDR_VRN_pad,
        inout  DDR_VRP_pad,
        inout  DDR_ODT_pad,
        inout  DDR_DRSTB_pad,
        inout  [3:0]  DDR_DQS_n_pads,
        inout  [3:0]  DDR_DQS_p_pads,
        inout  [2:0]  DDR_BankAddr_pads,
        inout  [31:0] DDR_DQ_pads,
        inout  [3:0]  DDR_DM_pads,
        inout  [14:0] DDR_Addr_pads, 

        // it is strange, but these pads should not be defined here because they 
        // conflict with some (probably hardcoded) signals in PS IP core 
        //    output DDR_WEB_pad,
        //    input PS_SRSTB_pad,
        //    input PS_CLK_pad,
        //    input PS_PORB_pad,

        // ENET0 PHY signals
        input ETH0_PHYLED0_pad,
        input ETH0_PHYLED1_pad,
        input ETH0_PHYLED2_pad,
        input ETH0_PHYCLK125_pad,
        input ETH0_PHYCONFIG_pad,
        output ETH0_PHYRESET_B_pad,

        // miscellanous pins
        input BUTTON_pad,
        output RLED_pad,
        output LLED_pad,  
        inout TAMPER0_pad,
        inout TAMPER1_pad,
        input TOUCH_IRQ_pad,
        output TAP_pad,

        // GPIO J10 pins
        output [28:1] GPIO_pads,
        
        // ADIO pins (some of them on J10) 
        inout [7:0] ADIO_pads,
        
        // Display pins
        output GLCD_HSYNC_pad,
        output GLCD_VSYNC_pad,
        output GLCD_PCLK_pad,
        output GLCD_DE_pad,   
        output [7:0] GLCD_R_pads,
        output [7:0] GLCD_G_pads,
        output [7:0] GLCD_B_pads,
        output LCD_BACKLIGHT_pad,

        // I2S SGTL5000 pins also available on J2 
        inout I2S_TX_BCLK_pad,
        inout I2S_TXD0_pad,
        inout I2S_TX_FS_pad,
        inout I2S_MCLK_pad,
        inout I2S_RX_BCLK_pad,
        inout I2S_RXD0_pad,
        inout I2S_RX_FS_pad,

        // SPI0 EMIO pins - on display connector J2 
        inout SPI0_PCS0_pad,
        inout SPI0_SIN_pad,
        inout SPI0_SOUT_pad,
        inout SPI0_SCK_pad,

        // UART0 EMIO pins - RS485, EasyBoard J4
        input  UART0_RX_pad,
        output UART0_TX_pad,
        output UART0_RTS_pad,
        
        // UART1 is on MIO, modem signals on EMIO, EasyBoard J3  
        input  UART1_CTS_pad, 
        output UART1_RTS_pad, 
        
        // EasyBoard pins marked as UART2 are general IO pins (no UART2 on Zynq)   
        input UART2_CTS_pad,
        output UART2_RTS_pad,
        input UART2_RX_pad,
        output UART2_TX_pad,

        // EasyBoard USB0 and USB1 connectors are both accessed from USB0 Zynq Module 
        // USB0 is USB_OTG mini-AB connector on EasyBoard
        output USB0_EN_pad, // power enable (active-low)
        input  USB0_OC_pad, // overcurrent sensing (active-low)
        // USB1 is USB_HOST connector on EasyBoard
        output USB1_EN_pad, // power enable (active-low)
        input  USB1_OC_pad  // overcurrent sensing (active-low)
    );

    ///////////////////////////////////////////////////////////////////////
    // system clock 
   
    wire FCLK_CLK0;
    wire FCLK_RESET0_N;
    
    // counter incremented at PL clock 25MHz
    reg [31:0] tick_counter_pli = 0;
    always @ (posedge SYSCLK_pad)
        tick_counter_pli <= tick_counter_pli + 1;

    // counter incremented at PS clock 100MHz
    reg [31:0] tick_counter_psi = 0;
    always @ (posedge FCLK_CLK0)
        tick_counter_psi <= tick_counter_psi + 1;

    ///////////////////////////////////////////////////////////////////////
    // GPIO signals 

    wire [63:0] GPIO_EMIO_tri_i;
    wire [63:0] GPIO_EMIO_tri_o;
    wire [63:0] GPIO_EMIO_tri_t;

    // better GPIO output signals, they are driven 1 when pin is input (reset-default) 
    // the pins are normally driven as _o when pin is set as output
    wire [63:0] GPIO_EMIO_tri_o2 = GPIO_EMIO_tri_t | GPIO_EMIO_tri_o;  
    
    // example how to connect the GPIO to external pad
    /*IOBUF gpio_0_0_buf (
        .O(GPIO_EMIO_tri_i[x]),
        .I(GPIO_EMIO_tri_o[x]),
        .T(GPIO_EMIO_tri_t[x]),
        .IO(ADIO_pads[y])
    );*/

    ///////////////////////////////////////////////////////////////////////
    // ENET 
        
    // ethernet LEDs used for debugging
    assign LLED_pad = ~ETH0_PHYLED2_pad; // link
    assign RLED_pad = ~ETH0_PHYLED2_pad; // not connected anyway
    assign ETH0_PHYRESET_B_pad = GPIO_EMIO_tri_o2[`GPIO_EMIO_ETH0_PHYRESET_B];
    // output feedback to input
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_ETH0_PHYRESET_B] = ETH0_PHYRESET_B_pad;
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_ETH0_CONFIG] = ETH0_PHYCONFIG_pad; 

    ///////////////////////////////////////////////////////////////////////
    // USB 

    // GPIO outputs
    assign USB0_EN_pad = GPIO_EMIO_tri_o2[`GPIO_EMIO_USB0_OTG_EN_B];
    assign USB1_EN_pad = GPIO_EMIO_tri_o2[`GPIO_EMIO_USB0_HOST_EN_B];
    // USB power fault is active-high. The GPIO and OC_pads are active-low.
    wire usb0_vbus_pwrfault = ~(GPIO_EMIO_tri_o2[`GPIO_EMIO_USB0_VBUS_PWRFLT_FORCE_B] & USB0_OC_pad & USB1_OC_pad);
    // output feedback to input
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_OTG_EN_B] = USB0_EN_pad; 
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_HOST_EN_B] = USB1_EN_pad;
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_VBUS_PWRFLT_FORCE_B] =  GPIO_EMIO_tri_o2[`GPIO_EMIO_USB0_VBUS_PWRFLT_FORCE_B];
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_VBUS_PWRFLT] = usb0_vbus_pwrfault;
     
    // GPIO inputs
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_OTG_OC_B] = USB0_OC_pad;
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_HOST_OC_B] = USB1_OC_pad;
    wire [1:0] usb0_port_indctl;
    wire usb0_vbus_pwrsel;
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_PORT_INDCTL0] = usb0_port_indctl[0];
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_PORT_INDCTL1] = usb0_port_indctl[1];
    assign GPIO_EMIO_tri_i[`GPIO_EMIO_USB0_VBUS_PWRSEL] = usb0_vbus_pwrsel;

    ///////////////////////////////////////////////////////////////////////
    // UARTs

    wire uart0_txd = UART0_TX_pad;
    wire uart0_rxd = UART0_RX_pad;
    wire uart0_rtsn = UART0_RTS_pad;
    
    wire uart1_ctsn = UART1_CTS_pad;
    wire uart1_rtsn = UART1_RTS_pad;
    
    ///////////////////////////////////////////////////////////////////////
    // miscelanous signals
     
    assign TAP_pad = 1;
    assign UART1_RTS_pad = 1; 
    
    ///////////////////////////////////////////////////////////////////////
    // DISPLAY 
    
    wire lcd_hsync;
    wire lcd_vsync;
    wire lcd_dclk;
    wire lcd_de;
    wire [7:0] lcd_r;
    wire [7:0] lcd_g;
    wire [7:0] lcd_b;
    wire lcd_back;

    assign GLCD_HSYNC_pad = lcd_hsync;
    assign GLCD_VSYNC_pad = lcd_vsync;
    assign GLCD_PCLK_pad = lcd_dclk;
    assign GLCD_DE_pad = lcd_de;   
    assign GLCD_R_pads = lcd_r;
    assign GLCD_G_pads = lcd_g;
    assign GLCD_B_pads = lcd_b;
    assign LCD_BACKLIGHT_pad = lcd_back;
    
    ///////////////////////////////////////////////////////////////////////
    // OMEGA DEBUG
    wire [16:1] OMEGA;

    assign OMEGA[1] = ETH0_PHYLED0_pad; 
    assign OMEGA[2] = ETH0_PHYLED1_pad;
    assign OMEGA[3] = ETH0_PHYLED2_pad; 
    assign OMEGA[4] = ETH0_PHYCLK125_pad;
    assign OMEGA[5] = ETH0_PHYCONFIG_pad;
    assign OMEGA[6] = 0;
    assign OMEGA[7] = 0;
    assign OMEGA[8] = 0;    
    assign OMEGA[9] = 0;
    assign OMEGA[10] = 0; 
    assign OMEGA[11] = 0;
    assign OMEGA[12] = 0;
    assign OMEGA[13] = 0;
    assign OMEGA[14] = 0;
    assign OMEGA[15] = tick_counter_pli[0];
    assign OMEGA[16] = tick_counter_psi[0];

    // OMEGA connections
    assign GPIO_pads[8:1] = OMEGA[8:1];
    assign GPIO_pads[28:22] = OMEGA[16:10];
	
	// GPIO[9:17] is NC - used for ethernet MDI - version L only
	// GPIO[18] not exist
	// GPIO[21:19] used for ethernet 1 LEDs via J10
   
    // the processor system
    ps ps_i(
        // .FIXED_IO_ps_clk(PS_CLK_pad),
        // .FIXED_IO_ps_porb(PS_PORB_pad),
        // .FIXED_IO_ps_srstb(PS_SRSTB_pad,
        
        .FCLK_CLK0(FCLK_CLK0),
        .FCLK_RESET0_N(FCLK_RESET0_N),
        
        .DDR_addr(DDR_Addr_pads),
        .DDR_ba(DDR_BankAddr_pads),
        .DDR_cas_n(DDR_CAS_n_pad),
        .DDR_ck_n(DDR_Clk_n_pad),
        .DDR_ck_p(DDR_Clk_p_pad),
        .DDR_cke(DDR_CKE_pad),
        .DDR_cs_n(DDR_CS_n_pad),
        .DDR_dm(DDR_DM_pads),
        .DDR_dq(DDR_DQ_pads),
        .DDR_dqs_n(DDR_DQS_n_pads),
        .DDR_dqs_p(DDR_DQS_p_pads),
        .DDR_odt(DDR_ODT_pad),
        .DDR_ras_n(DDR_RAS_n_pad),
        .DDR_reset_n(DDR_DRSTB_pad),
        // .DDR_we_n(DDR_WEB_pad),

        .GPIO_EMIO_tri_i(GPIO_EMIO_tri_i),
        .GPIO_EMIO_tri_o(GPIO_EMIO_tri_o),
        .GPIO_EMIO_tri_t(GPIO_EMIO_tri_t),

        // USB EMIO signals
        .USBIND_0_port_indctl(usb0_port_indctl),
        .USBIND_0_vbus_pwrfault(usb0_vbus_pwrfault),
        .USBIND_0_vbus_pwrselect(usb0_vbus_pwrsel),
        
        // UART0 (RS485) on EMIO
        .UART_0_ctsn(0),
        .UART_0_rxd(uart0_rxd),
        .UART_0_dcdn(0),
        .UART_0_dsrn(0),
        .UART_0_ri(0),
        .UART_0_rtsn(uart0_rtsn),
        .UART_0_txd(uart0_txd), 
        //.UART_0_dtrn(unused)

        // UART1 (RS232) is on MIO, CTS/RTS signals on EMIO  
        .UART_1_ctsn(uart1_ctsn),
        .UART_1_dcdn(0),
        .UART_1_dsrn(0),
        .UART_1_ri(0),
        .UART_1_rtsn(uart1_rtsn),
        //.UART_1_dtrn(unused)

        .LCD_HSYNC(lcd_hsync),
        .LCD_VSYNC(lcd_vsync),
        .LCD_DCLK(lcd_dclk),
        .LCD_DE(lcd_de),
        .LCD_R(lcd_r),
        .LCD_G(lcd_g),
        .LCD_B(lcd_b),
        .LCD_BACK(lcd_back),

        .FIXED_IO_ddr_vrn(DDR_VRN_pad),
        .FIXED_IO_ddr_vrp(DDR_VRP_pad),
        .FIXED_IO_mio(MIO_pads)
    );    
    
endmodule
