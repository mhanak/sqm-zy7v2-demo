# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "Component_Name" -parent ${Page_0}


}

proc update_PARAM_VALUE.STAGE_FP { PARAM_VALUE.STAGE_FP } {
	# Procedure called to update STAGE_FP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STAGE_FP { PARAM_VALUE.STAGE_FP } {
	# Procedure called to validate STAGE_FP
	return true
}

proc update_PARAM_VALUE.STAGE_DE { PARAM_VALUE.STAGE_DE } {
	# Procedure called to update STAGE_DE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STAGE_DE { PARAM_VALUE.STAGE_DE } {
	# Procedure called to validate STAGE_DE
	return true
}

proc update_PARAM_VALUE.STAGE_BP { PARAM_VALUE.STAGE_BP } {
	# Procedure called to update STAGE_BP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STAGE_BP { PARAM_VALUE.STAGE_BP } {
	# Procedure called to validate STAGE_BP
	return true
}


proc update_MODELPARAM_VALUE.STAGE_BP { MODELPARAM_VALUE.STAGE_BP PARAM_VALUE.STAGE_BP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STAGE_BP}] ${MODELPARAM_VALUE.STAGE_BP}
}

proc update_MODELPARAM_VALUE.STAGE_DE { MODELPARAM_VALUE.STAGE_DE PARAM_VALUE.STAGE_DE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STAGE_DE}] ${MODELPARAM_VALUE.STAGE_DE}
}

proc update_MODELPARAM_VALUE.STAGE_FP { MODELPARAM_VALUE.STAGE_FP PARAM_VALUE.STAGE_FP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STAGE_FP}] ${MODELPARAM_VALUE.STAGE_FP}
}

