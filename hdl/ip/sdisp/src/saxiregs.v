`timescale 1ns/1ps

// this is a S_AXI to REGISTERs decoder

module s_axi_lite_registers #(
      parameter integer REG_DATA_WIDTH = 32,  // can be 32 or 64
      parameter integer REG_INDEX_WIDTH = 4   // 4 means 2^4 registers (16x4=64 byte space for 32bit registers)
    ) (
    // System Signals 
    input S_AXI_ACLK,      // AXI clock signal
    input S_AXI_ARESETN,   // AXI active low reset signal

    // Slave Interface Write Address channel Ports
    input  [REG_INDEX_WIDTH+2:0] S_AXI_AWADDR, // Write address issued by master, accepted by Slave. Assuming 64bit register at worst
    input  S_AXI_AWVALID, // Write address valid. This signal indicates that the master signaling valid write address and control information.
    output S_AXI_AWREADY, // Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    // Slave Interface Write Data channel Ports
    input  [REG_DATA_WIDTH-1:0]   S_AXI_WDATA, // Write data issued by master, accepted by Slave. Assuming 64bit register at worst
    input  [REG_DATA_WIDTH/8-1:0] S_AXI_WSTRB, // Write strobes. This signal indicates which byte lanes hold valid data. There is one write strobe bit for each byte
    input  S_AXI_WVALID, // Write valid. This signal indicates that valid write data and strobes are available.
    output S_AXI_WREADY, // Write ready. This signal indicates that the slave can accept the write data.

    // Slave Interface Write Response channel Ports
    output [1:0] S_AXI_BRESP, // Write response. This signal indicates the status of the write transaction.
    output S_AXI_BVALID, // Write response valid. This signal indicates that the channel is signaling a valid write response.
    input  S_AXI_BREADY, // Response ready. This signal indicates that the master can accept a write response.

    // Slave Interface Read Address channel Ports
    input  [REG_INDEX_WIDTH+2:0] S_AXI_ARADDR, // Read address issued by master, accepted by Slave.
    input  S_AXI_ARVALID, // Read address valid. This signal indicates that the channel is signaling valid read address and control information.
    output S_AXI_ARREADY, // Read address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    // Slave Interface Read Data channel Ports
    output [REG_DATA_WIDTH-1:0] S_AXI_RDATA, // Read data issued by slave.
    output [1:0]  S_AXI_RRESP, // Read response. This signal indicates the status of the read transfer.
    output S_AXI_RVALID, // Read valid. This signal indicates that the channel is signaling the required read data.
    input  S_AXI_RREADY, // Read ready. This signal indicates that the master can accept the read data and response information.
    
    // decoded register access signals
    output [REG_INDEX_WIDTH-1:0] REG_RINDEX,
    input  [REG_DATA_WIDTH-1:0]  REG_RDATA,
    output [REG_INDEX_WIDTH-1:0] REG_WINDEX,
    output [REG_DATA_WIDTH-1:0]  REG_WDATA,
    output REG_WRITE,
    output REG_READ
    );

    ////////////////////////////////////////////////////
    // function clogb2 returns an integer which has the
    // value of the ceiling of the log base 2
    //     clogb2(0) = 0
    //     clogb2(1) = 1
    //     clogb2(2)..clogb(3)  = 2
    //     clogb2(4)..clogb(7)  = 3
    //     clogb2(8)..clogb(15) = 4
    //     ...
    
    function integer clogb2(input integer bd);
        integer bit_depth;
        begin
            bit_depth = bd;
            for(clogb2=0; bit_depth>0; clogb2=clogb2+1)
                bit_depth = bit_depth >> 1;
        end
    endfunction
    
    ////////////////////////////////////////////////////////////////////////////
    // internal parameter used in the code 
    localparam integer DATA_BITS  = REG_DATA_WIDTH; 
    localparam integer DATA_BYTES = DATA_BITS/8;
    localparam integer ADDR_LSB   = 2;//clogb2(DATA_BITS/8)-1;      // 2 for 32 bit data, 3 for 64 bit data
    localparam integer ADDR_MSB   = ADDR_LSB+REG_INDEX_WIDTH-1;
    localparam integer ADDR_BITS  = ADDR_MSB+1;  

    ////////////////////////////////////////////////////////////////////////////
    // AXI4 Lite internal signals
    reg [1:0] axi_rresp = 0; // read response
    reg [1:0] axi_bresp = 0; // write response
    reg axi_awready = 0;     // write address acceptance
    reg axi_wready = 0;      // write data acceptance
    reg axi_bvalid = 0;      // write response valid
    reg axi_rvalid = 0;      // read data valid
    reg axi_arready = 0;     // read address acceptance
    reg [ADDR_MSB:0] axi_awaddr = 0;   // write address
    reg [ADDR_MSB:0] axi_araddr = 0;   // read address
    reg [DATA_BITS-1:0] axi_rdata = 0; // read data

    ////////////////////////////////////////////////////////////////////////////
    // register decoding
    wire slv_reg_rden; // Slave register read enable
    wire slv_reg_wren; // Slave register write enable
    wire [DATA_BITS-1:0] reg_data_out; // register read data

    ////////////////////////////////////////////////////////////////////////////
    // AXI I/O Connections assignments
    assign S_AXI_AWREADY = axi_awready; // Write Address Ready (AWREADY)
    assign S_AXI_WREADY  = axi_wready;  // Write Data Ready(WREADY)
    assign S_AXI_BRESP   = axi_bresp;   // Write Response (BResp)
    assign S_AXI_BVALID  = axi_bvalid;  // Write Response valid (BVALID)
    assign S_AXI_ARREADY = axi_arready; // Read Address Ready(AREADY)
    assign S_AXI_RDATA   = axi_rdata;   // Read and Read Data (RDATA)
    assign S_AXI_RVALID  = axi_rvalid;  // Read Valid (RVALID)
    assign S_AXI_RRESP   = axi_rresp;   // Read Response (RRESP)

    ////////////////////////////////////////////////////////////////////////////
    // Implement axi_awready generation
    //
    //  axi_awready is asserted for one S_AXI_ACLK clock cycle when both
    //  S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is de-asserted 
    // when reset is low. This design expects no outstanding transactions.
    
    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
            axi_awready <= 1'b0;
        else
            axi_awready <= (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID) ? 1'b1 : 1'b0; 
    end

    ////////////////////////////////////////////////////////////////////////////
    // Implement axi_wready generation
    //
    //  axi_wready is asserted for one S_AXI_ACLK clock cycle when both
    //  S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is
    //  de-asserted when reset is low. Slave is ready to accept write data when
    //  there is a valid write address and write data on the write address and data 
    //  bus. This design expects no outstanding transactions.
    
    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
            axi_wready <= 1'b0;
        else 
            axi_wready <= (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID) ? 1'b1 : 1'b0;
    end

    ////////////////////////////////////////////////////////////////////////////
    // Implement axi_awaddr latching
    //
    //  This process is used to latch the address when both
    //  S_AXI_AWVALID and S_AXI_WVALID are valid. 
    
    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
            axi_awaddr <= 0;
        else if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
            axi_awaddr <= S_AXI_AWADDR; // latch address
    end

    ////////////////////////////////////////////////////////////////////////////
    // Implement memory mapped register select and write logic generation
    // 
    //  The write data is accepted and written to memory mapped
    //  registers when axi_wready, S_AXI_WVALID, axi_awready and S_AXI_AWVALID are asserted. 
    //  Write strobes are used to select byte enables of slave registers while writing.
    //  These registers are cleared when reset (active low) is applied.
    
    assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;
    
    // give our outer module the register index 
    assign REG_WINDEX[REG_INDEX_WIDTH-1:0] = axi_awaddr[ADDR_MSB:ADDR_LSB];

    // register write data
    assign REG_WDATA = S_AXI_WDATA; 

    // register write request, only when all bytes are valid 
    // we do not support partial register write to keep our interface simple
    assign REG_WRITE = slv_reg_wren & 
        ( S_AXI_WSTRB[DATA_BYTES-1:0] == { DATA_BYTES{1'b1} } ? 1'b1 : 1'b0  ) & 
        ( axi_awaddr[ADDR_LSB-1:0] == 0 ? 1 : 0 );

    ////////////////////////////////////////////////////////////////////////////
    // Implement write response logic generation
    //
    //  The write response and response valid signals are asserted by the slave
    //  when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
    //  This marks the acceptance of address and indicates the status of
    //  write transaction. No support for ERROR results.
    
    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
        begin
            axi_bvalid <= 1'b0;
            axi_bresp  <= 2'b00;
        end
        else if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
        begin
            axi_bvalid <= 1'b1;   // indicates a valid write response is available
            axi_bresp  <= 2'b00;  // 'OKAY' response
        end
        else if (S_AXI_BREADY && axi_bvalid)
        begin
            // check if bready is asserted while bvalid is high
            // (there is a possibility that bready is always asserted high)
            axi_bvalid <= 1'b0;
      end
    end

    ////////////////////////////////////////////////////////////////////////////
    // Implement axi_arready generation
    //
    //  axi_arready is asserted for one S_AXI_ACLK clock cycle when
    //  S_AXI_ARVALID is asserted. axi_awready is
    //  de-asserted when reset (active low) is asserted.
    //  The read address is also latched when S_AXI_ARVALID is
    //  asserted. axi_araddr is reset to zero on reset assertion.
    
    always @( posedge S_AXI_ACLK )
    begin
        if (S_AXI_ARESETN == 1'b0)
        begin
            axi_arready <= 1'b0;
            axi_araddr  <= { ADDR_BITS{1'b0} };
        end
        else if (~axi_arready && S_AXI_ARVALID)
        begin
            axi_arready <= 1'b1;
            axi_araddr  <= S_AXI_ARADDR; // latch the address
        end
        else
        begin
            axi_arready <= 1'b0;
        end
    end

    ////////////////////////////////////////////////////////////////////////////
    // Implement memory mapped register select and read logic generation
    //
    //  axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both
    //  S_AXI_ARVALID and axi_arready are asserted. The slave registers
    //  data are available on the axi_rdata bus at this instance. The
    //  assertion of axi_rvalid marks the validity of read data on the
    //  bus and axi_rresp indicates the status of read transaction.axi_rvalid
    //  is deasserted on reset (active low). axi_rresp and axi_rdata are
    //  cleared to zero on reset (active low).
    
    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
        begin
            axi_rvalid <= 0;
            axi_rresp  <= 0;
        end
        else if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
        begin
            axi_rvalid <= 1'b1;  // Valid read data is available at the read data bus
            axi_rresp  <= 2'b0;  // 'OKAY' response
        end
        else if (axi_rvalid && S_AXI_RREADY)
        begin
            // Read data is accepted by the master
            axi_rvalid <= 1'b0;
        end
    end

    ////////////////////////////////////////////////////////////////////////////
    // Slave register read enable is asserted when valid address is available
    // and the slave is ready to accept the read address.
    assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;

    // single clock pulse to notify our outer module that read data were latched
    // (it is single clock thanks to that axi_arready is single clock)
    assign REG_READ = slv_reg_rden;

    // give our outer module the register address aligned to register size 
    assign REG_RINDEX[REG_INDEX_WIDTH-1:0] = axi_araddr[ADDR_MSB:ADDR_LSB];

    // it is outer module responsibility to drive proper REG_RDATA after decoding REG_RADDR
    assign reg_data_out = S_AXI_ARESETN == 1'b0 ? { DATA_BITS{1'b0} } : REG_RDATA;

    ////////////////////////////////////////////////////////////////////////////
    // When there is a valid read address (S_AXI_ARVALID) with
    // acceptance of read address by the slave (axi_arready),
    // output the read dada
    
    always @( posedge S_AXI_ACLK )
    begin
        if (S_AXI_ARESETN == 1'b0)
            axi_rdata  <= 0;
        else if (slv_reg_rden)
            axi_rdata <= reg_data_out;    // register read data
    end

endmodule 
 