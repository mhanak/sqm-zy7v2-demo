    reg aclk;
    reg aresetn;

    reg [31:0] axi_awaddr; //input Write address (issued by master, acceped by Slave)
    reg axi_awvalid; //input Write address valid. This signal indicates that the master signaling valid write address and control information.
    wire axi_awready; //output Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    reg [31:0] axi_wdata; //input Write data (issued by master, acceped by Slave)
    reg axi_wvalid; //input Write valid. This signal indicates that valid write data and strobes are available.
    wire axi_wready; //output Write ready. This signal indicates that the slave can accept the write data.

    wire [1:0] axi_bresp; //output [1:0] Write response. This signal indicates the status of the write transaction.
    wire axi_bvalid; //output Write response valid. This signal indicates that the channel is signaling a valid write response.
    reg axi_bready; //input Response ready. This signal indicates that the master can accept a write response.

    // Slave Interface Read Address channel Ports
    reg  [31:0] axi_araddr; // Read address (issued by master, acceped by Slave)
    reg  axi_arvalid; // Read address valid. This signal indicates that the channel is signaling valid read address and control information.
    wire axi_arready; // Read address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    // Slave Interface Read Data channel Ports
    wire [31:0] axi_rdata; // Read data (issued by slave)
    wire [1:0]  axi_rresp; // Read response. This signal indicates the status of the read transfer.
    wire axi_rvalid; // Read valid. This signal indicates that the channel is signaling the required read data.
    reg  axi_rready; // Read ready. This signal indicates that the master can accept the read data and response information.     
	
	
	
    reg start_single_write;
    reg write_issued;

	reg start_single_read;
    reg read_issued;

	
    // aclk 100MHz
    initial 
    begin
        aclk = 1'b0;
        forever 
        begin
            aclk = #5 ~aclk;
        end
    end

    // areset
    initial 
    begin
        aresetn = 1'b0;
        repeat (5) @(posedge aclk);
        #1;
        aresetn = 1'b1;
    end

    always @(posedge aclk) begin
        ////////////////////////////////////////////////////////////////////////////
        //Only VALID signals must be deasserted during reset per AXI spec
        //Consider inverting then registering active-low reset for higher fmax
        if (aresetn == 1'b0 ) begin
            axi_awvalid <= 1'b0;
        end else begin
            ////////////////////////////////////////////////////////////////////////////
            //Signal a new address/data command is available by user logic
            if (start_single_write) begin
                axi_awvalid <= 1'b1;
            end else if (axi_awready && axi_awvalid) begin
                ////////////////////////////////////////////////////////////////////////////
                //Address accepted by interconnect/slave (issue of M_AXI_AWREADY by slave)
                axi_awvalid <= 1'b0;
            end
        end
    end

    always @(posedge aclk) begin
        if (aresetn == 1'b0 ) begin
            axi_wvalid <= 1'b0;
        end else if (start_single_write) begin
            ////////////////////////////////////////////////////////////////////////////
            //Signal a new address/data command is available by user logic
            axi_wvalid <= 1'b1;
        end else if (axi_awready && axi_wvalid) begin
            ////////////////////////////////////////////////////////////////////////////
            //Data accepted by interconnect/slave (issue of M_AXI_WREADY by slave)
            axi_wvalid <= 1'b0;
        end
    end

    always @(posedge aclk) begin
        if (aresetn == 1'b0 ) begin
            axi_bready <= 1'b0;
        end else if (axi_bvalid && ~axi_bready) begin
            ////////////////////////////////////////////////////////////////////////////
            // accept/acknowledge bresp with axi_bready by the master
            // when M_AXI_BVALID is asserted by slave
            axi_bready <= 1'b1;
        end else if (axi_bready) begin
            ////////////////////////////////////////////////////////////////////////////
            // deassert after one clock cycle
            axi_bready <= 1'b0;
            //start_single_write <= 1'b0;
        end else begin
            ////////////////////////////////////////////////////////////////////////////
            // retain the previous value
            axi_bready <= axi_bready;
        end
    end
 
    always @(posedge aclk) begin
        if (aresetn == 1'b0 ) begin
            write_issued <= 1'b0;
            start_single_write <= 1'b0;
        end else if (start_single_write && ~write_issued) begin
            write_issued  <= 1'b1;
        end else if (start_single_write && write_issued) begin
            start_single_write <= 1'b0;
        end else if (~start_single_write && write_issued && axi_bready) begin
            write_issued  <= 1'b0;
        end
    end
	
////////////////////////////////
// read

    always @(posedge aclk) begin
        ////////////////////////////////////////////////////////////////////////////
        //Only VALID signals must be deasserted during reset per AXI spec
        //Consider inverting then registering active-low reset for higher fmax
        if (aresetn == 1'b0 ) begin
            axi_arvalid <= 1'b0;
        end else begin
            ////////////////////////////////////////////////////////////////////////////
            //Signal a new address/data command is available by user logic
            if (start_single_read) begin
                axi_arvalid <= 1'b1;
            end else if (axi_arready && axi_rvalid) begin
                ////////////////////////////////////////////////////////////////////////////
                //Address accepted by interconnect/slave (issue of M_AXI_ARREADY by slave)
                axi_arvalid <= 1'b0;
            end
        end
    end

	always @(posedge aclk) begin
		if (aresetn == 1'b0 ) begin
			axi_rready <= 1'b0;
		end else begin
			axi_rready <= 1'b1;
		end
	end

    always @(posedge aclk) begin
        if (aresetn == 1'b0 ) begin
            read_issued <= 1'b0;
            start_single_read <= 1'b0;
        end else if (start_single_read && ~read_issued) begin
            read_issued  <= 1'b1;
        end else if (start_single_read && read_issued) begin
            start_single_read <= 1'b0;
        end else if (~start_single_read && read_issued && axi_rvalid) begin
            read_issued  <= 1'b0;
        end
    end

	