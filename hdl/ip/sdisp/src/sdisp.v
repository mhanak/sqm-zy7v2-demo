
`timescale 1ps/1ps

`include "sdisp.vh"

module sdisp (
    /////////////////////////////////////////////////////////////////////////////////////////
    // AXI SLAVE BUS for register control

    // System Signals
    input S_AXI_ACLK,      // AXI clock signal
    input S_AXI_ARESETN,   // AXI active low reset signal

    // Slave Interface Write Address channel Ports
    input  [`S_AXI_ADDR_WIDTH-1:0] S_AXI_AWADDR, // Write address (issued by master, accepted by Slave)
    input  S_AXI_AWVALID, // Write address valid. This signal indicates that the master signaling valid write address and control information.
    output S_AXI_AWREADY, // Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    // Slave Interface Write Data channel Ports
    input  [`S_AXI_DATA_WIDTH-1:0]   S_AXI_WDATA, // Write data (issued by master, accepted by Slave)
    input  [`S_AXI_DATA_WIDTH/8-1:0] S_AXI_WSTRB, // Write strobes. This signal indicates which byte lanes hold valid data. There is one write strobe bit for each byte
    input  S_AXI_WVALID, // Write valid. This signal indicates that valid write data and strobes are available.
    output S_AXI_WREADY, // Write ready. This signal indicates that the slave can accept the write data.

    // Slave Interface Write Response channel Ports
    output [1:0] S_AXI_BRESP, // Write response. This signal indicates the status of the write transaction.
    output S_AXI_BVALID, // Write response valid. This signal indicates that the channel is signaling a valid write response.
    input  S_AXI_BREADY, // Response ready. This signal indicates that the master can accept a write response.

    // Slave Interface Read Address channel Ports
    input  [`S_AXI_ADDR_WIDTH-1:0] S_AXI_ARADDR, // Read address (issued by master, accepted by Slave)
    input  S_AXI_ARVALID, // Read address valid. This signal indicates that the channel is signaling valid read address and control information.
    output S_AXI_ARREADY, // Read address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

    // Slave Interface Read Data channel Ports
    output [`S_AXI_DATA_WIDTH-1:0] S_AXI_RDATA, // Read data (issued by slave)
    output [1:0]  S_AXI_RRESP, // Read response. This signal indicates the status of the read transfer.
    output S_AXI_RVALID, // Read valid. This signal indicates that the channel is signaling the required read data.
    input  S_AXI_RREADY, // Read ready. This signal indicates that the master can accept the read data and response information.

    /////////////////////////////////////////////////////////////////////////////////////////
    // DISPLAY signals

    output LCD_HSYNC,
    output LCD_VSYNC,
    output LCD_DCLK,
    output LCD_DE,
    output LCD_BACK,
    output [7:0] LCD_R,
    output [7:0] LCD_G,
    output [7:0] LCD_B
    );

    ////////////////////////////////////////////////////////////////////////////
    // s/w accessible registers
    reg [31:0] r_ctrl = `REG_CTRL_INIT;
    wire [7:0] r_ctrl_fdiv = r_ctrl[`REGBIT_CTRL_FDIV_MSB:`REGBIT_CTRL_FDIV_LSB];
    wire       r_ctrl_ena = r_ctrl[`REGBIT_CTRL_ENA];
    wire       r_ctrl_inv = r_ctrl[`REGBIT_CTRL_INV];

    reg [31:0] r_hsync = `REG_HSYNC_INIT;
    wire [15:0] r_hsync_res = r_hsync[`REGBIT_HSYNC_RES_MSB:`REGBIT_HSYNC_RES_LSB];
    wire [15:0] r_hsync_len = r_hsync[`REGBIT_HSYNC_LEN_MSB:`REGBIT_HSYNC_LEN_LSB];
    wire [15:0] r_hsync_bp = r_hsync[`REGBIT_HSYNC_BP_MSB:`REGBIT_HSYNC_BP_LSB];
    wire [15:0] r_hsync_fp = r_hsync[`REGBIT_HSYNC_FP_MSB:`REGBIT_HSYNC_FP_LSB];

    reg [31:0] r_vsync = `REG_VSYNC_INIT;
    wire [15:0] r_vsync_res = r_vsync[`REGBIT_VSYNC_RES_MSB:`REGBIT_VSYNC_RES_LSB];
    wire [15:0] r_vsync_len = r_vsync[`REGBIT_VSYNC_LEN_MSB:`REGBIT_VSYNC_LEN_LSB];
    wire [15:0] r_vsync_bp = r_vsync[`REGBIT_VSYNC_BP_MSB:`REGBIT_VSYNC_BP_LSB];
    wire [15:0] r_vsync_fp = r_vsync[`REGBIT_VSYNC_FP_MSB:`REGBIT_VSYNC_FP_LSB];

    reg [31:0] r_bkgnd = `REG_BKGND_INIT;
    wire [7:0] r_bkgnd_r = r_bkgnd[`REGBIT_BGKND_R_MSB:`REGBIT_BGKND_R_LSB];
    wire [7:0] r_bkgnd_g = r_bkgnd[`REGBIT_BGKND_G_MSB:`REGBIT_BGKND_G_LSB];
    wire [7:0] r_bkgnd_b = r_bkgnd[`REGBIT_BGKND_B_MSB:`REGBIT_BGKND_B_LSB];

    // fake register to output internal status
    wire [31:0] r_status;

    ////////////////////////////////////////////////////////////////////////////
    // register AXI decoder

    wire [`REG_INDEX_WIDTH-1:0] reg_rindex;
    wire [`REG_INDEX_WIDTH-1:0] reg_windex;
    wire [31:0] reg_rdata;
    wire [31:0] reg_wdata;
    wire reg_write;
    wire reg_read; // unused here

    s_axi_lite_registers #(.REG_INDEX_WIDTH(`REG_INDEX_WIDTH)) registers_i(
        .S_AXI_ACLK(S_AXI_ACLK),      .S_AXI_ARESETN(S_AXI_ARESETN),
        .S_AXI_AWADDR(S_AXI_AWADDR),  .S_AXI_AWVALID(S_AXI_AWVALID), .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WDATA(S_AXI_WDATA),    .S_AXI_WSTRB(S_AXI_WSTRB),     .S_AXI_WVALID(S_AXI_WVALID),
        .S_AXI_WREADY(S_AXI_WREADY),  .S_AXI_BRESP(S_AXI_BRESP),     .S_AXI_BVALID(S_AXI_BVALID),
        .S_AXI_BREADY(S_AXI_BREADY),  .S_AXI_ARADDR(S_AXI_ARADDR),   .S_AXI_ARVALID(S_AXI_ARVALID),
        .S_AXI_ARREADY(S_AXI_ARREADY),.S_AXI_RDATA(S_AXI_RDATA),     .S_AXI_RRESP(S_AXI_RRESP),
        .S_AXI_RVALID(S_AXI_RVALID),  .S_AXI_RREADY(S_AXI_RREADY),
        .REG_RINDEX(reg_rindex), .REG_RDATA(reg_rdata),
        .REG_WINDEX(reg_windex), .REG_WDATA(reg_wdata),
        .REG_WRITE(reg_write), .REG_READ(reg_read)
      );

    ////////////////////////////////////////////////////////////////////////////
    // register write

    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 )
        begin
            r_ctrl <= `REG_CTRL_INIT;
            r_hsync <= `REG_HSYNC_INIT;
            r_vsync <= `REG_VSYNC_INIT;
            r_bkgnd <= `REG_BKGND_INIT;
        end
        else
        begin
            if (reg_write)
            begin
                case(reg_windex)
                    `REG_CTRL:  r_ctrl <= reg_wdata;
                    `REG_HSYNC: r_hsync <= reg_wdata;
                    `REG_VSYNC: r_vsync <= reg_wdata;
                    `REG_BKGND: r_bkgnd <= reg_wdata;
                endcase
            end
            else
            begin
              // automated register writes here...
            end
        end
    end

    ////////////////////////////////////////////////////////////////////////////
    // register read

    assign reg_rdata =
      reg_rindex == `REG_CTRL  ? r_ctrl  :
      reg_rindex == `REG_STATUS ? r_status :
      reg_rindex == `REG_HSYNC ? r_hsync :
      reg_rindex == `REG_VSYNC ? r_vsync :
      reg_rindex == `REG_BKGND ? r_bkgnd : 32'hdeaf_beef;

    ////////////////////////////////////////////////////////////////////
    //  user logic

    parameter integer STAGE_BP = 0;
    parameter integer STAGE_DE = 1;
    parameter integer STAGE_FP = 2;

    ////////////////////////////////////////////////////////////////////
    // generating display clock
    reg [7:0] fdiv_counter = 0; // counting 0...r_ctrl_fdiv

    always @ (posedge S_AXI_ACLK)
    begin
      if(r_ctrl_ena && fdiv_counter < r_ctrl_fdiv)
          fdiv_counter <= fdiv_counter+1;
      else
          fdiv_counter <= 0;
    end

    wire [7:0] dclk_half = (r_ctrl_fdiv+9'h1)/2;
    wire dclk_out  = fdiv_counter < dclk_half;       // DCLK signal (ACLK divided by FDIV+1)
    wire dclk_last = fdiv_counter == r_ctrl_fdiv;    // last ACLK pulse from the DCLK period

    ////////////////////////////////////////////////////////////////////
    // generating HSYNC phases
    reg  [1:0]  hstage = 0;
    reg  [11:0] hcounter = 0;
    wire hsync_out = (hstage == STAGE_BP && hcounter <= r_hsync_len) ? 1 : 0;
    wire hsync_last = (dclk_last && hstage == STAGE_FP && hcounter >= r_hsync_fp) ? 1 : 0;

    always @ (posedge S_AXI_ACLK)
    begin
        if(S_AXI_ARESETN == 1'b0)
        begin
            hstage <= STAGE_BP;
            hcounter <= 0;
        end
        else
        begin
            if(dclk_last)
            begin
                case(hstage)
                    STAGE_BP:
                    begin
                      if(hcounter >= r_hsync_bp)
                      begin
                          hcounter <= 0;
                          hstage <= STAGE_DE;
                      end
                      else
                      begin
                          hcounter <= hcounter+1;
                      end
                    end

                    STAGE_DE:
                    begin
                      if(hcounter >= r_hsync_res)
                      begin
                          hcounter <= 0;
                          hstage <= STAGE_FP;
                      end
                      else
                      begin
                          hcounter <= hcounter+1;
                      end
                    end

                    STAGE_FP:
                    begin
                      if(hcounter >= r_hsync_fp)
                      begin
                          hcounter <= 0;
                          hstage <= STAGE_BP;
                      end
                      else
                      begin
                          hcounter <= hcounter+1;
                      end
                    end
                endcase
            end
        end
    end

    ////////////////////////////////////////////////////////////////////
    // generating VSYNC phases
    reg  [1:0]  vstage = 0;
    reg  [11:0] vcounter = 0;
    wire vsync_out = (vstage == STAGE_BP && vcounter <= r_vsync_len) ? 1 : 0;
    wire vsync_last = (hsync_last && vstage == STAGE_FP && vcounter >= r_vsync_fp) ? 1 : 0;

    always @ (posedge S_AXI_ACLK)
    begin
        if(S_AXI_ARESETN == 1'b0)
        begin
            vstage <= STAGE_BP;
            vcounter <= 0;
        end
        else
        begin
            if(hsync_last)
            begin
                case(vstage)
                    STAGE_BP:
                    begin
                      if(vcounter >= r_vsync_bp)
                      begin
                          vcounter <= 0;
                          vstage <= STAGE_DE;
                      end
                      else
                      begin
                          vcounter <= vcounter+1;
                      end
                    end

                    STAGE_DE:
                    begin
                      if(vcounter >= r_vsync_res)
                      begin
                          vcounter <= 0;
                          vstage <= STAGE_FP;
                      end
                      else
                      begin
                          vcounter <= vcounter+1;
                      end
                    end

                    STAGE_FP:
                    begin
                      if(vcounter >= r_vsync_fp)
                      begin
                          vcounter <= 0;
                          vstage <= STAGE_BP;
                      end
                      else
                      begin
                          vcounter <= vcounter+1;
                      end
                    end
                endcase
            end
        end
    end

    ////////////////////////////////////////////////////////////////////
    // clocking out the data
    wire de_out = (hstage == STAGE_DE && vstage == STAGE_DE);

    wire chess = hcounter[5] ^ vcounter[5] ^ r_ctrl_inv;
    wire [7:0] red_out   = chess ? r_bkgnd_r : 8'h0;
    wire [7:0] green_out = chess ? r_bkgnd_g : 8'h0;
    wire [7:0] blue_out  = chess ? r_bkgnd_b : 8'h0;

    // status register
    assign r_status[`REGBIT_STAT_HCOUNTER_MSB:`REGBIT_STAT_HCOUNTER_LSB] = hcounter;
    assign r_status[`REGBIT_STAT_HSTAGE_MSB:`REGBIT_STAT_HSTAGE_LSB] = hstage;
    assign r_status[`REGBIT_STAT_VCOUNTER_MSB:`REGBIT_STAT_VCOUNTER_LSB] = vcounter;
    assign r_status[`REGBIT_STAT_VSTAGE_MSB:`REGBIT_STAT_VSTAGE_LSB] = vstage;
    assign r_status[`REGBIT_STAT_DCLK] = de_out;
    assign r_status[`REGBIT_STAT_DE] = de_out;

    // output signals
    assign LCD_HSYNC = ~hsync_out;
    assign LCD_VSYNC = ~vsync_out;
    assign LCD_DCLK = dclk_out;
    assign LCD_DE = de_out;
    assign LCD_R = red_out;
    assign LCD_G = green_out;
    assign LCD_B = blue_out;
    assign LCD_BACK = r_ctrl_ena;

endmodule
