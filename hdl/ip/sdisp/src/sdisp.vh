`ifndef __SDISP_H
`define __SDISP_H

// register addresses
`define REG_CTRL        0
`define REG_STATUS      1
`define REG_HSYNC       2
`define REG_VSYNC       3
`define REG_BKGND       4

`define REG_COUNT       8
`define REG_INDEX_WIDTH 3 // log2(REG_COUNT)-1

// SLAVE AXI parameters
`define S_AXI_DATA_WIDTH  32  // must be 32 
`define S_AXI_ADDR_WIDTH   7  // at least: REG_INDEX_WIDTH + log2(AXI_DATA_WIDTH)-1 

/* CTRL REGISTER layout
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |    
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| ENA| INV|         |                   |                   |                   |                                       |                  FDIV                 |
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
*/

`define REG_CTRL_INIT        32'h0000_000a  // (divide by 10+1=11 -> 9MHz)
`define REGBIT_CTRL_ENA         31
`define REGBIT_CTRL_INV         30
`define REGBIT_CTRL_FDIV_LSB    0
`define REGBIT_CTRL_FDIV_MSB    7

/* HSYNC REGISTER layout
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |    
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
|   Post sync (front porch in DCLK)     |    Pre-sync (back porch in DCLK)      | HSYNC (part of BP)|             HRES (display X resolution in pixels)         |
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
*/

`define REG_HSYNC_INIT   32'h08_2b_01e0 // FP=8, BP=43, RES=480
`define REGBIT_HSYNC_RES_LSB   0
`define REGBIT_HSYNC_RES_MSB   11  // horizontal resolution
`define REGBIT_HSYNC_LEN_LSB   12
`define REGBIT_HSYNC_LEN_MSB   15  // HSYNC length in clocks (counted within back porch)
`define REGBIT_HSYNC_BP_LSB    16
`define REGBIT_HSYNC_BP_MSB    23  // total Back Porch in clocks (includes HSYNC len)  
`define REGBIT_HSYNC_FP_LSB    24                                                      
`define REGBIT_HSYNC_FP_MSB    31  // total Front Porch in clocks                      

/* VSYNC REGISTER layout
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |    
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
|   Post sync (front porch in HSYNCs)   |    Pre-sync (back porch in HSYNCs)    | VSYNC (part of BP)|              VRES (display Y resolution in pixels)        |
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
*/

`define REG_VSYNC_INIT   32'h04_0c_0110 // FP=4, BP=12, RES=272
`define REGBIT_VSYNC_RES_LSB   0
`define REGBIT_VSYNC_RES_MSB   11  // vertical resolution 
`define REGBIT_VSYNC_LEN_LSB   12
`define REGBIT_VSYNC_LEN_MSB   15  // VSYNC length in HSYNCs (counted within back porch)
`define REGBIT_VSYNC_BP_LSB    16
`define REGBIT_VSYNC_BP_MSB    23  // total Back Porch in HSYNCs (includes VSYNC len)
`define REGBIT_VSYNC_FP_LSB    24
`define REGBIT_VSYNC_FP_MSB    31  // total Front Porch in HSYNCs

/* BACK-COLOR REGISTER layout
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |    
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
|                                       |                   BLUE                |                   GREEN               |                   RED                 |
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
*/

`define REG_BKGND_INIT   32'h0000_8000
`define REGBIT_BGKND_R_LSB   0
`define REGBIT_BGKND_R_MSB   7
`define REGBIT_BGKND_G_LSB   8
`define REGBIT_BGKND_G_MSB   15
`define REGBIT_BGKND_B_LSB   16
`define REGBIT_BGKND_B_MSB   23

/* STATUS REGISTER layout
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |    
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
|         | VSTAGE  |                     VCOUNTER                              |CLK | DE | HSTAGE  |                     HCOUNTER                              |
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
*/

`define REGBIT_STAT_HCOUNTER_LSB  0
`define REGBIT_STAT_HCOUNTER_MSB 11
`define REGBIT_STAT_HSTAGE_LSB   12
`define REGBIT_STAT_HSTAGE_MSB   13
`define REGBIT_STAT_DCLK         14
`define REGBIT_STAT_DE           15
`define REGBIT_STAT_VCOUNTER_LSB 16
`define REGBIT_STAT_VCOUNTER_MSB 27
`define REGBIT_STAT_VSTAGE_LSB   28
`define REGBIT_STAT_VSTAGE_MSB   29


`endif
