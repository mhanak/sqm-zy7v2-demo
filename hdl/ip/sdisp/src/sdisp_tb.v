`timescale 1ns / 1ps

`include "sdisp.vh"

module sdisp_tb();

	//------------------------------------------------
	// this include provide:
	// - aclk ... clk signal with period 10 time unit (TU)
	// - aresetn ... reset signal during first 5 aclk period + 1TU
	// - axi-lite master funkcionality
    `include "sim_axilite_incl.v"

    // place for signal declarations
    reg [15:0] axis_tdata = 0;
    reg axis_tvalid = 0;

	// main statements - example
    initial
    begin
        wait(aresetn == 1'b1);	// wait for end of reset 

        #100;	// wait
        axi_awaddr = `REG_VSYNC<<2;	//	set addr
        axi_wdata = 32'h0 | 4<<`REGBIT_VSYNC_RES_LSB | 2<<`REGBIT_VSYNC_LEN_LSB | 4<<`REGBIT_VSYNC_BP_LSB | 2<<`REGBIT_VSYNC_FP_LSB;	// set data
        start_single_write = 1'b1;	// start write
        wait(start_single_write == 1'b0 && write_issued == 1'b0);	// wait for end of write

        #100;	// wait
        axi_awaddr = `REG_HSYNC<<2;	//	set addr
        axi_wdata = 32'h0 | 8<<`REGBIT_HSYNC_RES_LSB | 2<<`REGBIT_HSYNC_LEN_LSB | 4<<`REGBIT_HSYNC_BP_LSB | 2<<`REGBIT_HSYNC_FP_LSB;	// set data FP=8, BP=43, RES=480
        start_single_write = 1'b1;	// start write
        wait(start_single_write == 1'b0 && write_issued == 1'b0);	// wait for end of write

        #100;	// wait
        axi_awaddr = `REG_CTRL<<2;	//	set addr
        axi_wdata = `REG_CTRL_INIT | 32'd1<<`REGBIT_CTRL_ENA | 1<<`REGBIT_CTRL_INV;	// set data
        start_single_write = 1'b1;	// start write
        wait(start_single_write == 1'b0 && write_issued == 1'b0);	// wait for end of write

	end
	
	
	wire hsync;
	wire vsync;
	wire dclk;
	wire de;
	wire back;
	wire [7:0] r;
	wire [7:0] g;
	wire [7:0] b;
	
	sdisp sdisp_i(
		/////////////////////////////////////////////////////////////////////////////////////////
		// AXI SLAVE BUS for register control 
		
		// System Signals 
		.S_AXI_ACLK(aclk),      // AXI clock signal
		.S_AXI_ARESETN(aresetn),   // AXI active low reset signal

		// Slave Interface Write Address channel Ports
		.S_AXI_AWADDR(axi_awaddr), // Write address (issued by master, acceped by Slave)
		.S_AXI_AWVALID(axi_awvalid), // Write address valid. This signal indicates that the master signaling valid write address and control information.
		.S_AXI_AWREADY(axi_awready), // Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

		// Slave Interface Write Data channel Ports
		.S_AXI_WDATA(axi_wdata), // Write data (issued by master, acceped by Slave)
		.S_AXI_WSTRB(4'b1111), // Write strobes. This signal indicates which byte lanes hold valid data. There is one write strobe bit for each byte
		.S_AXI_WVALID(axi_wvalid), // Write valid. This signal indicates that valid write data and strobes are available.
		.S_AXI_WREADY(axi_wready), // Write ready. This signal indicates that the slave can accept the write data.

		// Slave Interface Write Response channel Ports
		.S_AXI_BRESP(axi_bresp), // Write response. This signal indicates the status of the write transaction.
		.S_AXI_BVALID(axi_bvalid), // Write response valid. This signal indicates that the channel is signaling a valid write response.
		.S_AXI_BREADY(axi_bready), // Response ready. This signal indicates that the master can accept a write response.

		// Slave Interface Read Address channel Ports
		.S_AXI_ARADDR(0), // Read address (issued by master, acceped by Slave)
		.S_AXI_ARVALID(1'b0), // Read address valid. This signal indicates that the channel is signaling valid read address and control information.
		.S_AXI_ARREADY(), // Read address ready. This signal indicates that the slave is ready to accept an address and associated control signals.

		// Slave Interface Read Data channel Ports
		.S_AXI_RDATA(), // Read data (issued by slave)
		.S_AXI_RRESP(), // Read response. This signal indicates the status of the read transfer.
		.S_AXI_RVALID(), // Read valid. This signal indicates that the channel is signaling the required read data.
		.S_AXI_RREADY(1'b0), // Read ready. This signal indicates that the master can accept the read data and response information.     

		/////////////////////////////////////////////////////////////////////////////////////////
		// DISPLAY signals
		
		.LCD_HSYNC(hsync),
		.LCD_VSYNC(vsync),
		.LCD_DCLK(dclk),
		.LCD_DE(de),
		.LCD_BACK(back),
		.LCD_R(r), 
		.LCD_G(g), 
		.LCD_B(b) 
		);

endmodule		
		