/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief Network handling
*
***************************************************************************/

#include "main.h"
#include "shell.h"
#include "fapp_fs.h"
#include "xd_gic.h"


/************************************************************************/

static void duplicate_ip_handler(fnet_netif_desc_t netif)
{
    char name[FNET_NETIF_NAMELEN];
    char ip_str[FNET_IP4_ADDR_STR_SIZE];
    fnet_ip4_addr_t addr;

    fnet_netif_get_name( netif, name, FNET_NETIF_NAMELEN );
    addr = fnet_netif_get_ip4_addr( netif );
    fnet_inet_ntoa(*(struct in_addr *)( &addr), ip_str);

    fnet_printf("\n%s: %s has IP address conflict with another system on the network!\n", name, ip_str);
}

/************************************************************************/

void network_init(void)
{
    static unsigned char stack_heap[FNET_CFG_HEAP_SIZE];
    struct fnet_init_params init_params;

    /* FNET depends on Xilinx drivers - needs pointers to global drier objects */
    fnet_xc7z_gic = gic_get_low();

    /* Input parameters for FNET stack initialization */
    init_params.netheap_ptr = stack_heap;
    init_params.netheap_size = FNET_CFG_HEAP_SIZE;

    /* Add event handler on duplicated IP address */
#if FNET_CFG_IP4
    fnet_netif_dupip_handler_init(duplicate_ip_handler);
#endif

    /* Init FNET stack */
    if(fnet_init(&init_params) != FNET_ERR)
    {
         if(fnet_netif_get_default() == FNET_NULL)
            fnet_printf("Error: Default Network Interface is not configured!\n");

    #if FAPP_CFG_PARAMS_READ_FLASH
        /* During bootup, the most recently stored customer configuration data will be read and used to configure the interfaces.*/
        if(fapp_params_from_flash() == FNET_OK)
        {
            fnet_printf(FAPP_PARAMS_LOAD_STR);
        }
    #endif

    #if (FAPP_CFG_EXP_CMD && FNET_CFG_FS) || (FAPP_CFG_HTTP_CMD && FNET_CFG_HTTP)
        fapp_fs_mount(); /* Init FS and mount FS Image. */
    #endif
    }
    else
    {
        fnet_printf("Error: FNET stack not started\n");
    }
}

/* change MAC address by slot_id */

void network_set_mac(int netif_index, u32 seed)
{
    u8 mac[6];

    fnet_netif_desc_t netif_desc = fnet_netif_get_by_number(netif_index);

    if(netif_desc)
    {
        mac[0] = 0;
        mac[1] = 0x04;
        mac[2] = 0x9f;
        mac[3] = (seed >> 16) & 0xff;
        mac[4] = (seed >> 8) & 0xff;
        mac[5] = (seed & 0xff) + netif_index;

        fnet_netif_set_hw_addr(netif_desc, mac, 6);
    }
}

static void dhcp_on_updated(fnet_netif_desc_t netif, void *shl_desc)
{
    char name[16];

    fnet_shell_desc_t desc = (fnet_shell_desc_t) shl_desc;

    fnet_ip4_addr_t ip_addr;
    char ip_str[FNET_IP4_ADDR_STR_SIZE+1];

    //fnet_dhcp_handler_updated_set(0, 0);
    //fnet_dhcp_handler_discover_set(0, 0);

    fnet_netif_get_name(netif, name, sizeof(name));
    name[sizeof(name)-1] = 0;

    ip_addr = fnet_netif_get_ip4_addr(netif);
    fnet_shell_printf(desc, "%s:dhcp:%s/", name, fnet_inet_ntoa(*(struct in_addr *)(&ip_addr), ip_str) );

    ip_addr = fnet_netif_get_ip4_subnet_mask(netif);
    fnet_shell_printf(desc, "%s\n", fnet_inet_ntoa(*(struct in_addr *)(&ip_addr), ip_str) );
}

static void dhcp_on_discover(fnet_netif_desc_t netif, void *shl_desc)
{
    char name[16];

    fnet_shell_desc_t desc = (fnet_shell_desc_t) shl_desc;
    FNET_COMP_UNUSED_ARG(netif);

    fnet_netif_get_name(netif, name, sizeof(name));
    name[sizeof(name)-1] = 0;

    fnet_shell_printf(desc, "Probing %s dhcp...\n", name);
}

void net_run_dhcp(int netif_index)
{
    struct fnet_dhcp_params dhcp_params;
    fnet_netif_desc_t netif = fnet_netif_get_by_number(netif_index);

    fnet_memset_zero(&dhcp_params, sizeof(struct fnet_dhcp_params));

    if(fnet_dhcp_init(netif, &dhcp_params) != FNET_ERR)
    {
        /* Register DHCP event handlers. */
        fnet_dhcp_handler_updated_set(dhcp_on_updated, (void*)shell_desc);
        fnet_dhcp_handler_discover_set(dhcp_on_discover, (void*)shell_desc);
    }
    else
    {
        fnet_printf("Error starting DHCP service on interface %d\n", netif_index);
    }
}

