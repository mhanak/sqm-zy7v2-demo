/**************************************************************************
*
* Copyright 2013-2014 by ZD Rpety - DAKEL, Czech Republic
* 
* @author Michal Hanak
*
* @brief This file verifies and completes the user configuration in appcfg.h
*
***************************************************************************/

#ifndef __CONFIG_H
#define __CONFIG_H

#include "appcfg.h"

/*
 * Debug or Release build?
 */
#ifndef _DEBUG
	#ifndef _RELEASE
	#error You must define either _DEBUG or _RELEASE build in project or in appcfg.h
	#else
	#define _DEBUG (0)
	#endif
#endif
#ifndef _RELEASE
	#define _RELEASE !(_DEBUG)
#endif
#if _DEBUG && _RELEASE
	#error Both _DEBUG and _RELEASE are defined non zero in project or in appcfg.h
#endif

/*
 * Do not use inline in _DEBUG code
 */
#if _DEBUG
#define _inline
#else
#define _inline inline
#endif

#endif /* __CONFIG_H */
