/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief FNET Shell-related functions
*
***************************************************************************/

#ifndef __SHELL_H
#define __SHELL_H

void shell_init(void);

extern fnet_shell_desc_t shell_desc;

#endif /* __SHELL_H */
