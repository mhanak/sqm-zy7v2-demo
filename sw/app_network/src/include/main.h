/**************************************************************************
*
* Copyright 2013-2014 by ZD Rpety - DAKEL, Czech Republic
* 
* @author Michal Hanak
*
***************************************************************************/

#ifndef __MAIN_H
#define __MAIN_H

/* Xilinx basic support */
#include "xdlib.h"
#include "platform.h"
#include "xparameters.h"
#include "xparameters_ps.h"
#include "xil_cache.h"
#include <stdio.h>

/* bsp-specific settings, path is relative to auto-configured BSP include path */
#include "../../bspsettings.h"

/* our configuration */
#include "appcfg.h"
#include "config.h"

/* FNET is used almost everywhere */
#include "fnet.h"
#include "fapp.h"

void sh_rtc_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_temp_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_mma_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_log1_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_log2_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_can_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_flash_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_flashi_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
void sh_memtest_cmd(fnet_shell_desc_t desc, int argc, char ** argv);

#endif /* __MAIN_H */
