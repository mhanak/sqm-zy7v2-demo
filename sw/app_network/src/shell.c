/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
* 
* @brief ZEDO Shell initialization and uncategorized commands
*
***************************************************************************/

#include "main.h"
#include "shell.h"
#include "xd_cputime.h"

/************************************************************************
 * local prototypes
 */

static void sh_help_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
static void sh_time_cmd(fnet_shell_desc_t desc, int argc, char ** argv);
static void sh_ver_cmd(fnet_shell_desc_t desc, int argc, char ** argv);

static void shell_welcome(fnet_shell_desc_t desc);

/*****************************************************************************
* globals and statics
*/

fnet_shell_desc_t shell_desc = 0;   /* Shell descriptor. */
static char shell_line_buffer[1024];       /* Line buffer */

/***************************************************************************/ /*!
* @brief Shell startup callback
******************************************************************************/
static void shell_welcome(fnet_shell_desc_t desc)
{
    fnet_shell_printf(desc, "\nStarting shell...\n");
    sh_ver_cmd(desc, 0, NULL);
}

/***************************************************************************/ /*!
* @brief The "help" command implementation
******************************************************************************/
static void sh_help_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    fnet_shell_help(desc);
}

/***************************************************************************/ /*!
* @brief The "ver" command implementation
******************************************************************************/
static void sh_ver_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    fnet_shell_printf(desc, "\nBuild: " __DATE__ " " __TIME__ "; BSP: " BSP_NAME "\n");
}

/***************************************************************************/ /*!
* @brief The "time" command implementation
******************************************************************************/
static void sh_time_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    char buf[128];
    u64 ns = cputime_get_ns();

    fnet_shell_printf(desc, "System up time: %s\n", cputime_sprintf_ns_time(buf, ns, TIME_FMT_DHMS_NS));
    fnet_shell_printf(desc, "FNET time [ticks] = %u\n\n", fnet_timer_ticks());

    if(argc == 2)
    {
        u32 duration = 0;

        if(sscanf(argv[1], "%i", &duration) == 1 && duration <= 60)
        {
            u32 us_stop = (u32)(ns/1000) + duration*1000000UL;

            while(cputime_get_us32() < us_stop)
                fnet_shell_printf(desc, "System up time: %s\r", cputime_sprintf_ns_time(buf, cputime_get_ns(), TIME_FMT_DHMS_NS));

            fnet_shell_printf(desc, "\n\n");
        }
    }
}

/************************************************************************
 * Shell command table
 */

void net_check_link(fnet_shell_desc_t desc, int argc, char ** argv);

static const struct fnet_shell_command shell_table [] =
{
    { FNET_SHELL_CMD_TYPE_NORMAL, "help",       0, 0, (void *)sh_help_cmd, "This help message", ""},

    { FNET_SHELL_CMD_TYPE_NORMAL, "log1",       0, 0, (void *)sh_log1_cmd, "Dump non-overwriting log", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "log2",       0, 0, (void *)sh_log2_cmd, "Dump overwriting log", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "time",       0, 1, (void *)sh_time_cmd,      "Print system time", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "ver",        0, 0, (void *)sh_ver_cmd,       "Print version", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "rtc",        0, 1, (void *)sh_rtc_cmd,       "RTC test", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "temp",       0, 1, (void *)sh_temp_cmd,      "XADC Temperature Measurement", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "mma",        0, 1, (void *)sh_mma_cmd,       "MMA8451Q Test", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "can",        0, 1, (void *)sh_can_cmd,       "CAN Test", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "flash",      0, 5, (void *)sh_flash_cmd,     "QSPI Flash Test", ""},
    { FNET_SHELL_CMD_TYPE_NORMAL, "flashi",     0, 5, (void *)sh_flashi_cmd,     "QSPI Flash Test", ""},

    { FNET_SHELL_CMD_TYPE_NORMAL, "memtest",    0, 0, (void *)sh_memtest_cmd,   "DDR memory test (HANGS THE SYSTEM!)", ""},

    /* FNET shell is part of our shell (keep at the bottom, so it has the lowest priority) */
    { FNET_SHELL_CMD_TYPE_SHELL_FLAT, "fnet",        0, 0, (void *)&fapp_shell, "FNET shell commands", ""},
    { FNET_SHELL_CMD_TYPE_END,     0,           0, 0, 0, 0, 0}
};

const static struct fnet_shell zedo_shell =
{
    shell_table,     /* cmd_table */
    "demo> ",        /* prompt_str */
    shell_welcome,   /* shell_init */
};

/***************************************************************************/ /*!
* @brief Initialize FNET shell; the FNET should already be initialized before
******************************************************************************/
void shell_init(void)
{
    struct fnet_shell_params shell_params;

    /* Init main shell. */
    shell_params.shell = &zedo_shell;
    shell_params.cmd_line_buffer = shell_line_buffer;
    shell_params.cmd_line_buffer_size = sizeof(shell_line_buffer)-1;
    shell_params.stream = FNET_SERIAL_STREAM_DEFAULT;
    shell_params.echo = 1;

    if((shell_desc = fnet_shell_init(&shell_params)) == FNET_ERR)
        fnet_printf("Error: Shell not initialized\n");
}
