#include "main.h"

#ifndef BSP_NAME_ZY7V20U
#error This application can only run on ZY7V20U with USB device enabled
#endif

static XUsbPs usb_low;	/* The instance of the USB Controller low-level driver */

//void print(const char *str);
static void gpio_pins_init();
static void print_gpio_value(char* text, int pin);


int main()
{
	int i, status;

    init_platform();

    print("\r\nUSB Test\r\n");
    print("Build: " __DATE__ " " __TIME__ "; BSP: " BSP_NAME "\r\n");

	/* key SLCR initialization */
    print("XCPU init...");
	xcpu_init();

	/* reset PL peripherals */
    print("done\r\nFPGA reset assert...");
	xcpu_assert_fpga_reset(0xf);

	/* set OCM mapping (256kB OCM 0x00000-0x40000) */
    print("done\r\nOCM mapping...");
	xcpu_set_ocm_map(0, 0x100000, 0xffe00000, TRUE);
	/* interrupt controller */
    print("done\r\nGIC init...");
    gic_init();
    for(i=0; i<XSCUGIC_MAX_NUM_INTR_INPUTS; i++)
    	gic_disable(i);

    /* time base */
    print("done\r\nCPU-time init...");
    cputime_init();

	/* GPIO pins */
	gpio_init();
	gpio_pins_init();

	/* XADC initialization */
    print("done\r\nXADC init...");
	xadc_init();

	/* enable PL peripherals */
    print("done\r\nFPGA reset de-assert...");
	xcpu_deassert_fpga_reset(0xf);

    print("done\r\n\r\nStarting Xilinx USB example application.\r\n"
          "Attach PC Host to EasyBoard mini USB connector.\r\n");

    status = UsbIntrExample(gic_get_low(), &usb_low, XPAR_XUSBPS_0_DEVICE_ID, XPAR_XUSBPS_0_INTR);

    print("\r\n\r\nApplication finished ");
    print(status == XST_SUCCESS ? "successfully" : "with errors!");
    print("\r\nEND\r\n");

    while(1);
    return 0;
}

static void gpio_pins_init()
{
	int i;

	/* pre-set all EMIO pins to TRUE while keeping it tri-state */
	for(i=GPIO_PIN_OUTPUT_FIRST; i<=GPIO_PIN_OUTPUT_LAST; i++)
	{
		gpio_set_pin_outenable(i, FALSE);
		gpio_set_pin_direction(i, TRUE);
		gpio_set_pin_value(i, TRUE);
	}

	/* enable output (the pre-set high value is now applied) */
	for(i=GPIO_PIN_OUTPUT_FIRST; i<=GPIO_PIN_OUTPUT_LAST; i++)
		gpio_set_pin_outenable(i, TRUE);
}

static void print_gpio_value(char* text, int pin)
{
	print(text);
	print(":");
	print(gpio_get_pin_value(pin) ? "1 " : "0 ");
}
