#include <stdio.h>
#include "platform.h"
#include "xdlibrary.h"
#include "xd_utils.h"
#include "xd_cputime.h"
#include "xd_xadc.h"

/* other BSP drivers */
#include "xusbps.h"

/* bsp-specific settings, path is relative to auto-configured BSP include path */
#include "../../bspsettings.h"

int UsbIntrExample(XScuGic *IntcInstancePtr, XUsbPs *UsbInstancePtr, u16 UsbDeviceId, u16 UsbIntrId);
