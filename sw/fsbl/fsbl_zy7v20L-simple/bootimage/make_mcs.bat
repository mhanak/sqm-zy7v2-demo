
rem make sure the app_display is built with the bsp_zy7v20L-display BSP
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_display_debug.bif -o app_display_debug.mcs -w on

rem make sure the app_memtest is built with the bsp_zy7v20L-simple (or bsp_zy7v20L-display BSP)
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_memtest_debug.bif -o app_memtest_debug.mcs -w on

rem make sure the app_network is built with the bsp_zy7v20L-simple (or bsp_zy7v20L-display BSP)
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_network_debug.bif -o app_network_debug.mcs -w on
