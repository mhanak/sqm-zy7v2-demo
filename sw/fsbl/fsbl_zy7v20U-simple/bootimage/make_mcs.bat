
rem make sure the app_display is built with the bsp_zy7v20U-display BSP
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_display_debug.bif -o app_display_debug.mcs -w on

rem make sure the app_memtest is built with the bsp_zy7v20U-simple (or bsp_zy7v20U-display BSP)
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_memtest_debug.bif -o app_memtest_debug.mcs -w on

rem make sure the app_usbdev is built with the bsp_zy7v20U-simple (or bsp_zy7v20U-display BSP)
call c:\Xilinx\SDK\2018.3\bin\bootgen.bat -image app_usbdev_debug.bif  -o app_usbdev_debug.mcs -w on
 