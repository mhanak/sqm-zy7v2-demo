#include "main.h"

//void print(const char *str);

int main()
{
    int i;

    init_platform();

    /* enable cache on OCM */
    dsb(); isb();
    Xil_SetTlbAttributes(0, 0x4c0e);
    Xil_ICacheEnable();
    Xil_DCacheEnable();
    dsb(); isb();

    print("\r\nDDR Memory test\r\n");
    print("Build: " __DATE__ " " __TIME__ "; BSP: " BSP_NAME "\r\n");

    /* key SLCR initialization */
    print("XCPU init...");
    xcpu_init();

    /* reset PL peripherals */
    print("done\r\nFPGA reset assert...");
    xcpu_assert_fpga_reset(0xf);

    /* set OCM mapping (256kB OCM 0x00000-0x40000) */
    print("done\r\nOCM mapping...");
    xcpu_set_ocm_map(0, 0x100000, 0xffe00000, TRUE);

    /* set HP AXI slaves */
/*    print("done\r\nAXI-HP configuration...");
    xcpu_set_afi_static_wrqos(0, 4, FALSE); // AXI HP0 - 64bit DMA access
    xcpu_set_afi_static_wrqos(1, 8, TRUE);  // AXI HP1 - 32bit DMA-SG access
    xcpu_set_afi_static_wrqos(2, 4, FALSE); // AXI HP2 - 64bit DMA access
    xcpu_set_afi_static_wrqos(3, 8, TRUE);  // AXI HP3 - 32bit DMA-SG access
*/
    /* interrupt controller */
    print("done\r\nGIC init...");
    gic_init();
    for(i=0; i<XSCUGIC_MAX_NUM_INTR_INPUTS; i++)
        gic_disable(i);

    /* time base */
    print("done\r\nCPU-time init...");
    cputime_init();

    /* XADC initialization */
    print("done\r\nXADC init...");
    xadc_init();

    /* enable PL peripherals */
    //print("done\r\nFPGA reset de-assert...");
    //xcpu_deassert_fpga_reset(0xf);

    print("done\r\n\r\nStarting memory test:\n");

    volt_dump();
    ddrc_dump();
    memtest();

    return 0;
}

/*
void _boot(void);
__attribute__ ((section (".entry_address_section"))) void* entry_point = _boot;
note: jump to 0 is good for us, no need to store entry point
*/
