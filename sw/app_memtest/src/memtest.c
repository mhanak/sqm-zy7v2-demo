#include "main.h"

/*****************************************************************************
*
* MEMORY TESTING
*
******************************************************************************/

static u32 memtest_fill(u32* mem_from, u32 mem_size, u32 data, u32 incr, u32 xorv, bool flushing)
{
    u32 i, count;
    u32* p;
    u32 d, d_1, d_2;
    u32 err=0;

    ASSERT(mem_size % 4 == 0);
    count = mem_size / 4;

    p = mem_from;
    d = d_1 = d_2 = data;

    for(i=0; i<count; i++)
    {
        /*if(((u32)p) == 0x100080) err++;*/

        *p = d;
        if(*p != d)
        {
            err++;
        }
        if(i>0 && p[-1] != d_1)
        {
            err++;
        }
        if(i>1 && p[-2] != d_2)
        {
            err++;
        }

        p++;
        d_2 = d_1;
        d_1 = d;
        d += incr;
        d = d ^ xorv;
    }

    if(flushing)
    {
        Xil_L1DCacheFlushRange((u32) mem_from, mem_size);
        Xil_L1DCacheInvalidateRange((u32) mem_from, mem_size);
    }

    p = mem_from;
    d = data;

    for(i=0; i<count; i++)
    {
        if(*p != d)
        {
            err++;
        }

        p++;
        d += incr;
        d = d ^ xorv;
    }

    /* count just one error for all */
    return err ? 1 : 0;
}

static void out_one(char l1, u32 e)
{
    outbyte(l1);
    outbyte(e ? '-' : '+');

    if(!e)
    {
        outbyte('\b');
        outbyte('\b');
    }
}

static u32 memtest_range(u32 mem_from, u32 mem_size)
{
    bool flush;
    char c;
    u32* mem_addr = (u32*)mem_from;
    u32 err=0, e=0;

    ASSERT(((u32)mem_addr) % 4 == 0);
    ASSERT(mem_size % 4 == 0);

    for(flush=0; flush<=1; flush++)
    {
        c = flush ? 'a' : 'A';
        e = memtest_fill(mem_addr, mem_size, 0x00000000, 0, 0, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0xffffffff, 0, 0, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0x55aa55aa, 0, 0, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0xaa55aa55, 0, 0, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0xaa55aa55, 0, 0xffffffff, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0x00000000, 142427, 0x00ff00ff, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0x00000000, 199601, 0xff00ff00, flush);
        err+=e; out_one(c++, e);
        e = memtest_fill(mem_addr, mem_size, 0x00000000, 0x01010101, 0xffffffff, flush);
        err+=e; out_one(c++, e);
    }

    outbyte(' ');
    outbyte(' ');

    return err;
}

#define MEM_FROM  0x00100000   /* start after OCM */
//#define MEM_TO    0x40000000   /* 1G RAM */
#define MEM_TO    0x10000000   /* 256M RAM */
#define MEM_CHUNK 0x01000000   /* 16MB one chunk */

void memtest()
{
    xadc_data2_t xadc;
    u32 err=0, e;
    int t100;
    u32 from;
    u32 to;

    while(1)
    {
        if(xadc_get_last_data2(&xadc))
            t100 = (int)(xadc.temp*100+0.5);
        else
            t100 = 0;

        printf("uptime=%us t=%u.%02u�C all-errors=%u \n", cputime_get_ms32()/1000UL, t100/100, t100%100, err);

        from = MEM_FROM;

        do
        {
            to = (from + MEM_CHUNK) & ~(MEM_CHUNK-1);
            printf("%08x..%08x ", from, to);
            fflush(stdout);

            e = memtest_range(from, to-from);
            err += e;

            outbyte('\r');
            if(e)
                outbyte('\n');

            from = to;

        } while(from < MEM_TO);
    }
}


void ddrc_dump_old()
{
    ddrc_regs_t* const ddrc = (ddrc_regs_t*) XPS_DDR_CTRL_BASEADDR;
    slcr_regs_t* const slcr = (slcr_regs_t*) XPS_SYS_CTRL_BASEADDR;

    printf("\nDDR Controller Registers (silicon version %d)\n", xcpu_get_silicon_version());
    printf("ddrc->ddrc_ctrl       = 0x%08x\n", ddrc->ddrc_ctrl      );
    printf("ddrc->two_rank_cfg    = 0x%08x\n", ddrc->two_rank_cfg   );
    printf("ddrc->hpr_reg         = 0x%08x\n", ddrc->hpr_reg        );
    printf("ddrc->lpr_reg         = 0x%08x\n", ddrc->lpr_reg        );
    printf("ddrc->wr_reg          = 0x%08x\n", ddrc->wr_reg         );
    printf("ddrc->dram_param_reg0 = 0x%08x\n", ddrc->dram_param_reg0);
    printf("ddrc->dram_param_reg1 = 0x%08x\n", ddrc->dram_param_reg1);
    printf("ddrc->dram_param_reg2 = 0x%08x\n", ddrc->dram_param_reg2);
    printf("ddrc->dram_param_reg3 = 0x%08x\n", ddrc->dram_param_reg3);
    printf("ddrc->dram_param_reg4 = 0x%08x\n", ddrc->dram_param_reg4);
    printf("slcr->ddriob_ddr_ctrl = 0x%08x\n", slcr->ddriob_ddr_ctrl);
    printf("slcr->ddriob_drive_slew_addr  = 0x%08x\n", slcr->ddriob_drive_slew_addr);
    printf("slcr->ddriob_drive_slew_data  = 0x%08x\n", slcr->ddriob_drive_slew_data);
    printf("slcr->ddriob_drive_slew_diff  = 0x%08x\n", slcr->ddriob_drive_slew_diff);
    printf("slcr->ddriob_drive_slew_clock = 0x%08x\n", slcr->ddriob_drive_slew_clock);

    printf("\n");
}

void ddrc_dump()
{
    ddrc_regs_t* const ddrc = (ddrc_regs_t*) XPS_DDR_CTRL_BASEADDR;

	#define DDROFFSET(m) ((u32)(&((ddrc_regs_t*)0)->m))
    #define DDRDUMP(member, name) printf("[0x%04x]=0x%08x  %s  %s\n", DDROFFSET(member), ddrc->member, #member, name)

    DDRDUMP(ddrc_ctrl, "0x00000000 (rw): DDRC Control, reset=0x00000200");
    DDRDUMP(two_rank_cfg, "0x00000004 (rw, 29bits): Two Rank Configuration, reset=0x000C1076");
    DDRDUMP(hpr_reg, "0x00000008 (rw, 26bits): HPR Queue control, reset=0x03C0780F");
    DDRDUMP(lpr_reg, "0x0000000C (rw, 26bits): LPR Queue control, reset=0x03C0780F");
    DDRDUMP(wr_reg, "0x00000010 (rw, 26bits): WR Queue control, reset=0x0007F80F");
    DDRDUMP(dram_param_reg0, "0x00000014 (rw, 21bits): DRAM Parameters 0, reset=0x00041016");
    DDRDUMP(dram_param_reg1, "0x00000018 (rw): DRAM Parameters 1, reset=0x351B48D9");
    DDRDUMP(dram_param_reg2, "0x0000001C (rw): DRAM Parameters 2, reset=0x83015904");
    DDRDUMP(dram_param_reg3, "0x00000020 (mixed): DRAM Parameters 3, reset=0x250882D0");
    DDRDUMP(dram_param_reg4, "0x00000024 (mixed, 28bits): DRAM Parameters 4, reset=0x0000003C");
    DDRDUMP(dram_init_param, "0x00000028 (rw, 14bits): DRAM Initialization Parameters, reset=0x00002007");
    DDRDUMP(dram_emr_reg, "0x0000002C (rw): DRAM EMR2, EMR3 access, reset=0x00000008");
    DDRDUMP(dram_emr_mr_reg, "0x00000030 (rw): DRAM EMR, MR access, reset=0x00000940");
    DDRDUMP(dram_burst8_rdwr, "0x00000034 (mixed, 29bits): DRAM Burst 8 read/write, reset=0x00020034");
    DDRDUMP(dram_disable_dq, "0x00000038 (mixed, 13bits): DRAM Disable DQ, reset=0x00000000");
    DDRDUMP(dram_addr_map_bank, "0x0000003C (rw, 20bits): Row/Column address bits, reset=0x00000F77");
    DDRDUMP(dram_addr_map_col, "0x00000040 (rw): Column address bits, reset=0xFFF00000");
    DDRDUMP(dram_addr_map_row, "0x00000044 (rw, 28bits): Select DRAM row address bits, reset=0x0FF55555");
    DDRDUMP(dram_odt_reg, "0x00000048 (rw, 30bits): DRAM ODT control, reset=0x00000249");
    DDRDUMP(phy_dbg_reg, "0x0000004C (ro, 20bits): PHY debug, reset=0x00000000");
    DDRDUMP(phy_cmd_timeout_rddata_cpt, "0x00000050 (mixed): PHY command time out and read data capture FIFO, reset=0x00010200");
    DDRDUMP(mode_sts_reg, "0x00000054 (ro, 21bits): Controller operation mode status, reset=0x00000000");
    DDRDUMP(dll_calib, "0x00000058 (rw, 17bits): DLL calibration, reset=0x00000101");
    DDRDUMP(odt_delay_hold, "0x0000005C (rw, 16bits): ODT delay and ODT hold, reset=0x00000023");
    DDRDUMP(ctrl_reg1, "0x00000060 (mixed, 13bits): Controller 1, reset=0x0000003E");
    DDRDUMP(ctrl_reg2, "0x00000064 (mixed, 18bits): Controller 2, reset=0x00020000");
    DDRDUMP(ctrl_reg3, "0x00000068 (rw, 26bits): Controller 3, reset=0x00284027");
    DDRDUMP(ctrl_reg4, "0x0000006C (rw, 16bits): Controller 4, reset=0x00001610");
    DDRDUMP(ctrl_reg5, "0x00000078 (mixed): Controller register 5, reset=0x00455111");
    DDRDUMP(ctrl_reg6, "0x0000007C (mixed): Controller register 6, reset=0x00032222");
    DDRDUMP(che_refresh_timer01, "0x000000A0 (rw, 24bits): CHE_REFRESH_TIMER01, reset=0x00008000");
    DDRDUMP(che_t_zq, "0x000000A4 (rw): ZQ parameters, reset=0x10300802");
    DDRDUMP(che_t_zq_short_interval_reg, "0x000000A8 (rw, 28bits): Misc parameters, reset=0x0020003A");
    DDRDUMP(deep_pwrdwn_reg, "0x000000AC (rw, 9bits): Deep powerdown (LPDDR2), reset=0x00000000");
    DDRDUMP(reg_2c, "0x000000B0 (mixed, 29bits): Training control, reset=0x00000000");
    DDRDUMP(reg_2d, "0x000000B4 (rw, 11bits): Misc Debug, reset=0x00000200");
    DDRDUMP(dfi_timing, "0x000000B8 (rw, 25bits): DFI timing, reset=0x00200067");
    DDRDUMP(che_ecc_control_reg_offset, "0x000000C4 (rw, 2bits): ECC error clear, reset=0x00000000");
    DDRDUMP(che_corr_ecc_log_reg_offset, "0x000000C8 (mixed, 8bits): ECC error correction, reset=0x00000000");
    DDRDUMP(che_corr_ecc_addr_reg_offset, "0x000000CC (ro, 31bits): ECC error correction address log, reset=0x00000000");
    DDRDUMP(che_corr_ecc_data_31_0_reg_offset, "0x000000D0 (ro): ECC error correction data log low, reset=0x00000000");
    DDRDUMP(che_corr_ecc_data_63_32_reg_offset, "0x000000D4 (ro): ECC error correction data log mid, reset=0x00000000");
    DDRDUMP(che_corr_ecc_data_71_64_reg_offset, "0x000000D8 (ro, 8bits): ECC error correction data log high, reset=0x00000000");
    DDRDUMP(che_uncorr_ecc_log_reg_offset, "0x000000DC (clron wr, 1bits): ECC unrecoverable error status, reset=0x00000000");
    DDRDUMP(che_uncorr_ecc_addr_reg_offset, "0x000000E0 (ro, 31bits): ECC unrecoverable error address, reset=0x00000000");
    DDRDUMP(che_uncorr_ecc_data_31_0_reg_offset, "0x000000E4 (ro): ECC unrecoverable error data low, reset=0x00000000");
    DDRDUMP(che_uncorr_ecc_data_63_32_reg_offset, "0x000000E8 (ro): ECC unrecoverable error data middle, reset=0x00000000");
    DDRDUMP(che_uncorr_ecc_data_71_64_reg_offset, "0x000000EC (ro, 8bits): ECC unrecoverable error data high, reset=0x00000000");
    DDRDUMP(che_ecc_stats_reg_offset, "0x000000F0 (clron wr, 16bits): ECC error count, reset=0x00000000");
    DDRDUMP(ecc_scrub, "0x000000F4 (rw, 4bits): ECC mode/scrub, reset=0x00000008");
    DDRDUMP(che_ecc_corr_bit_mask_31_0_reg_offset, "0x000000F8 (ro): ECC data mask low, reset=0x00000000");
    DDRDUMP(che_ecc_corr_bit_mask_63_32_reg_offset, "0x000000FC (ro): ECC data mask high, reset=0x00000000");
    DDRDUMP(phy_rcvr_enable, "0x00000114 (rw, 8bits): Phy receiver enable register, reset=0x00000000");
    DDRDUMP(phy_config0, "0x00000118 (rw, 31bits): PHY configuration register for data slice 0., reset=0x40000001");
    DDRDUMP(phy_config1, "0x0000011C (rw, 31bits): PHY configuration register for data slice 1., reset=0x40000001");
    DDRDUMP(phy_config2, "0x00000120 (rw, 31bits): PHY configuration register for data slice 2., reset=0x40000001");
    DDRDUMP(phy_config3, "0x00000124 (rw, 31bits): PHY configuration register for data slice 3., reset=0x40000001");
    DDRDUMP(phy_init_ratio0, "0x0000012C (rw, 20bits): PHY init ratio register for data slice 0., reset=0x00000000");
    DDRDUMP(phy_init_ratio1, "0x00000130 (rw, 20bits): PHY init ratio register for data slice 1., reset=0x00000000");
    DDRDUMP(phy_init_ratio2, "0x00000134 (rw, 20bits): PHY init ratio register for data slice 2., reset=0x00000000");
    DDRDUMP(phy_init_ratio3, "0x00000138 (rw, 20bits): PHY init ratio register for data slice 3., reset=0x00000000");
    DDRDUMP(phy_rd_dqs_cfg0, "0x00000140 (rw, 20bits): PHY read DQS configuration register for data slice 0., reset=0x00000040");
    DDRDUMP(phy_rd_dqs_cfg1, "0x00000144 (rw, 20bits): PHY read DQS configuration register for data slice 1., reset=0x00000040");
    DDRDUMP(phy_rd_dqs_cfg2, "0x00000148 (rw, 20bits): PHY read DQS configuration register for data slice 2., reset=0x00000040");
    DDRDUMP(phy_rd_dqs_cfg3, "0x0000014C (rw, 20bits): PHY read DQS configuration register for data slice 3., reset=0x00000040");
    DDRDUMP(phy_wr_dqs_cfg0, "0x00000154 (rw, 20bits): PHY write DQS configuration register for data slice 0., reset=0x00000000");
    DDRDUMP(phy_wr_dqs_cfg1, "0x00000158 (rw, 20bits): PHY write DQS configuration register for data slice 1., reset=0x00000000");
    DDRDUMP(phy_wr_dqs_cfg2, "0x0000015C (rw, 20bits): PHY write DQS configuration register for data slice 2., reset=0x00000000");
    DDRDUMP(phy_wr_dqs_cfg3, "0x00000160 (rw, 20bits): PHY write DQS configuration register for data slice 3., reset=0x00000000");
    DDRDUMP(phy_we_cfg0, "0x00000168 (rw, 21bits): PHY FIFO write enable configuration for data slice 0., reset=0x00000040");
    DDRDUMP(phy_we_cfg1, "0x0000016C (rw, 21bits): PHY FIFO write enable configuration for data slice 1., reset=0x00000040");
    DDRDUMP(phy_we_cfg2, "0x00000170 (rw, 21bits): PHY FIFO write enable configuration for data slice 2., reset=0x00000040");
    DDRDUMP(phy_we_cfg3, "0x00000174 (rw, 21bits): PHY FIFO write enable configuration for data slice 3., reset=0x00000040");
    DDRDUMP(wr_data_slv0, "0x0000017C (rw, 20bits): PHY write data slave ratio config for data slice 0., reset=0x00000080");
    DDRDUMP(wr_data_slv1, "0x00000180 (rw, 20bits): PHY write data slave ratio config for data slice 1., reset=0x00000080");
    DDRDUMP(wr_data_slv2, "0x00000184 (rw, 20bits): PHY write data slave ratio config for data slice 2., reset=0x00000080");
    DDRDUMP(wr_data_slv3, "0x00000188 (rw, 20bits): PHY write data slave ratio config for data slice 3., reset=0x00000080");
    DDRDUMP(reg_64, "0x00000190 (rw): Training control 2, reset=0x10020000");
    DDRDUMP(reg_65, "0x00000194 (rw, 20bits): Training control 3, reset=0x00000000");
    DDRDUMP(reg69_6a0, "0x000001A4 (ro, 29bits): Training results for data slice 0., reset=0x000F0000");
    DDRDUMP(reg69_6a1, "0x000001A8 (ro, 29bits): Training results for data slice 1., reset=0x000F0000");
    DDRDUMP(reg6c_6d2, "0x000001B0 (ro, 29bits): Training results for data slice 2., reset=0x000F0000");
    DDRDUMP(reg6c_6d3, "0x000001B4 (ro, 29bits): Training results for data slice 3., reset=0x000F0000");
    DDRDUMP(reg6e_710, "0x000001B8 (ro, 30bits): Training results (2) for data slice 0.");
    DDRDUMP(reg6e_711, "0x000001BC (ro, 30bits): Training results (2) for data slice 1.");
    DDRDUMP(reg6e_712, "0x000001C0 (ro, 30bits): Training results (2) for data slice 2.");
    DDRDUMP(reg6e_713, "0x000001C4 (ro, 30bits): Training results (2) for data slice 3.");
    DDRDUMP(phy_dll_sts0, "0x000001CC (ro, 27bits): Slave DLL results for data slice 0., reset=0x00000000");
    DDRDUMP(phy_dll_sts1, "0x000001D0 (ro, 27bits): Slave DLL results for data slice 1., reset=0x00000000");
    DDRDUMP(phy_dll_sts2, "0x000001D4 (ro, 27bits): Slave DLL results for data slice 2., reset=0x00000000");
    DDRDUMP(phy_dll_sts3, "0x000001D8 (ro, 27bits): Slave DLL results for data slice 3., reset=0x00000000");
    DDRDUMP(dll_lock_sts, "0x000001E0 (ro, 24bits): DLL Lock Status, read, reset=0x00000000");
    DDRDUMP(phy_ctrl_sts, "0x000001E4 (ro, 30bits): PHY Control status, read");
    DDRDUMP(phy_ctrl_sts_reg2, "0x000001E8 (ro, 27bits): PHY Control status (2), read, reset=0x00000000");
    DDRDUMP(axi_id, "0x00000200 (ro, 26bits): ID and revision information, reset=0x00153042");
    DDRDUMP(page_mask, "0x00000204 (rw): Page mask, reset=0x00000000");
    DDRDUMP(axi_priority_wr_port0, "0x00000208 (mixed, 20bits): AXI Priority control for write port 0., reset=0x000803FF");
    DDRDUMP(axi_priority_wr_port1, "0x0000020C (mixed, 20bits): AXI Priority control for write port 1., reset=0x000803FF");
    DDRDUMP(axi_priority_wr_port2, "0x00000210 (mixed, 20bits): AXI Priority control for write port 2., reset=0x000803FF");
    DDRDUMP(axi_priority_wr_port3, "0x00000214 (mixed, 20bits): AXI Priority control for write port 3., reset=0x000803FF");
    DDRDUMP(axi_priority_rd_port0, "0x00000218 (mixed, 20bits): AXI Priority control for read port 0., reset=0x000003FF");
    DDRDUMP(axi_priority_rd_port1, "0x0000021C (mixed, 20bits): AXI Priority control for read port 1., reset=0x000003FF");
    DDRDUMP(axi_priority_rd_port2, "0x00000220 (mixed, 20bits): AXI Priority control for read port 2., reset=0x000003FF");
    DDRDUMP(axi_priority_rd_port3, "0x00000224 (mixed, 20bits): AXI Priority control for read port 3., reset=0x000003FF");
    DDRDUMP(excl_access_cfg0, "0x00000294 (rw, 18bits): Exclusive access configuration for port 0., reset=0x00000000");
    DDRDUMP(excl_access_cfg1, "0x00000298 (rw, 18bits): Exclusive access configuration for port 1., reset=0x00000000");
    DDRDUMP(excl_access_cfg2, "0x0000029C (rw, 18bits): Exclusive access configuration for port 2., reset=0x00000000");
    DDRDUMP(excl_access_cfg3, "0x000002A0 (rw, 18bits): Exclusive access configuration for port 3., reset=0x00000000");
    DDRDUMP(mode_reg_read, "0x000002A4 (ro): Mode register read data, reset=0x00000000");
    DDRDUMP(lpddr_ctrl0, "0x000002A8 (rw, 12bits): LPDDR2 Control 0, reset=0x00000000");
    DDRDUMP(lpddr_ctrl1, "0x000002AC (rw): LPDDR2 Control 1, reset=0x00000000");
    DDRDUMP(lpddr_ctrl2, "0x000002B0 (rw, 22bits): LPDDR2 Control 2, reset=0x003C0015");
    DDRDUMP(lpddr_ctrl3, "0x000002B4 (rw, 18bits): LPDDR2 Control 3, reset=0x00000601");
}


void volt_dump()
{
	xadc_data2_t data;

	#define VOLTDUMP(member) printf("%s = %hf\n", #member, data.member)

	if(xadc_get_last_data2(&data))
	{
		VOLTDUMP(vccint);
		VOLTDUMP(vccaux);
		VOLTDUMP(vbram);
		VOLTDUMP(vccpint);
		VOLTDUMP(vccpaux);
		VOLTDUMP(vccpdro);
	}
}
