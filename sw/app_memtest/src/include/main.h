#include <stdio.h>
#include "platform.h"
#include "xdlibrary.h"
#include "xd_utils.h"
#include "xd_cputime.h"
#include "xd_xadc.h"

/* bsp-specific settings, path is relative to auto-configured BSP include path */
#include "../../bspsettings.h"

void memtest(void);
void ddrc_dump(void);
void volt_dump(void);
