/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief Network and FNET related functions
*
***************************************************************************/

#ifndef __NETWORK_H
#define __NETWORK_H

/*
 * Function prototypes
 */
void network_init(void);
void network_set_mac(int netif_index, u32 seed);

int net_send(SOCKET s, char *buf, int len, int flags);

void net_run_dhcp(int netif_index);

#endif /* __NETWORK_H */
