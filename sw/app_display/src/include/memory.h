/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief Memory handlers for FNET console and TFTP 
*
***************************************************************************/

#ifndef __MEMORY_H
#define __MEMORY_H

#define MEM_QSFLASH_SPACE_ADDR 0xD0000000 /* fake address to emulate QSPI flash address */
#define MEM_QSFLASH_SPACE_SIZE 0x01000000 /* total QSPI flash size */
#define MEM_QSFLASH_EPAGE_SIZE 0x00010000 /* erasable unit */

#define MEM_SRAM_ADDRESS 0x00000000
#define MEM_SRAM_SIZE    0x00040000

#define MEM_RAM_ADDRESS 0x00100000
#define MEM_RAM_SIZE    0x3FF00000

void memory_init(void);

#endif /* __MEMORY_H */
