/**************************************************************************
*
* Copyright 2013-2014 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
***************************************************************************/

#ifndef __SDISP_H
#define __SDISP_H

#define SDISP_BASE_ADDR XPAR_SDISP_0_BASEADDR

typedef volatile struct sdisp_regs_s
{
    u32 ctrl;
    u32 status;
    u32 hsync;
    u32 vsync;
    u32 bkgnd;
    u32 dead;

} sdisp_regs_t;

#define SDISP_CTRL_ENA         0x80000000
#define SDISP_CTRL_INV         0x40000000
#define SDISP_CTRL_FDIV_MASK   0x000000ff
#define SDISP_CTRL_FDIV_SHIFT  0
#define SDISP_CTRL_FDIV(x)     ((((x)-1)<<SDISP_CTRL_FDIV_SHIFT)&SDISP_CTRL_FDIV_MASK)

#define SDISP_HSYNC_RES_MASK   0x00000fff
#define SDISP_HSYNC_RES_SHIFT  0
#define SDISP_HSYNC_RES(x)     ((((x)-1)<<SDISP_HSYNC_RES_SHIFT)&SDISP_HSYNC_RES_MASK)
#define SDISP_HSYNC_LEN_MASK   0x0000f000
#define SDISP_HSYNC_LEN_SHIFT  12
#define SDISP_HSYNC_LEN(x)     ((((x)-1)<<SDISP_HSYNC_LEN_SHIFT)&SDISP_HSYNC_LEN_MASK)
#define SDISP_HSYNC_BP_MASK    0x00ff0000
#define SDISP_HSYNC_BP_SHIFT   16
#define SDISP_HSYNC_BP(x)      ((((x)-1)<<SDISP_HSYNC_BP_SHIFT)&SDISP_HSYNC_BP_MASK)
#define SDISP_HSYNC_FP_MASK    0xff000000
#define SDISP_HSYNC_FP_SHIFT   24
#define SDISP_HSYNC_FP(x)      ((((x)-1)<<SDISP_HSYNC_FP_SHIFT)&SDISP_HSYNC_FP_MASK)

#define SDISP_VSYNC_RES_MASK   0x00000fff
#define SDISP_VSYNC_RES_SHIFT  0
#define SDISP_VSYNC_RES(x)     ((((x)-1)<<SDISP_VSYNC_RES_SHIFT)&SDISP_VSYNC_RES_MASK)
#define SDISP_VSYNC_LEN_MASK   0x0000f000
#define SDISP_VSYNC_LEN_SHIFT  12
#define SDISP_VSYNC_LEN(x)     ((((x)-1)<<SDISP_VSYNC_LEN_SHIFT)&SDISP_VSYNC_LEN_MASK)
#define SDISP_VSYNC_BP_MASK    0x00ff0000
#define SDISP_VSYNC_BP_SHIFT   16
#define SDISP_VSYNC_BP(x)      ((((x)-1)<<SDISP_VSYNC_BP_SHIFT)&SDISP_VSYNC_BP_MASK)
#define SDISP_VSYNC_FP_MASK    0xff000000
#define SDISP_VSYNC_FP_SHIFT   24
#define SDISP_VSYNC_FP(x)      ((((x)-1)<<SDISP_VSYNC_FP_SHIFT)&SDISP_VSYNC_FP_MASK)

#define RGB(r,g,b) ( (((u8)r)<<0) | (((u8)g)<<8) | (((u8)b)<<16) )
#define RGB_R(c) ( (u8)(((c)>>0)&0xff) )
#define RGB_G(c) ( (u8)(((c)>>8)&0xff) )
#define RGB_B(c) ( (u8)(((c)>>16)&0xff) )

void sdisp_init();
void sh_disp_cmd(fnet_shell_desc_t desc, int argc, char ** argv);

#endif
