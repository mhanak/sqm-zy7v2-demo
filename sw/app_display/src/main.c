/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief The DEMO application
*
***************************************************************************/

#include "main.h"
#include "xdlibrary.h"
#include "xd_stopwatch.h"
#include "network.h"
#include "shell.h"
#include "xd_can.h"
#include "sdisp.h"
#include "ff.h"

#ifndef BSP_HAS_DISPLAY
#error This application requires the display IP block in Vivado project and in the BSP
#endif

static void cache_init(void);

rtc_abracon_t rtc_abracon = {
    .iic_instance = 0,
    .slave_addr = 0xd0>>1,
};

accel_mma845x_t accel_mma845x = {
    .iic_instance = 0,
    .slave_addr = 0x1c,
};

/***********************************************************/

static void gpio_pins_init();
static void cache_init(void);

/***********************************************************/

int main()
{
    alarm_t alarm_1sec, alarm_5sec, alarm_100ms, alarm_1ms;
    bool services_started = 0;
    u32 subsec;

    /* Xilinx low-level init (generated code) */
    init_platform();

    /* key SLCR initialization */
    xcpu_init();

    /* reset PL peripherals */
    xcpu_assert_fpga_reset(SLCR_FPGA_RST_CTRL_ALL);
    xcpu_assert_gem_reset(SLCR_GEM_RST_CTRL_ALL);

    /* set OCM mapping (256kB OCM 0x00000-0x40000) */
    xcpu_set_ocm_map(0, 0x100000, 0xffe00000, TRUE);

    /* set non-cached areas of memory */
    cache_init();

    /* interrupt controller */
    gic_init();

    /* time base */
    cputime_init();

    /* GPIO pins */
    gpio_init();
    gpio_pins_init();

    /* external reset of PHYs */
    gpio_set_pin_value(GPIO_PIN_ETH0_PHYRESET_B, 0);
#if FNET_CFG_CPU_ETH1
    gpio_set_pin_value(GPIO_PIN_ETH1_PHYRESET_B, 0);
#endif

    /* 10ms reset */
    usleep(10000);

    /* enable PL peripherals */
    xcpu_deassert_gem_reset(SLCR_GEM_RST_CTRL_ALL);
    xcpu_deassert_fpga_reset(SLCR_FPGA_RST_CTRL_ALL);

    /* release reset of PHYs */
    gpio_set_pin_value(GPIO_PIN_ETH0_PHYRESET_B, 1);
#if FNET_CFG_CPU_ETH1
    gpio_set_pin_value(GPIO_PIN_ETH1_PHYRESET_B, 1);
#endif

    usleep(10000);

    /* Initialize FNET serial line */
    fnet_cpu_serial_init(FNET_CFG_CPU_SERIAL_PORT_DEFAULT, 115200);
    fnet_printf("\n\n");

#if 0
    /* initialize FNET network stack */
    network_init();
#if FNET_CFG_CPU_ETH0
    network_set_mac(0, 0xdeadbeef);
#endif
#if FNET_CFG_CPU_ETH1
    network_set_mac(1, 0xcafeface);
#endif

    /* verify if our interrupt system works */
    usleep(110000);
    if(fnet_timer_ticks() == 0)
        fnet_printf("PROBLEM WITH FNET TIMEBASE DETECTED!!!\n");
#endif

    /* initialize FNET shell stack */
    shell_init();

    /* Simple Display (FPGA-synthesized peripheral) initialization */
    sdisp_init();

    /* XADC initialization */
    xadc_init();

    /* I2C initialization */
    iic_init(0);

    /* IIC devices initialization */
    rtc_abracon_init(0, &rtc_abracon);
    accel_mma845x_init(0, &accel_mma845x);

    /* CAN module initialization */
    can_init(0);

    /* QSPI Flash initialization */
    qsflash_init(0, 0);
    qsflash_set_clock(0, 100000UL, 0, 0); /* 100 MHz clock, low-speed read will not work! */
    qsflash_set_device_map(0, qsflash_map_n25q128_uniform);

    /* 1 and 5 second intervals */
    alarm_init(&alarm_1ms, 1000, 0);
    alarm_init(&alarm_100ms, 100000, 0);
    alarm_init(&alarm_1sec, 1000000, 0);
    alarm_init(&alarm_5sec, 5000000, 0);

#if 0
    /* DHCP: FNET2.5 only supports one instance of DHCP service. It would be
     *       easy to extend it, but this is out of scope of this simple demo. */
#if FNET_CFG_CPU_ETH0
    net_run_dhcp(0);
#elif FNET_CFG_CPU_ETH1
    net_run_dhcp(1);
#endif

    /* set default ETH interface for console commands, only one can be default */
#if FNET_CFG_CPU_ETH0
    fnet_netif_set_default(fnet_netif_get_by_number(0));
#elif FNET_CFG_CPU_ETH1
    fnet_netif_set_default(fnet_netif_get_by_number(1));
#endif

#endif

    while(1)
    {
        /* Polling services.*/
       fnet_poll_services();

       /* do 5-seconds periodic actions */
       if(alarm_get_expired(&alarm_5sec, &subsec, TRUE))
       {
           if(!services_started)
           {
                /* run services, once when the system settles (after 5 sec) */
                fnet_shell_script(shell_desc, "tftps; http;");
                services_started = TRUE;
           }
       }

       /* do 1-second periodic actions */
       if(alarm_get_expired(&alarm_1sec, &subsec, TRUE))
       {
           gpio_toggle_pin(GPIO_PIN_LED_CTRL); /* nothing here yet */
       }

       /* do 100-millisecond periodic actions */
       if(alarm_get_expired(&alarm_100ms, &subsec, TRUE))
       {
       }

       /* do 1-millisecond periodic actions */
       if(alarm_get_expired(&alarm_1ms, &subsec, TRUE))
       {
       }
    }

    return 0;
}

static void cache_init(void)
{
    typedef struct { u32 start; u32 end; u32 attr; } nocache_tbl_t;
    extern nocache_tbl_t __NOCACHE_TABLE[];
    nocache_tbl_t* pt;;

    u32 addr;

    for(pt = &__NOCACHE_TABLE[0]; pt->end != 0; pt++)
    {
        /* Set Buffer area cache attribute as defined in the table */
        ASSERT((pt->start & 0xfffff) == 0);     /* need to be at MMU section boundary */
        ASSERT((pt->end & 0xfffff) == 0xfffff); /* need to be at MMU section boundary */
        for(addr=pt->start; addr<pt->end; addr+=0x100000)
        {
            /* addr overflow to 0 */
            if(pt->start && !addr)
                break;

            Xil_SetTlbAttributes(addr, pt->attr);
            Xil_DCacheInvalidateRange(addr, 0x100000);
        }
    }
}

static void gpio_pins_init()
{
    int i;

    /* pre-set all EMIO pins to TRUE while keeping it tri-state */
    for(i=GPIO_PIN_OUTPUT_FIRST; i<=GPIO_PIN_OUTPUT_LAST; i++)
    {
        gpio_set_pin_outenable(i, FALSE);
        gpio_set_pin_direction(i, TRUE);
        gpio_set_pin_value(i, TRUE);
    }

    /* enable output (the pre-set high value is now applied) */
    for(i=GPIO_PIN_OUTPUT_FIRST; i<=GPIO_PIN_OUTPUT_LAST; i++)
        gpio_set_pin_outenable(i, TRUE);
}


#define PRINT_XADC_D1(memb)  { sprintf(line, #memb " = %.4g", d1.memb); fnet_shell_println(desc, "%s", line); }
#define PRINT_XADC_D2(memb)  { sprintf(line, #memb " = %.4g, (min=%.4g, max=%.4g)", d2.memb, d2min.memb, d2max.memb); fnet_shell_println(desc, "%s", line); }

void sh_temp_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    xadc_data2_t d2, d2min, d2max;
    xadc_data1_t d1;
    char line[128];

    if(xadc_get_last_data1(&d1))
    {
        PRINT_XADC_D1(vpvn);
        PRINT_XADC_D1(vrefp);
        PRINT_XADC_D1(vrefn);
    }
    else
    {
        fnet_shell_printf(desc, "Error getting XADC data1\n");
    }

    if(xadc_get_last_data2(&d2) && xadc_get_peak_data2(&d2min, TRUE) && xadc_get_peak_data2(&d2max, FALSE))
    {
        PRINT_XADC_D2(temp);
        PRINT_XADC_D2(vccint);
        PRINT_XADC_D2(vccaux);
        PRINT_XADC_D2(vbram);
        PRINT_XADC_D2(vccpint);
        PRINT_XADC_D2(vccpaux);
        PRINT_XADC_D2(vccpdro);
    }
    else
    {
        fnet_shell_printf(desc, "Error getting XADC data2\n");
    }
}

void sh_rtc_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    rtc_data_t d;

    xd_err_t err = rtc_get_data(0, &d);

    if(SUCCEEDED(err))
    {
        fnet_shell_printf(desc, "RTC: %04d/%02d/%02d %02d:%02d:%02d\n",
                d.year, d.month, d.day, d.hour, d.minute, d.second);
    }
    else
    {
        fnet_shell_printf(desc, "RTC: Error getting data: %s\n", err);
    }
}

void sh_mma_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    accel_data_t d;

    xd_err_t err = accel_get_data(0, &d);

    if(SUCCEEDED(err))
    {
        fnet_shell_printf(desc, "ACCEL: x=%d, y=%d, z=%d\n", d.x, d.y, d.z);
    }
    else
    {
        fnet_shell_printf(desc, "ACCEL: Error getting data: %s\n", err);
    }
}

void sh_log1_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
}

void sh_log2_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
}

void sh_can_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    xd_err_t err;
    u32 rate, id;
    int len, i;
    u8 buff[8];
    u16 ts;

    if(can_get_mode(0) != CAN_MODE_LOOPBACK)
    {
        err = can_set_mode(0, CAN_MODE_CONFIG, 1000);
        fnet_shell_printf(desc, "CAN: Configuration mode: %s\n", ERR2STR(err));

        rate = can_set_timing(0, 4, 1, 12, 1);
        fnet_shell_printf(desc, "CAN: Baudrate: %u\n", rate);

        err = can_disable_filters(0, 0xf);

        err = can_set_filter(0, 0, 0x3f0, 0x1f0, 1000);
        fnet_shell_printf(desc, "CAN: Filter 1 set: %s\n", ERR2STR(err));
        err = can_set_filter(0, 1, CAN_ID_EXT | 0x1ffffff0, CAN_ID_EXT | 0x12345678, 1000);
        fnet_shell_printf(desc, "CAN: Filter 2 set: %s\n", ERR2STR(err));
        err = can_enable_filters(0, 0x3);

        fnet_shell_printf(desc, "CAN: Filters enabled: 0x%x\n", can_get_enabled_filters(0));

        err = can_set_mode(0, CAN_MODE_LOOPBACK, 1000);
        fnet_shell_printf(desc, "CAN: In loopback mode: %s\n", ERR2STR(err));
        if(FAILED(err))
            fnet_shell_printf(desc, "CAN: current mode is: 0x%x\n", can_get_mode(0));
    }

    err = can_send(0, 0x1f0, 6, "Hello1");
    fnet_shell_printf(desc, "CAN: Sent#1: %s\n", ERR2STR(err));
    err = can_send(0, 0x1e1, 6, "Hello2"); /* this one will not pass the filter */
    fnet_shell_printf(desc, "CAN: Sent#2: %s\n", ERR2STR(err));
    err = can_send(0, 0x1f1, 6, "Hello3");
    fnet_shell_printf(desc, "CAN: Sent#3: %s\n", ERR2STR(err));

    err = can_send(0, 0x12345678 | CAN_ID_EXT, 5, "World1");
    fnet_shell_printf(desc, "CAN: Sent#4: %s\n", ERR2STR(err));
    err = can_send(0, 0x12345679 | CAN_ID_EXT, 5, "World2");
    fnet_shell_printf(desc, "CAN: Sent#5: %s\n", ERR2STR(err));
    err = can_send(0, 0x123f5678 | CAN_ID_EXT, 5, "World3"); /* this one will not pass the filter */
    fnet_shell_printf(desc, "CAN: Sent#6: %s\n", ERR2STR(err));

    while(SUCCEEDED(can_recv(0, &id, &len, buff, &ts)))
    {
        fnet_shell_printf(desc, "CAN: Received: ID=0x%x, len=%d, time=%d, data:", id, len, ts);
        for(i=0; i<len; i++)
            fnet_shell_printf(desc, "%02x ", buff[i]);
        fnet_shell_printf(desc, "\n");
    }
}

static u8 flash_buff[0x1000];

void sh_flash_dump(fnet_shell_desc_t desc, void* data, u32 size)
{
    u8* pdata = (u8*) data;
    u8 x;
    char c;
    int i = 0;

    while(size--)
    {
        x = *pdata++;

        if(x == 0)
            c = '-';
        else if(x == 0xff)
            c = '#';
        else if(isprint(x))
            c = x;
        else
            c = '.';

        fnet_shell_putchar(desc, c);

        if(++i == 64)
        {
            fnet_shell_printf(desc, "\n");
            i = 0;
        }
    }

    if(i)
        fnet_shell_printf(desc, "\n");
}

bool sh_flash_identify(fnet_shell_desc_t desc)
{
    qsflash_info_t info;
    xd_err_t err;
    u8 ii;

    err = qsflash_identify(0, &info, QSFLASH_BASIC_INFO_LEN);
    fnet_shell_printf(desc, "FLASH READ INFO: %s\n", ERR2STR(err));
    if(FAILED(err))
        return FALSE;

    if(info.edid_cfd_len > 0)
    {
        err = qsflash_identify(0, &info, QSFLASH_BASIC_INFO_LEN + info.edid_cfd_len);
        fnet_shell_printf(desc, "FLASH READ FULL INFO: %s\n", ERR2STR(err));
        if(FAILED(err))
            return FALSE;
    }

    fnet_shell_printf(desc, " - manufacturer_id: 0x%02x\n", info.manufacturer_id);
    fnet_shell_printf(desc, " - memory_type:     0x%02x\n", info.memory_type);
    fnet_shell_printf(desc, " - memory_capacity: 0x%02x\n", info.memory_capacity);
    fnet_shell_printf(desc, " - edid_cfd_len:    0x%02x\n", info.edid_cfd_len);

    fnet_shell_printf(desc, " - edid_cfd: ");
    for(ii=0; ii<info.edid_cfd_len; ii++)
        fnet_shell_printf(desc, "%02x ", info.edid_cfd[ii]);
    fnet_shell_printf(desc, "\n");

    return info.manufacturer_id != 0;
}

void sh_flash_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    xd_err_t err;
    u32 test_addr = 0x100000;
    u32 test_addr2 = test_addr + 0x3000;
    u32 test_size = 0x231;
    u32 test_offs = 0x13;
    u32 read_size = 0x280;
    u32 i;

    if(!sh_flash_identify(desc))
        return ;

    fnet_shell_printf(desc, "*** READ TEST ***\n");

    err = qsflash_read(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH READ-SPEED4: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);

    /* reading the same data with lower speed */
    err = qsflash_verify(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_DOUBLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED2: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_SINGLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED1: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_LOW);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED0: %s\n", ERR2STR(err));

    /* erase sectors */
    fnet_shell_printf(desc, "*** ERASE TEST ***\n");
    err = qsflash_erase(0, test_addr, test_size, FALSE, 10000);
    fnet_shell_printf(desc, "FLASH ERASE: %s\n", ERR2STR(err));

    err = qsflash_read(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH READ-SPEED4: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);

    /* aligned write sectors */
    fnet_shell_printf(desc, "*** ALIGNED WRITE TEST ***\n");
    for(i=0; i<test_size; i++)
        flash_buff[i] = 'A' + i%26;

    err = qsflash_write(0, test_addr, test_size, flash_buff, 10000);
    fnet_shell_printf(desc, "FLASH ALIGNED WRITE: %s\n", ERR2STR(err));

    err = qsflash_verify(0, test_addr, test_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED4: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr, test_size, flash_buff, QSFLASH_SPEED_DOUBLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED2: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr, test_size, flash_buff, QSFLASH_SPEED_SINGLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED1: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr, test_size, flash_buff, QSFLASH_SPEED_LOW);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED0: %s\n", ERR2STR(err));

    err = qsflash_read(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH READ-SPEED4: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);
    err = qsflash_read(0, test_addr, read_size, flash_buff, QSFLASH_SPEED_LOW);
    fnet_shell_printf(desc, "FLASH READ-SPEED0: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);

    /* misaligned write sectors */
    fnet_shell_printf(desc, "*** MISALIGNED WRITE TEST ***\n");

    err = qsflash_read(0, test_addr2, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "PRE-WRITE READ-SPEED4: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);

    for(i=0; i<test_size; i++)
        flash_buff[i] = 'a' + i%26;

    err = qsflash_write(0, test_addr2+test_offs, test_size, flash_buff, 10000);
    fnet_shell_printf(desc, "FLASH MISALIGNED WRITE: %s\n", ERR2STR(err));

    err = qsflash_verify(0, test_addr2+test_offs, test_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED4: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr2+test_offs, test_size, flash_buff, QSFLASH_SPEED_DOUBLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED2: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr2+test_offs, test_size, flash_buff, QSFLASH_SPEED_SINGLE);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED1: %s\n", ERR2STR(err));
    err = qsflash_verify(0, test_addr2+test_offs, test_size, flash_buff, QSFLASH_SPEED_LOW);
    fnet_shell_printf(desc, "FLASH VERIFY-SPEED0: %s\n", ERR2STR(err));

    err = qsflash_read(0, test_addr2, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH READ-SPEED4: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);
    err = qsflash_read(0, test_addr2, read_size, flash_buff, QSFLASH_SPEED_LOW);
    fnet_shell_printf(desc, "FLASH READ-SPEED0: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);


    err = qsflash_read(0, 0, read_size, flash_buff, QSFLASH_SPEED_QUAD);
    fnet_shell_printf(desc, "FLASH FROM 0x0000000: %s\n", ERR2STR(err));
    sh_flash_dump(desc, flash_buff, read_size);

}

const char* TEST_STRING = "Hello World\n2nd line\n";
const char* FILE_NAME = "mytest.txt";
char large_buff[1024*1024*128];

#define FFCHECK(r, c, msg) \
        fnet_shell_printf(desc, msg); \
        if((r != FR_OK) || !(c)) {  \
            fnet_shell_printf(desc, " failed (res=%d)\n", r); \
            goto err_ret; \
        } else \
        { \
            fnet_shell_printf(desc, " succeeded\n", r); \
        }

void sh_ff2_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    static FATFS fatfs;
    FRESULT res;
    FIL fil;
    UINT done;
    char msg[200];
    char fname[12];
    int i, off, size;
    float t;

    stopwatch_t sw;

    for(i=0; i<sizeof(large_buff); i++)
    {
        off = i%27;
        large_buff[i] = off != 26 ? 'A' + off : '\n';
    }

    res = f_mount(&fatfs, "0:/", 1);
    FFCHECK(res, 1, "Mount");

    for(i=0; i<100; i++)
    {
        size = (i+1)*1000*1000;
        sprintf(fname, "data%02d.txt", i);

        stopwatch_init(&sw);
        stopwatch_start(&sw);

        sprintf(msg, "File %02d open", i);
        memset(&fil, 0, sizeof(fil));
        res = f_open(&fil, fname, FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
        FFCHECK(res, 1, msg);

        sprintf(msg, "File %02d write %d bytes", i, size);
        res = f_write(&fil, large_buff, size, &done);
        FFCHECK(res, done == size, msg);

        res = f_close(&fil);
        FFCHECK(res, 1, "File close");

        stopwatch_stop(&sw);

        t = (float)stopwatch_get_us32(&sw);
        if(t > 0.0)
        {
            t /= 1e6;
            sprintf(msg, "Written %d bytes in %.3f sec (%.3f bps)\n", size, (float)t, (float)(size/t));
            fnet_shell_printf(desc, msg);
        }
    }

err_ret:
    res = f_close(&fil);
    if (res == FR_OK)
        fnet_shell_printf(desc, "File close succeeded\n");
    else
        fnet_shell_printf(desc, "File close failed\n");

    f_mount(NULL, "0:/", 1);
}

void sh_ff_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    static FATFS fatfs;
    static char buff[200];
    FRESULT res;
    FIL fil;
    UINT done;

    memset(&fatfs, 0, sizeof(fatfs));
    memset(&fil, 0, sizeof(fil));

    res = f_mount(&fatfs, "0:/", 1);
    FFCHECK(res, 1, "Mount");

    res = f_open(&fil, FILE_NAME, FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
    FFCHECK(res, 1, "File open");

    res = f_write(&fil, TEST_STRING, strlen(TEST_STRING), &done);
    FFCHECK(res, done == strlen(TEST_STRING), "File write");

    res = f_sync(&fil);
    FFCHECK(res, 1, "File sync");

    res = f_close(&fil);
    FFCHECK(res, 1, "File close 1");

    memset(&fil, 0, sizeof(fil));
    res = f_open(&fil, FILE_NAME, FA_OPEN_EXISTING | FA_READ);
    FFCHECK(res, 1, "File open 2");

    memset(buff, 0, sizeof(buff));
    res = f_read(&fil, buff, sizeof(buff), &done);
    FFCHECK(res, done == strlen(TEST_STRING), "File read/verify 2");

err_ret:
    res = f_close(&fil);
    if (res == FR_OK)
        fnet_shell_printf(desc, "File close 2 succeeded\n");
    else
        fnet_shell_printf(desc, "File close 2 failed\n");

    f_mount(NULL, "0:/", 1);
}
