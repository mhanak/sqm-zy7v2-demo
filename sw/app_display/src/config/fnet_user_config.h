/**********************************************************************/ /*!
*
* @file fnet_user_config.h
*
* @brief FNET User configuration file.
* It should be used to change any default configuration parameter.
*
***************************************************************************/

#ifndef _FNET_USER_CONFIG_H_
#define _FNET_USER_CONFIG_H_

/* bsp-specific settings, path is relative to auto-configured BSP include path */
#include "../../bspsettings.h"

#define FNET_CFG_DEBUG_ASSERT 	(1)

/*****************************************************************************
* Enable compiler support.
******************************************************************************/
#define FNET_CFG_COMP_GCCCS       	(1)

/*****************************************************************************
* Processor type.
* Selected processor definition should be only one and must be defined as 1. 
* All others may be defined but must have 0 value.
******************************************************************************/
#define FNET_CFG_CPU_XC7Z020       	(1)

/* both ENETs */
#define FNET_CFG_CPU_ETH0           (1)  /* 1G-capable Marvel 88E1510 on EasyBoard's 100M port */

#if BSP_NAME_ZY7V20L
#define FNET_CFG_CPU_ETH1           (1)  /* 1G-capable Marvel 88E1510 on extension 1G connector */
#else
#define FNET_CFG_CPU_ETH1           (0)  /* ENET1 is only available on ZY7V20L */
#endif

/* ENET modules share the MDIO bus */
#define FNET_CFG_CPU_ETH1_PHY_SHARED_MDIO (1)

/* Both PHY addresses are auto-discovered from 0 */
#define FNET_CFG_CPU_ETH0_PHY_ADDR_DISCOVER (1)
#define FNET_CFG_CPU_ETH0_PHY_ADDR          (0)
#define FNET_CFG_CPU_ETH1_PHY_ADDR_DISCOVER (1)
#define FNET_CFG_CPU_ETH1_PHY_ADDR          (1)

/* initial MAC addresses are zeroed to postpone the MAC start until we assign the MAC in the application */
#define FNET_CFG_CPU_ETH0_MAC_ADDR      ("00:00:00:00:00:00")
#define FNET_CFG_CPU_ETH1_MAC_ADDR      ("00:00:00:00:00:00")

#define FNET_CFG_SOCKET_MAX              20
#define FNET_CFG_SOCKET_TCP_TX_BUF_SIZE (10*1024)
#define FNET_CFG_SOCKET_TCP_RX_BUF_SIZE (10*1024)

/*****************************************************************************
* IPv4 and/or IPv6 protocol support.
******************************************************************************/
#define FNET_CFG_IP4                (1)
#define FNET_CFG_IP6                (0)

/*****************************************************************************
* Marvel 88E1510 enables 12MHz MDIO clock
******************************************************************************/
#define FNET_CFG_CPU_ETH0_PHY_MDIO_CLK_KHZ	(12000) // 2500
#define FNET_CFG_CPU_ETH1_PHY_MDIO_CLK_KHZ	(12000)

/*****************************************************************************
* Advertise Ethernet speeds
******************************************************************************/
#define FNET_CFG_CPU_ETH0_AUTONEGOTIATION_TIMEOUT     (4000)
/* ENET0 - EasyBoard does not enable 1000 option */
#define FNET_CFG_CPU_ETH0_AUTONEGOTIATION_ADVERTISE   (FNET_ETH_PHY_MEDIA_100)
/* un-comment this to get 100 and 1000 speed on a custom board which uses the SQM4 module */
//#define FNET_CFG_CPU_ETH0_AUTONEGOTIATION_ADVERTISE   (FNET_ETH_PHY_MEDIA_100 | FNET_ETH_PHY_MEDIA_1000)

/* ENET1 on SQM4 module */
#define FNET_CFG_CPU_ETH1_AUTONEGOTIATION_TIMEOUT     (4000)
#define FNET_CFG_CPU_ETH1_AUTONEGOTIATION_ADVERTISE   (FNET_ETH_PHY_MEDIA_100 | FNET_ETH_PHY_MEDIA_1000)

/*****************************************************************************
* IP addresses for the Ethernet interface.
******************************************************************************/

#if FNET_CFG_CPU_ETH0

	#define FNET_CFG_ETH0_IP4_ADDR      (FNET_IP4_ADDR_INIT(192, 168, 1, 21))
	#define FNET_CFG_ETH0_IP4_MASK      (FNET_IP4_ADDR_INIT(255, 0, 0, 0))
	#define FNET_CFG_ETH0_IP4_GW        (FNET_IP4_ADDR_INIT(192, 168, 1, 1))
	#define FNET_CFG_ETH0_IP4_DNS       FNET_CFG_ETH0_IP4_GW
#endif

#if FNET_CFG_CPU_ETH1
	#define FNET_CFG_ETH1_IP4_ADDR      (FNET_IP4_ADDR_INIT(11, 0, 0, 22))
    #define FNET_CFG_ETH1_IP4_MASK      (FNET_IP4_ADDR_INIT(255, 0, 0, 0))
	#define FNET_CFG_ETH1_IP4_GW        (FNET_IP4_ADDR_INIT(11, 0, 0, 1))
#define FNET_CFG_ETH1_IP4_DNS           FNET_CFG_ETH1_IP4_GW
#endif

/*****************************************************************************
* Size of the internal static heap buffer. 
* This definition is used only if the fnet_init_static() was 
* used for the FNET initialization.
******************************************************************************/
#define FNET_CFG_HEAP_SIZE              (64*1024)
#define FNET_CFG_CPU_ETH_RX_BUFS_MAX    (16)
#define FNET_CFG_CPU_ETH_TX_BUFS_MAX    (5)

/*****************************************************************************
* TCP protocol support.
* You can disable it to save a substantial amount of code if 
* your application only needs UDP. By default it is enabled.
******************************************************************************/
#define FNET_CFG_TCP                (1)

/*****************************************************************************
* UDP protocol support.
* You can disable it to save a some amount of code if your 
* application only needs TCP. By default it is enabled.
******************************************************************************/
#define FNET_CFG_UDP                (1)

/*****************************************************************************
* UDP checksum.
* If enabled, the UDP checksum will be generated for transmitted 
* datagrams and be verified on received UDP datagrams.
* You can disable it to speedup UDP applications. 
* By default it is enabled.
******************************************************************************/
#define FNET_CFG_UDP_CHECKSUM       (1)

/*****************************************************************************
* IP fragmentation.
* If the IP fragmentation is enabled, the IP will attempt to reassemble IP 
* packet fragments and will able to generate fragmented IP packets.
* If disabled, the IP will  silently discard fragmented IP packets..
******************************************************************************/
#define FNET_CFG_IP4_FRAGMENTATION   (1)

/*****************************************************************************
* DHCP Client service support.
******************************************************************************/
#define FNET_CFG_DHCP               (1)

/*****************************************************************************
* HTTP Server service support.
******************************************************************************/
#define FNET_CFG_HTTP                       (1)
#define FNET_CFG_HTTP_SESSION_MAX           (10)
#define FNET_CFG_HTTP_AUTHENTICATION_BASIC  (0)
#define FNET_CFG_HTTP_POST                  (0)

/*****************************************************************************
* Telnet Server service support.
******************************************************************************/
#define FNET_CFG_TELNET                     (1)

/*****************************************************************************
* TFTP service support.
******************************************************************************/
#define FNET_CFG_TFTP_SRV                   (1)
#define FNET_CFG_TFTP_CLN                   (1)

/*****************************************************************************
* Flash Module driver support.
******************************************************************************/
#define FNET_CFG_FLASH                      (0)

/*****************************************************************************
* DNS client/resolver service support.
******************************************************************************/
#define FNET_CFG_DNS                        (1)
#define FNET_CFG_DNS_RESOLVER               (1)

/*****************************************************************************
* PING service support.
******************************************************************************/
#define FNET_CFG_PING                       (1)

/*****************************************************************************
* ROM Filesystem
******************************************************************************/
#define FNET_CFG_FS                         (1)
#define FNET_CFG_FS_ROM                     (1)

/*****************************************************************************
* Help print format
******************************************************************************/
#define FNET_CFG_SHELL_HELP_FORMAT "%8s %s - %s"

/*****************************************************************************
* default console is UART1
******************************************************************************/
#define FNET_CFG_CPU_SERIAL_PORT_DEFAULT 1

#endif /* _FNET_USER_CONFIG_H_ */

