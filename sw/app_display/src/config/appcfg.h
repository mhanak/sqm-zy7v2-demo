/**************************************************************************
*
* @author Michal Hanak
*
* @brief This file is the main configuration file of the demo application.
*        Only macro constants allowed. Do put any code declarations here.
*        The configuration is validated and completed in the config.h file.
*
***************************************************************************/

#ifndef __APPCFG_H
#define __APPCFG_H

/***************************************************************************/ /*!
* @brief Debug build?
*****************************************************************************/
#define _DEBUG 1 // TODO: this should be set in Debug build target in project


#endif /* __APPCFG_H */
