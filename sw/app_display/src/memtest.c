/**************************************************************************
*
* Copyright 2015 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief Memtest launcher
*
***************************************************************************/

#include "main.h"
#include "xdlibrary.h"
#include "shell.h"

/* binary image of mem_test application compiled separately (just make sure it is built with proper bsp) */
#include "../../app_memtest/image/app_memtest_image.h"

typedef void (*jump_func)(void);

void sh_memtest_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    jump_func jump;
    u32 b_opcode;
    int i;

    /* the first address should be branch to entry point (_boot) */
    b_opcode = *(u32*)app_memtest_image;
    if((b_opcode & 0xff000000) != 0xea000000)
    {
        fnet_shell_printf(desc, "Can not start the memtest. Test application seems corrupted.\n");
        return ;
    }

    fnet_shell_printf(desc, "Killing the application, FPGA reset, interrupt disable\n");

    /* disable all interrupts */
    for(i=0; i<XSCUGIC_MAX_NUM_INTR_INPUTS; i++)
        gic_disable(i);
    xcpu_assert_fpga_reset(0xf);

    fnet_shell_printf(desc, "Copying the mem_test to OCM\n");
    memcpy(0x00000000, app_memtest_image, app_memtest_image_len);
    Xil_DCacheFlush();

    jump = (jump_func) ((b_opcode & 0x00ffffff) * 4 + 8);

    dsb();
    isb();

    jump();

    while(1);
}

