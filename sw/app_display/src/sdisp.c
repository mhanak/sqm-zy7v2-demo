/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief Display driver
*
***************************************************************************/

#include "main.h"
#include "sdisp.h"

static sdisp_regs_t* sdisp_regs;

void sdisp_init()
{
    sdisp_regs = (sdisp_regs_t*)SDISP_BASE_ADDR;

    // setup for 800x480 display
    sdisp_regs->hsync = SDISP_HSYNC_RES(800) | SDISP_HSYNC_LEN(15) | SDISP_HSYNC_BP(88) | SDISP_HSYNC_FP(40);
    sdisp_regs->vsync = SDISP_VSYNC_RES(480) | SDISP_VSYNC_LEN(3) | SDISP_VSYNC_BP(32) | SDISP_VSYNC_FP(13);

    sdisp_regs->ctrl = SDISP_CTRL_FDIV(3);
    sdisp_regs->ctrl |= SDISP_CTRL_ENA;
}

static void sdisp_dump_regs(fnet_shell_desc_t desc)
{
    fnet_shell_printf(desc, "->ctrl=0x%08x\n", sdisp_regs->ctrl);
    fnet_shell_printf(desc, "->status=0x%08x\n", sdisp_regs->status);
    fnet_shell_printf(desc, "->hsync=0x%08x\n", sdisp_regs->hsync);
    fnet_shell_printf(desc, "->vsync=0x%08x\n", sdisp_regs->vsync);
    fnet_shell_printf(desc, "->bkgnd=0x%08x\n", sdisp_regs->bkgnd);
    fnet_shell_printf(desc, "->dead=0x%08x\n", sdisp_regs->dead);
}


void sh_disp_cmd(fnet_shell_desc_t desc, int argc, char ** argv)
{
    int i;

    if(argc <= 1)
    {
        sdisp_dump_regs(desc);
        return ;
    }

    u32 c;
    int v;

    for(i=1; i<argc; i++)
    {
        c = sdisp_regs->bkgnd;

        if(strcmp(argv[i], "off") == 0)
            sdisp_regs->ctrl &= ~SDISP_CTRL_ENA;
        else if(strcmp(argv[i], "on") == 0)
            sdisp_regs->ctrl |= SDISP_CTRL_ENA;
        else if(strcmp(argv[i], "red") == 0)
            sdisp_regs->bkgnd = RGB(0xff,0,0);
        else if(strcmp(argv[i], "green") == 0)
            sdisp_regs->bkgnd = RGB(0,0xff,0);
        else if(strcmp(argv[i], "blue") == 0)
            sdisp_regs->bkgnd = RGB(0,0,0xff);
        else if(strcmp(argv[i], "black") == 0)
            sdisp_regs->bkgnd = RGB(0,0,0);
        else if(strcmp(argv[i], "white") == 0)
            sdisp_regs->bkgnd = RGB(0xff,0xff,0xff);
        else if(strcmp(argv[i], "inv") == 0)
            sdisp_regs->ctrl ^= SDISP_CTRL_INV;
        else if(argv[i][0] == 'r' && sscanf(argv[i]+1, "%i", &v)==1)
            sdisp_regs->bkgnd = RGB(v, RGB_G(c), RGB_B(c));
        else if(argv[i][0] == 'g' && sscanf(argv[i]+1, "%i", &v)==1)
            sdisp_regs->bkgnd = RGB(RGB_R(c), v, RGB_B(c));
        else if(argv[i][0] == 'b' && sscanf(argv[i]+1, "%i", &v)==1)
            sdisp_regs->bkgnd = RGB(RGB_R(c), RGB_G(c), v);
    }
}

