/**************************************************************************
*
* Copyright 2013 by ZD Rpety - DAKEL, Czech Republic
*
* @author Michal Hanak
*
* @brief QSP Flash handling compatible with FNET memory regions
*
***************************************************************************/

#include "main.h"
#include "xdlibrary.h"
#include "memory.h"
#include "fapp_mem.h"

static int memory_qsflash_copy_to(void* param, FNET_COMP_PACKED_VAR void *dest, const FNET_COMP_PACKED_VAR void *src, unsigned n);
static int memory_qsflash_copy_from(void* param,  FNET_COMP_PACKED_VAR void *dest, const FNET_COMP_PACKED_VAR void *src, unsigned n);
static int memory_qsflash_erase(void* param, void *address, unsigned n);
static int memory_qsflash_on_write(void* param , int post_write);

static const struct fapp_mem_region_interface qsflash_rw_interface =
{
    memory_qsflash_copy_to,   /* copy_to */
    memory_qsflash_copy_from, /* copy_from */
    memory_qsflash_erase,     /* erase */
    memory_qsflash_on_write,  /* on_write */
};

/* context structure used as region API parameter */
struct qsflash_rw_param
{
    int qsflash_instance;
    unsigned long last_used_sector;
};

static struct qsflash_rw_param p1 = { 0, 0 };

const struct fapp_mem_region fapp_mem_regions[] =
{
    /* note: the first region is TFTP destination by default */
    { "QSPIFLASH", &qsflash_rw_interface,  MEM_QSFLASH_SPACE_ADDR, MEM_QSFLASH_SPACE_SIZE, MEM_QSFLASH_EPAGE_SIZE, MEM_QSFLASH_SPACE_ADDR, &p1 },
    { "SRAM",      NULL, MEM_SRAM_ADDRESS, MEM_SRAM_SIZE },
    { "RAM",       NULL, MEM_RAM_ADDRESS,  MEM_RAM_SIZE },
    {0,0,0,0,0,0} /* End */
};

#define QSFLASH_TIMEOUT_MS 10000
#define IS_QSPI_SPACE(addr) ( (((u32)(addr)) >= MEM_QSFLASH_SPACE_ADDR) && (((u32)(addr)) < (MEM_QSFLASH_SPACE_ADDR+MEM_QSFLASH_SPACE_SIZE)) )

const struct fapp_mem_region_reserved fapp_mem_regions_reserved[] =
{
    {0,0,0} /* End */
};

static int memory_qsflash_copy_to(void* param, FNET_COMP_PACKED_VAR void *dest, const FNET_COMP_PACKED_VAR void *src, unsigned n)
{
    struct qsflash_rw_param* p = (struct qsflash_rw_param*) param;
    int result = FNET_ERR;
    unsigned long qsflash_addr;
    unsigned long addr;
    unsigned long size;

    /* write flash */
    if(IS_QSPI_SPACE(dest) && !IS_QSPI_SPACE(src))
    {
        /* make address zero-based */
        qsflash_addr = ((unsigned long)dest) - MEM_QSFLASH_SPACE_ADDR;

        if(SUCCEEDED(qsflash_get_sector_info(p->qsflash_instance, (unsigned long)qsflash_addr, &addr, &size, NULL)))
        {
            /* generally OK, but something may fail later */
            result = FNET_OK;

            /* erase before write? */
            if(p->last_used_sector != addr)
            {
                unsigned long to_erase = n;

                while(result == FNET_OK && to_erase > 0)
                {
                    if(FAILED(qsflash_erase(p->qsflash_instance, addr, size, 0, QSFLASH_TIMEOUT_MS)))
                        result = FNET_ERR;

                    /* remember which sector was erased last */
                    p->last_used_sector = addr;

                    /* need to erase next? */
                    if(FAILED(qsflash_get_sector_info(p->qsflash_instance, addr+size, &addr, &size, NULL)))
                        result = FNET_ERR;

                    if(to_erase > size)
                        to_erase -= size;
                    else
                        to_erase = 0;
                }
            }

            /* write data even if the above has failed, the final result will reflect the overall success */
            if(FAILED(qsflash_write(0, qsflash_addr, n, src, QSFLASH_TIMEOUT_MS)))
                result = FNET_ERR;
        }
    }

    return result;
}

static int memory_qsflash_copy_from(void* param,  FNET_COMP_PACKED_VAR void *dest, const FNET_COMP_PACKED_VAR void *src, unsigned n)
{
    struct qsflash_rw_param* p = (struct qsflash_rw_param*) param;
    unsigned long qsflash_addr;
    int result = FNET_ERR;

    /* read flash */
    if(!IS_QSPI_SPACE(dest) && IS_QSPI_SPACE(src))
    {
        /* make address zero-based */
        qsflash_addr = ((unsigned long)src) - MEM_QSFLASH_SPACE_ADDR;

        if(SUCCEEDED(qsflash_read(p->qsflash_instance, qsflash_addr, n, dest, QSFLASH_SPEED_QUAD)))
            result = FNET_OK;
    }

    return result;
}

static int memory_qsflash_erase(void* param, void *address, unsigned n)
{
    struct qsflash_rw_param* p = (struct qsflash_rw_param*) param;
    unsigned long qsflash_addr;
    int result = FNET_ERR;

    if(IS_QSPI_SPACE(address))
    {
        /* make address zero-based */
        qsflash_addr = ((unsigned long)address) - MEM_QSFLASH_SPACE_ADDR;

        if(SUCCEEDED(qsflash_erase(p->qsflash_instance, qsflash_addr, n, TRUE, 10000)))
            result = FNET_OK;
    }

    return result;
}

static int memory_qsflash_on_write(void* param , int post_write)
{
    struct qsflash_rw_param* p = (struct qsflash_rw_param*) param;

    /* forget about any sector erasing during write */
    p->last_used_sector = 0xffffffff;
    return FNET_OK;
}

