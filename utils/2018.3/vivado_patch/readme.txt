
To add SQM4 boards support in Vivado:

copy processing_system7_v5_5 folder content to:
c:\Xilinx\Vivado\2015.3\data\ip\xilinx\processing_system7_v5_5

copy board_parts folder content to
c:\Xilinx\Vivado\2015.3\data\boards\board_parts\zynq
